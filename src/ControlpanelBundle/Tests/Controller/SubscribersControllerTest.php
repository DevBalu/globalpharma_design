<?php

namespace ControlpanelBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SubscribersControllerTest extends WebTestCase
{
    public function testProductsubslist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/productsubslist');
    }

    public function testNewsletsubs()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'newsletsubslist');
    }

    public function testNotifyproductsubs()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/notifyproductsubs');
    }

    public function testNotifynewsletsubs()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/notifynewsletsubs');
    }

}
