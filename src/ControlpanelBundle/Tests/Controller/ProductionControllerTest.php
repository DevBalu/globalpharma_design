<?php

namespace ControlpanelBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductionControllerTest extends WebTestCase
{
    public function testCategory()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Category');
    }

    public function testSubcategorylist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Subcategorylist');
    }

    public function testNewsubcategory()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Newsubcategory');
    }

    public function testNewcategory()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Newcategory');
    }

    public function testProductlist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Productlist');
    }

    public function testNewproduct()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Newproduct');
    }

}
