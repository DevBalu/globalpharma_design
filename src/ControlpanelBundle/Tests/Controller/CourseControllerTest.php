<?php

namespace ControlpanelBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CourseControllerTest extends WebTestCase
{
    public function testNewcourse()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/newcourse');
    }

    public function testRmvcourse()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/rmvCourse');
    }

    public function testUpdatecourse()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/updateCourse');
    }

}
