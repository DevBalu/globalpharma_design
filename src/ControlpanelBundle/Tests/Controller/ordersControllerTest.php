<?php

namespace ControlpanelBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ordersControllerTest extends WebTestCase
{
    public function testAllorders()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/allorders');
    }

    public function testNeworders()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/neworders');
    }

    public function testErrororders()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/errororders');
    }

}
