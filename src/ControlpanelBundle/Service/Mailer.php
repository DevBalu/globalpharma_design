<?php
namespace ControlpanelBundle\Service;

use Symfony\Component\HttpFoundation\JsonResponse;

// Entity
use AppBundle\Entity\Orderr;

// PHP Mailer extention
use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

class Mailer {

	/*---------------------------------------
			Mailer service
	---------------------------------------*/
	public function sendMail($from, $to, $subject, $body, $files = null, $contact_prof) {
		$mail = new PHPMailer(); 

		$contact_prof = (object)$contact_prof;
		try {

			/*--------------------------------------------------------------------
						SERVER CONFIGURATION
			---------------------------------------------------------------------*/

			$mail->isSMTP();                                      // Set mailer

			/*Enable SMTP debugging
			0 = off (for production use)
			1 = client messages
			2 = client and server messages*/
			$mail->SMTPDebug = 0;

			/*Set the hostname of the mail server*/
			 $mail->Host = 'smtp.gmail.com';

			 // Encoding message for usage "rus" characters
			 $mail->CharSet = 'UTF-8';

			/*Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission*/
			$mail->Port = 587;

			/*Set the encryption system to use - ssl (deprecated) or tls*/
			$mail->SMTPSecure = 'tls';

			/*Whether to use SMTP authentication*/
			$mail->SMTPAuth = true;

			/*Username to use for SMTP authentication - use full email address for gmail*/
			$mail->Username = $contact_prof->mail_value;

			/*Password to use for SMTP authentication*/
			$mail->Password = $contact_prof->mail_pass;

			/*--------------------------------------------------------------------
						MAIL SETTINGS
			---------------------------------------------------------------------*/
			//Set who the message is to be sent from
			// ini_set("auto_detect_line_endings", true);
			$mail->AddReplyTo($from);
			$mail->setFrom($from);

			//Set an alternative reply-to address
			// $mail->addReplyTo('askdevbalu@gmail.com');

			//Set who the message is to be sent to
			foreach ($to as $value) {
				$mail->addAddress($value);
			}

			//Set the subject line
			$mail->Subject = $subject;

			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			$mail->msgHTML($body, __DIR__);

			//Replace the plain text body with one created manually
			$mail->AltBody = 'This is a plain-text message body';

			/* Attach an image file */
			if (!empty($files)) {
				foreach ($files as $key => $file) {
					$uploadfile = tempnam(sys_get_temp_dir(), hash('sha256', $file->getClientOriginalName()) );
					if (move_uploaded_file($file->getRealPath(), $uploadfile)) {
						$file_name = $file->getClientOriginalName();

						$mail->addAttachment($uploadfile, $file_name);
					}
				}

			}

			// print "<pre>";
			// print_r($file);
			// print "</pre>";
			// die;

			//send the message, check for errors
			if ($mail->send()) {
				return 'success';
			}

		} catch (Exception $e) {
			// echo 'Message could not be sent. Mailer Error:';
			return $mail->ErrorInfo;
		}

	}


	/*----------------------------------------------------------------
		Prepare entry whcih will be use for notify customer
	----------------------------------------------------------------*/ 
	public function prodSubscPrepare($products, $subscribers){

		$users_to_notify = [];

		foreach ($subscribers as $key => $subscriber) {

			$need_prod = $products->findBy(
				['id' => $subscriber->getPid()],
				[]
			);

			if (!empty($need_prod) && $need_prod[0]->getInStock() == 1) {
				$prepare_subs_arr = (array)$subscriber;
				$prepare_subs_arr['prod_detail'] = (array)$need_prod;
			} else {
				$prepare_subs_arr = [];
			}

			$users_to_notify[] = $prepare_subs_arr;

		}
		return $users_to_notify;
	} /*END prodSubscPrepare*/

	/*----------------------------------------------------------------
		check if product need to 
	----------------------------------------------------------------*/ 

	public function prodNotify($products, $subscribers, $contact_prof){
		// get prepared arr
		$needed_subs_arr = $this->prodSubscPrepare($products, $subscribers);

		if (empty($needed_subs_arr)) {
			return null;
		}

		$result = [
			'sent' => [],
			'not_sent' => []
		];

		// prepare variables needed for method
		foreach ($needed_subs_arr as $key => $subs) {
			if (!empty($subs)) {

				$prod_detail = (array)$subs['prod_detail'][0];

				//prepare prod images
				if ($prod_detail['images'][0]) {
					$prod_img = ' <img width="100" src="' . $prod_detail['images'][0] . '" > <br>';
				}else {
					$prod_img = '';
				}

				$from = $contact_prof['mail_value'];

				$to = [
					$subs['mail']
				];

				// $subject =  base64_encode('Оповещение о появление в наличии продукта');
				$subject =  'Notify:' . $prod_detail['name'] . $prod_detail['count_dosage'] . ': appear on stock.' ;

				$body = $prod_img
					. 'Продукт ' . $prod_detail['name'] . $prod_detail['count_dosage'] . '<br>'
					. '<p> Есть в наличии </p>' ;
				$files = null;

				$send_mail = $this->sendMail($from, $to, $subject, $body, $files, $contact_prof);

				//set value of subs which need to remove
				if ($send_mail == "success") {
					$result['sent'][] = [
						'pid' => $prod_detail['id'],
						'subs_id' => $subs['id'],
						'pname' => $prod_detail['name'] . $prod_detail['count_dosage'],
						'mail_response' => $send_mail
					];
				} else {
					$result['not_sent'][] = [
						'pid' => $prod_detail['id'],
						'subs_id' => $subs['id'],
						// 'pname' => $subs['name'],
						'user_mail' => $subs['mail']
					];
				}
			}
		}

		return $result;

	} /*END prodNotify*/


} /*END class*/