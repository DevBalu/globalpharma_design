<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
// Entity
use AppBundle\Entity\Contact;

// sevices
use AppBundle\Service\FileUpload;
use ControlpanelBundle\Service\Mailer;

class DefaultController extends Controller
{

	/**
	 * @Route("/testCP", name="testCP")
	 */
	public function testCPAction(Request $request, Mailer $mailer)
	{
		$db_detail = [
			'user' => 'b020c17f191d1e',
			'pass' => '4f904957',
			'host' => 'us-cdbr-iron-east-04.cleardb.net',
			'heroku_db' => 'heroku_814c5bfd6cbf28a'
		];

		$script = 'mysqldump -h ' . $db_detail['host'] . ' -u ' . $db_detail['user']  . ' ' . $db_detail['heroku_db'] . ' -p ' . $db_detail['heroku_db'] . '> db_backup.sql';

		$percentage = 3;
		$total = 200;

		$new = ($percentage / 100) * $total;

		print "<pre>";
		print_r($new);
		print "</pre>";

		// if (exec($script)) {
		// }

		return new JsonResponse();
	}

	/**
	 * @Route("/cpdefault", name="cpdefault")
	 */
	public function cpanelAction()
	{
		return $this->render('@CP/Default/index.html.twig');
	}

	/**
	 * @Route("/notfound", name="notfound")
	 */
	public function notfoundAction()
	{
		return $this->render('@CP/Errorpage/page_404.html.twig');
	}

}
