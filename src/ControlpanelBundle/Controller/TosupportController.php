<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TosupportController extends Controller
{
	/**
	 * @Route("/setMail", name="setMail")
	 */
	public function setMailAction()
	{
		return $this->render('@CP/Tosupport/to_support_message.html.twig', array(
			// ...
		));
	}

	/**
	 * @Route("/rmvMessage")
	 */
	public function rmvMessageAction()
	{
		return $this->render('ControlpanelBundle:Tosupport:rmv_message.html.twig', array(
			// ...
		));
	}

}
