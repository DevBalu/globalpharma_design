<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\shopstate;

// Catch error when insert on DB
use Doctrine\DBAL\DBALException;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class ShopstateController extends Controller
{

	/**
	* @Route("/shop_state_list", name="shop_state_list")
	*/
	public function shop_state_listAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('logout');
		}

		$em = $this->getDoctrine()->getManager();
		/*                   Check at shop state
		------------------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);
		$shopstates =  $shopstate_repo->findAll();

		return $this->render('@CP/shopstate/shop_state_list.html.twig', array(
			'shopstates' => $shopstates
		));

	}

	/**
	* @Route("/newState", name="newState")
	*/
	public function newStateAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('logout');
		}

		/*                  get repo-es from db
		------------------------------------------------------------------------------*/
		$em = $this->getDoctrine()->getManager();
		$cache = new FilesystemCache();

		$shopstate_repo = $em->getRepository(shopstate::class);
		$shopstate = $shopstate_repo->findBy([
			'status' => 1
		],[]);

		$sate_name = $request->request->get('sate_name');
		$sate_content = $request->request->get('sate_content');

		if (empty($sate_name) || empty($sate_name)) {
			$this->addFlash(
				'error',
				'Оба поля яв-ся обязательными для заполнения.'
			);
			return $this->redirectToRoute('shop_state_list');
		}

		/*--------------------------------------------------
				Make all states innactive, becouse
		--------------------------------------------------*/
		foreach ($shopstate as $key => $state) {
			try {
				$state->setStatus(null);
				$em->persist($state);
				$em->flush();

			} catch (DBALException $e) {
				$this->addFlash(
					'error',
					$e->getMessage()
				);
				return $this->redirectToRoute('statemassage');
			}
		}

		try {
			$shopstate = new shopstate();
			$shopstate->setState($sate_name);
			$shopstate->setContent($sate_content);
			$shopstate->setStatus(0);

			$em->persist($shopstate);
			$em->flush();

			$cache->clear();

			$this->addFlash(
				'success',
				'Состояние сайта было успешно создано'
			);
			return $this->redirectToRoute('shop_state_list');

		} catch (DBALException $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('shop_state_list');
		}

	}

	/**
	* @Route("/updateState", name="updateState")
	*/
	public function updateStateAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('logout');
		}

		/*                  get repo-es from db
		------------------------------------------------------------------------------*/
		$em = $this->getDoctrine()->getManager();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();

		$shopstate_repo = $em->getRepository(shopstate::class);

		$sid = $request->request->get('sid');
		$sName = $request->request->get('sName');
		$sContent = $request->request->get('sContent');
		$sStatus = $request->request->get('sStatus');

		if (empty($sid)) {
			$this->addFlash(
				'error',
				'Не заданы параметры для поиска записи.'
			);
			return $this->redirectToRoute('shop_state_list');
		}


		/*--------------------------------------------------
				if status not empty, make field status on 
				another entries inncative
		--------------------------------------------------*/
		if ($sStatus == 'checked') {
			$shopstate = $shopstate_repo->findBy([
				'status' => 1
			],[]);

			foreach ($shopstate as $key => $state) {
				try {
					$state->setStatus(null);
					$em->persist($state);
					$em->flush();

				} catch (DBALException $e) {
					$this->addFlash(
						'error',
						$e->getMessage()
					);
					return $this->redirectToRoute('statemassage');
				}
			}
		}


		try {
			$shopstate = $shopstate_repo->find($sid);

			if (!$shopstate) {
				$this->addFlash(
					'error',
					'Не найдено записи для едитирования'
				);
				return $this->redirectToRoute('shop_state_list');
			}

			$shopstate->setState($sName);
			$shopstate->setContent($sContent);

			if ($sStatus == 'checked') {
				$shopstate->setStatus(1);
			}else {
				$shopstate->setStatus(0);
			}

			$em->persist($shopstate);
			$em->flush();

			$cache->clear();

			$this->addFlash(
				'success',
				'Данные были обновлены'
			);
			return $this->redirectToRoute('shop_state_list');

		} catch (DBALException $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('shop_state_list');
		}

	}

	/*-----------------------------------------------------
				New shop state entry
	-----------------------------------------------------*/

	/**
	 * @Route("/removeState/{sid}", name="removeState")
	 */
	public function removeWexWalletAction(Request $request, $sid)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();

		$state_repo = $this->getDoctrine()->getRepository(shopstate::class);

		try {
			$state = $state_repo->find($sid);

			if ($state) {
				$em->remove($state);
				$em->flush();

				$cache->clear();

				$this->addFlash(
					'success',
					"Статус был удалён"
				);
			}else{
				$this->addFlash(
					'error',
					"Не найдено записи с таким ИД"
				);
			}

			return $this->redirectToRoute('shop_state_list');

		} catch (Exception $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('shop_state_list');
		}

		return $this->redirectToRoute('shop_state_list');
	}

	/**
	* @Route("/statemessage", name="statemessage")
	*/
	public function statemassageAction()
	{
		$em = $this->getDoctrine()->getManager();
		/*                   Check at shop state
		------------------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);
		$shopstate =  $shopstate_repo->findOneBy(array(
			'status' => 1
		), []);

		if ($shopstate) {
			$current_state = $shopstate->getState();
			if ($current_state == "shop_active") {
				return $this->redirectToRoute('homepage');
			}
		}

		return $this->render('@CP/shopstate/statemessage.html.twig', array(
			'shopstate' => $shopstate
		));

	}


} /*./ END of CLASS*/
