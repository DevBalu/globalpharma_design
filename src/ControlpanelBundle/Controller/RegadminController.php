<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\User;

/*for encode password*/
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class RegadminController extends Controller
{
	/**
	* @Route("/regadmin", name="regadmin")
	*/
	public function regadminAction(Request $request, UserPasswordEncoderInterface $encoder)
	{
		/* Register new user 
		-----------------------------*/

		// $_POST parameters
		$name_login = $request->request->get('name_login');
		$email_login = $request->request->get('email_login');
		$password_login = $request->request->get('password_login');
		$role = $request->request->get('role');


		/* check if data resived is valid
		-----------------------------*/
		if (!empty($name_login) && !empty($password_login) && !empty($role) ) {

			try {
				//Get the enity manager
				$em = $this->getDoctrine()->getManager();
				$user = new User();

				// encode pass
				$encoded_password = $encoder->encodePassword($user, $password_login);

				/* set data in sb
				-----------------------------*/
				$user->setUsername($name_login);
				$user->setPassword($encoded_password);

				// mail validation
				if (!empty($email_login) && filter_var($email_login, FILTER_VALIDATE_EMAIL) ) {
					$user->setEmail($email_login);
				}

				$em->persist($user);
				$em->flush();

				//Get the user with name registered
				$user_role= $em->getRepository(User::class)->findOneBy(Array("username" => $name_login));
				//Set the user role
				$user_role->setRoles([$role]);
				//Save it to the database
				$em->persist($user_role);
				$em->flush();

				$this->addFlash(
					'notice',
					'Пользователь успешно был добавлен в систему!'
				);

				return $this->redirectToRoute('cpanel');
				// END set data in sb

			}
			/*if appear error with duplicate of entity redirect user to registration page.
			need to male notify */
			catch(UniqueConstraintViolationException $e) {
				return $this->redirectToRoute('regcp');
			}

		} else {
			return $this->redirectToRoute('regcp');
		}

	}/*./regadminAction END */


	/**
	* @Route("/regcp", name="regcp")
	*/
	public function regcpAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		} else {
			return $this->render('@CP/authcp/regcp.html.twig');
		}
	}/* ./regcpAction end */

}
