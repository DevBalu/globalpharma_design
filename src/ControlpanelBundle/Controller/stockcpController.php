<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

// Entity
use AppBundle\Entity\category;
use AppBundle\Entity\products;
use AppBundle\Entity\Manufacturers;
use AppBundle\Entity\subscribers;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Course;

use ControlpanelBundle\Service\Mailer;

// Catch db extention
use Doctrine\DBAL\DBALException;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class stockcpController extends Controller
{


	public function accessDenied(){
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}
	}

	/**
	* @Route("/stockcp", name="stockcp")
	*/
	public function stockcpAction()
	{
		$this->accessDenied();

		$em = $this->getDoctrine()->getManager();

		/*------------------------------------------------------------
						PREPARE DATA FOR USING 
		--------------------------------------------------------------*/

		/* --------------products repo --------- */
		$products_repo = $this->getDoctrine()->getRepository(products::class);


		$products = $products_repo->findBy([
			'entry_status' => 1,
		], ['id' => 'DESC']);

		/*------------------------------------------------------------
				Check if products is in stock or not
				If is quantity of product is 0  change status
				to "not in stock" 
		--------------------------------------------------------------*/
		$product_not_in_stock = $products_repo->inStock();

		/*------------------------------------------------------------
				Statistics how much products in 
				orders and with how status have
		--------------------------------------------------------------*/
		$prodOnOrder = $products_repo->prodOnOrder(); 

		return $this->render('@CP/stockcp/stockcp.html.twig', array(
			"products" => $products,
			"prodOnOrder" => $prodOnOrder
		));
	}

	/**
	* @Route("/notifyAbouProd", name="notifyAbouProd")
	*/
	public function notifyAbouProdAction(Mailer $mailer)
	{
		$em = $this->getDoctrine()->getManager();

		/* ------------subscribers repo --------- */
		$subscribers_repo = $this->getDoctrine()->getRepository(subscribers::class);
		/* --------------products repo --------- */
		$products_repo = $this->getDoctrine()->getRepository(products::class);

		$subscribers = $subscribers_repo->findBy(
				array(
				'target' => 'products'
			), array(
				'id' => 'DESC'
			));

		/*------------------------------------------------------------
				call service which will be check 
				how products at moment appear on sistem
				and need to notify customer
		--------------------------------------------------------------*/
		$contact_repo = $em->getRepository(Contact::class);
		$contact_prof = $contact_repo->findOneBy([], ['id' => "DESC"], 1)->info;

		$mail_result = $mailer->prodNotify($products_repo, $subscribers, $contact_prof);

		/*------------------------------------------------------------
				If notify message was successful sended
				remove entries from table subscribers
		--------------------------------------------------------------*/

		if (!empty($mail_result['sent'])) {
			foreach ($mail_result['sent'] as $key => $entry) {

				$entry_to_remove = $subscribers_repo->find($entry['subs_id']);

				if ($entry_to_remove) {
					$em->remove($entry_to_remove);
					$em->flush();
				}
			}
		}

		return new JsonResponse($mail_result);
	}

	/**
	* @Route("/updateStockcp", name="updateStockcp")
	*/
	public function updateStockcpAction(Request $request)
	{
		$this->accessDenied();

		$cache = new FilesystemCache();

		$pid = $request->request->get('pid');
		$quantity = $request->request->get('quantity');
		if (empty($quantity) ) {
			$quantity = 0;
		}

		if ($quantity < 0) {
			$this->addFlash(
				'error',
				"Кол-во не должно быть отрицательным"
			);

			return $this->redirectToRoute('stockcp');
		}

		$em = $this->getDoctrine()->getManager();
		try{

			$products_repo = $this->getDoctrine()->getRepository(products::class);
			$product = $products_repo->find($pid);

			$product->setProductQuantity($quantity);
			$em->persist($product);
			$em->flush();

			$cache->clear();

			$this->addFlash(
				'default',
				"Данные успешно обновлены"
			);

		}
		catch( DBALException $e ){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
		}

		return $this->redirectToRoute('stockcp');

		// return new JsonResponse();

	}

}
