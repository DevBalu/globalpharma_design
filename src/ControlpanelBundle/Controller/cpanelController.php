<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

// Entity
use AppBundle\Entity\reviews;
use AppBundle\Entity\products;
use AppBundle\Entity\Orderr;

class cpanelController extends Controller
{
	/**
	* @Route("/cpanel", name="cpanel")
	*/
	public function startcpAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_COURIER')) {
			return $this->redirectToRoute('redirectrole');
		}

		/*------------------------------------------------------
			Nr of testimonials wated confirmation
		------------------------------------------------------*/

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		// get reviews entity
		$reviews_repo = $em->getRepository(reviews::class);


		$testimonials = $reviews_repo->findBy(
			array(
				'entry_status' => 1,
				'status' => 'confirm',
			), array('id' => 'DESC'));

		$nr_new_test = count($testimonials);

		/*------------------------------------------------------
			Work with order statistics
		------------------------------------------------------*/
		// get order entity
		$Order_repo = $em->getRepository(Orderr::class);
		$orders_statistics = $Order_repo->ordersStatistics();


		/*------------------------------------------------------------
				Check if products is in stock or not
				If is quantity of product is 0  change status
				to "not in stock" 
		--------------------------------------------------------------*/
		$products_repo = $em->getRepository(products::class);
		$product_not_in_stock = $products_repo->inStock();

		return $this->render('@CP/startcp/startcp.html.twig', array(
			'testimonials' => $nr_new_test,
			"product_not_in_stock" => $product_not_in_stock,
			"orders_statistics" => $orders_statistics
		));

	}


	/**
	* @Route("/logincp", name="logincp")
	*/
	public function logincpAction()
	{
		return $this->render('@CP/authcp/logincp.html.twig');
	}

	/**
	* @Route("/redirectrole", name="redirectrole")
	*/
	public function redirectroleAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
			return $this->redirectToRoute('logincp');
		}
		else if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
			return $this->redirectToRoute('cpanel');
		}elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COURIER')) {
			return $this->redirectToRoute('cpanel');
		}elseif ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}else{
			return $this->redirectToRoute('logincp');
		}
	}

}
