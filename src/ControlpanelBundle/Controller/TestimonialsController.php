<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// Entity
use AppBundle\Entity\reviews;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class TestimonialsController extends Controller
{

	public function AccessDenied(){
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}
	}

	/**
	* @Route("/testimonialsList", name="testimonialsList")
	*/
	public function testimonialsListAction()
	{
		$this->AccessDenied();

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		// get user entity
		$reviews_repo = $em->getRepository(reviews::class);

		$testimonials = $reviews_repo->findBy(
			array(
				'entry_status' => 1
			), array('id' => 'DESC'));

		return $this->render('@CP/Testimonials/testimonials_list.html.twig', array(
			'testimonials' => $testimonials
		));
	}


	/**
	* @Route("/updatetestimonials", name="updatetestimonials")
	*/
	public function updatetestimonialsAction(Request $request)
	{
		$this->AccessDenied();
		// get new status of testimonial 
		$status = $request->request->get('status');

		// get testimonials id
		$test_id = $request->request->get('test_id');

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();

		// if parametres is empty return error notify
		if (empty($status) || empty($test_id)) {
			$arrData = [
				'type' => 'error',
				'output' => 'Один из параметров не был задан правельно.'
			];

			return new JsonResponse($arrData);
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		// get user entity
		$reviews_repo = $em->getRepository(reviews::class);

		$entry = $reviews_repo->find($test_id);

		if ($entry) {
			$entry->setStatus($status);
			$entry->setDate();
			$em->persist($entry);
			$em->flush();

			$cache->clear();
		}else {
			$arrData = [
				'type' => 'error',
				'output' => 'Отзыв с таким ид не был найден'
			];

			return new JsonResponse($arrData);
		}

		$arrData = [
			'type' => 'success',
			'output' => 'Статус отзыва был успешно изменён'
		];

		return new JsonResponse($arrData);
	}

	/**
	* @Route("/rmTestimonial", name="rmTestimonial")
	*/
	public function rmTestimonialAction(Request $request)
	{
		$this->AccessDenied();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();

		// get testimonials id
		$test_id = $request->request->get('test_id');

		// if parametres is empty return error notify
		if (empty($test_id)) {
			$arrData = [
				'type' => 'error',
				'output' => 'Параметр поиска  не был задан.'
			];

			return new JsonResponse($arrData);
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		// get user entity
		$reviews_repo = $em->getRepository(reviews::class);

		$entry = $reviews_repo->find($test_id);

		if ($entry) {
			$entry->setEntryStatus(0);
			$em->persist($entry);
			$em->flush();

			$cache->clear();
		}else {
			$arrData = [
				'type' => 'error',
				'output' => 'Отзыв с таким ид не был найден'
			];

			return new JsonResponse($arrData);
		}

		$arrData = [
			'type' => 'success',
			'output' => 'Отзыв был успешно удалён.'
		];

		return new JsonResponse($arrData);
	}

}
