<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use AppBundle\Entity\category;
use AppBundle\Entity\subcategory;
use AppBundle\Entity\products;
use AppBundle\Entity\Manufacturers; 
use AppBundle\Entity\Basket; 
use AppBundle\Entity\Delivery_cost; 

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class ProductionController extends Controller
{

	/*---------------------------------------------------------------------------------------------------------------------------
	                                              Manufacturers ACTIONS
	---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * @Route("/Manufacturers", name="Manufacturers")
	 */
	public function ManufacturersAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// get doctrine
			$em = $this->getDoctrine()->getManager();

			/*                   Work with cahce
			------------------------------------------------------------------------------*/
			$cache = new FilesystemCache();

			// get user entity
			$man_repo = $em->getRepository(Manufacturers::class);

			if (!$cache->get('cpanel.Manufacturers')) {
				$man_list = $man_repo->findBy(
					array(
						'entry_status' => 1
					), array('id' => 'DESC'));

				$cache->set('start.Manufacturers', $man_list);
			}else {
				$man_list = $cache->get('start.Manufacturers');
			}

			return $this->render('@CP/Production/Manufacturers.html.twig', array(
				'man_list' => $man_list
			));
		}
	}

	/**
	 * @Route("/Newmanufacturer", name="Newmanufacturer") 
	 */
	public function NewmanufacturerAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			if ( $request->request->get('manufacturer_name') ) {

				// get doctrine
				$em = $this->getDoctrine()->getManager();

				/*                   Work with cahce
				------------------------------------------------------------------------------*/
				$cache = new FilesystemCache();

				// get user entity
				$Manufacturer = new Manufacturers();
				$manufacturer_name = $request->request->get('manufacturer_name');

				try {
					// set manufacturer_name
					$Manufacturer->setName($manufacturer_name);

					$em->persist($Manufacturer);
					$em->flush();

					$cache->clear();

					$this->addFlash(
						'success',
						'Производитель был успешно добавлен!'
					);

					return $this->redirectToRoute('Manufacturers');
				}

				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					return $this->redirectToRoute('cpanel');
				}
			} else {
				return $this->render('@CP/Production/newManufacturer.html.twig', array(
					// ...
				));
			}
		}
	}

	/**
	 * @Route("/manufacturerUpdate", name="manUpdate") 
	 */
	public function manUpdateAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			if ( $request->request->get('manufacturer_id') ) {
				try{
					// get doctrine
					$em = $this->getDoctrine()->getManager();

					/*                   Work with cahce
					------------------------------------------------------------------------------*/
					$cache = new FilesystemCache();

					// get repo
					$repository = $this->getDoctrine()->getRepository(Manufacturers::class);
					// get entity
					$manufacturer_id = $request->request->get('manufacturer_id');

					$Manufacturer = $repository->find($manufacturer_id);

					// UPDATE LOGIC
					/*subcat_name*/
					if ( $request->request->get('manufacturer_name') ) {
						$manufacturer_name = $request->request->get('manufacturer_name');
						$Manufacturer->setName($manufacturer_name);
					}

					$Manufacturer->setDate();

					$em ->persist($Manufacturer);
					$em ->flush();

					$cache->clear();

					$this->addFlash(
						'success',
						'Данные производителя были обновлены!'
					);

					return $this->redirectToRoute('Manufacturers');
				}
				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'notice',
						'Подкатегория с таким именем уже существует в базе данных!'
					);
					return $this->redirectToRoute('Manufacturers');
				} /*end catch duplicate*/
			}/*END if Action is requested*/

			return $this->redirectToRoute('Manufacturers');
		}/*END access limitaion*/
	}

	/**
	 * @Route("/rmvman/{manid}", name="rmvman") 
	 */
	public function rmvmanAction($manid)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			if ($manid) {
				// get doctrine
				$em = $this->getDoctrine()->getManager();

				/*                   Work with cahce
				------------------------------------------------------------------------------*/
				$cache = new FilesystemCache();

				// get products repo 
				$man_repository = $this->getDoctrine()->getRepository(Manufacturers::class);

				// get products repo 
				$prod_repository = $this->getDoctrine()->getRepository(products::class);

				// get man entity
				$on_man = $man_repository->findOneBy(array("id" => $manid), array());

				// get product entity
				$on_prod = $prod_repository->findOneBy(array("categoryId" => $manid), array());


				// if was finded entry remove her
				if ($on_man) {
					// $em ->remove($on_man);
					$on_prod->setEntryStatus(0);
				}

				// if was finded entry on production table with this field catid = give her value 0
				if ($on_prod) {
					$on_prod->setManufacturerid(null);
				}

				$em ->flush();

				$cache->clear();

				$this->addFlash(
					'success',
					'Производитель был успешно удалён!'
				);

				return $this->redirectToRoute('Manufacturers');
			} else {
				return $this->redirectToRoute('Manufacturers');
			}
		}
	}
	/*---------------------------------------------------------------------------------------------------------------------------
	                                              CATEGORY ACTIONS
	---------------------------------------------------------------------------------------------------------------------------*/

	/**
	 * @Route("/Categorylist", name="Categorylist")
	 */
	public function CategoryAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// get doctrine
			$em = $this->getDoctrine()->getManager();

			// get user entity
			$cat_repo = $em->getRepository(category::class);

			/*                   Work with cahce
			------------------------------------------------------------------------------*/
			$cache = new FilesystemCache();

			if (!$cache->get('start.category')) {
				$cat_list = $cat_repo->findBy(array(
					'entry_status' => 1
				), array('id' => 'DESC'));

				if ($cat_list) {
					$cache->set('start.category', $cat_list);
				}

			}else {
				$cat_list = $cache->get('start.category');
			}

			return $this->render('@CP/Production/categorylist.html.twig', array(
				'cat_list' => $cat_list
			));
		}
	}

	/**
	 * @Route("/Newcategory", name="Newcategory") 
	 */
	public function NewcategoryAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			if ( $request->request->get('category_name') ) {

				// get doctrine
				$em = $this->getDoctrine()->getManager();

				/*                   Work with cahce
				------------------------------------------------------------------------------*/
				$cache = new FilesystemCache();

				// get user entity
				$category = new category();
				$category_name = $request->request->get('category_name');

				try {
					// set category_name
					$category->setName($category_name);

					$em->persist($category);
					$em->flush();

					$cache->clear();

					$this->addFlash(
						'success',
						'Категория была успешно создана!'
					);

					return $this->redirectToRoute('Categorylist');
				}

				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'Категория с таким наименованием уже существует!'
					);
					return $this->redirectToRoute('Categorylist');

				}
			} else {
				return $this->render('@CP/Production/newcategory.html.twig', array(
					// ...
				));
			}
		}
	}


	/**
	 * @Route("/catupdate", name="catupdate") 
	 */
	public function catupdateAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			/*subcatid*/
			if ( $request->request->get('catid') ) {
				try{
					// get doctrine
					$em = $this->getDoctrine()->getManager();

					/*                   Work with cahce
					------------------------------------------------------------------------------*/
					$cache = new FilesystemCache();

					// get repo
					$repository = $this->getDoctrine()->getRepository(category::class);
					// get entity
					$catid = $request->request->get('catid');

					$category = $repository->find($catid);

					// UPDATE LOGIC
					/*subcat_name*/
					if ( $request->request->get('cat_name') ) {
						$cat_name = $request->request->get('cat_name');
						$category->setName($cat_name);
					}

					$category->setDate();

					$em->persist($category);
					$em->flush();

					$cache->clear(); 

					$this->addFlash(
						'success',
						'Категория была обновлена!'
					);

					return $this->redirectToRoute('Categorylist');
				}
				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'Категория с таким именем уже существует в базе данных!'
					);
					return $this->redirectToRoute('Categorylist');
				} /*end catch duplicate*/
			}/*END if Action is requested*/

			return $this->redirectToRoute('Categorylist');
		}/*END access limitaion*/
	}


	/**
	 * @Route("/rmcat/{catid}", name="catid") 
	 */
	public function rmcatAction($catid)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			if ($catid) {
				// get doctrine
				$em = $this->getDoctrine()->getManager();

				/*                   Work with cahce
				------------------------------------------------------------------------------*/
				$cache = new FilesystemCache();

				// get category repo 
				$cat_repository = $this->getDoctrine()->getRepository(category::class);

				// get subcategory repo 
				$subcat_repository = $this->getDoctrine()->getRepository(subcategory::class);

				// get products repo 
				$prod_repository = $this->getDoctrine()->getRepository(products::class);


				// get category entity
				$on_category = $cat_repository->find($catid);

				// get subcategory entity
				$on_subcategory = $subcat_repository->findOneBy(array("categoryid" => $catid), array());

				// get subcategory entity
				$on_prod = $prod_repository->findOneBy(array("categoryId" => $catid), array());

				// if was finded entry remove her
				if ($on_category) {
					// $em ->remove($on_category);
					$on_category->setEntryStatus(0);
				}

				// if was finded entry on subcategory table with this field catid = give value 0
				if ($on_subcategory) {
					$on_subcategory->setCategoryId(null);
				}

				// if was finded entry on production table with this field catid = give her value 0
				if ($on_prod) {
					$on_prod->setCategoryId(null);
				}

				$em ->flush();

				$cache->clear();

				$this->addFlash(
					'danger',
					'Категория была удалена успешно!'
				);

				return $this->redirectToRoute('Categorylist');
			} else {
				return $this->redirectToRoute('Categorylist');
			}
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------------
	                                                   SUBCATEGORY ACTIONS
	---------------------------------------------------------------------------------------------------------------------------*/
	/**
	 * @Route("/Subcategorylist", name="Subcategorylist")
	 */
	public function SubcategorylistAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// get doctrine
			$em = $this->getDoctrine()->getManager();

			// get subcategory.* and category name  
			$query = $this->getDoctrine()->getManager()
			->createQuery('
				SELECT s, c.name
				FROM AppBundle:subcategory s
					LEFT JOIN AppBundle:category c 
					WITH s.categoryid = c.id
				WHERE s.entry_status = 1
			');

			$subcat_list = $query->getResult();

			$cat_repo = $em->getRepository(category::class);
			$cat_list = $cat_repo->findBy(array(
				'entry_status' => 1
			), array('id' => 'DESC'));

			$catid = "0";
			return $this->render('@CP/Production/subcategorylist.html.twig', array(
				'subcat_list' => $subcat_list,
				'cat_list' => $cat_list,
				"getcatid" => $catid
			));
		}
	}

	/**
	 * @Route("/Newsubcategory", name="Newsubcategory")
	 */
	public function NewsubcategoryAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// get doctrine
			$em = $this->getDoctrine()->getManager();

			/*                   Work with cahce
			------------------------------------------------------------------------------*/
			$cache = new FilesystemCache();

			// if Action is requested from form with post methods make functionality bellow 
			if ( $request->request->get('subcategory_name') && $request->request->get('category_id') !== 'f' ) {
				// get category entity
				$subcategory = new subcategory();
				$subcategory_name = $request->request->get('subcategory_name');
				$category_id = $request->request->get('category_id');

				try {
					// set category_name
					$subcategory->setName($subcategory_name);
					$subcategory->setCategoryId($category_id);

					$em->persist($subcategory);
					$em->flush();

					$cache->clear();

					$this->addFlash(
						'success',
						'Подкатегория была успешно создана!'
					);

					return $this->redirectToRoute('Subcategorylist');
				}

				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'Подкатегория с таким именем уже существует в базе данных!'
					);
					return $this->redirectToRoute('Newsubcategory');
				}
			}/* END if action is requested with method post*/
			else if ($request->request->get('subcategory_name') && $request->request->get('category_id') == 'f' ) {
				$this->addFlash(
					'warning',
					'Категория не была выбрана, выберите категорию и повторите попытку!'
				);
				return $this->redirectToRoute('Newsubcategory');
			}

			// display all categories 
			// get user entity
			$cat_repo = $em->getRepository(category::class);

			if(!$cache->get('start.category')){
				$cat_list = $cat_repo->findBy(array(), array('name' => 'ASC'));
				if ($cat_list) {
					$cache->set('start.category', $cat_list);
				}
			}else {
				$cat_list =$cache->get('start.category');
			}

			return $this->render('@CP/Production/newsubcategory.html.twig', array(
				'cat_list' => $cat_list
			));

		}/*END if access permision*/
	}


	/**
	 * @Route("/sortsubcat/{catid}", name="sortsubcat",  requirements={"catid"="\d+"})
	 */
	public function SortsubcatAction(Request $request, $catid = 1)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {
			// get doctrine
			$em = $this->getDoctrine()->getManager();

			// get subcategory.* and category name  
			$query = $this->getDoctrine()->getManager()
			->createQuery('
				SELECT s, c.name
				FROM AppBundle:subcategory s
					LEFT JOIN AppBundle:category c 
					WITH s.categoryid=c.id
				WHERE s.id='.$catid.'
				AND s.entry_status = 1
				');

			$subcat_list = $query->getResult();

			// get list of category by filter 
			$cat_repo = $em->getRepository(category::class);
			$cat_list = $cat_repo->findBy(array(), array('id' => 'DESC'));

			return $this->render('@CP/Production/subcategorylist.html.twig', array(
				'subcat_list' => $subcat_list,
				'cat_list' => $cat_list,
				"getcatid" => $catid
			));
		}
	}

	/**
	 * @Route("/subcatupdate", name="subcatupdate") 
	 */
	public function subcatupdateAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			/*subcatid*/
			if ( $request->request->get('subcatid') ) {
				try{
					// get doctrine
					$em = $this->getDoctrine()->getManager();
					/*                   Work with cahce
					------------------------------------------------------------------------------*/
					$cache = new FilesystemCache();

					// get repo
					$repository = $this->getDoctrine()->getRepository(subcategory::class);
					// get entity
					$subcatid = $request->request->get('subcatid');

					$subcategory = $repository->find($subcatid);

					// UPDATE LOGIC
					/*subcat_name*/
					if ( $request->request->get('subcat_name') ) {
						$subcat_name = $request->request->get('subcat_name');
						$subcategory->setName($subcat_name);
					}

					/*parint_cat*/
					if ( $request->request->get('parint_cat') ) {
						$parintcat = $request->request->get('parint_cat');
						$subcategory->setCategoryId($parintcat);
					}

					$subcategory->setDate();

					$em->persist($subcategory);
					$em->flush();

					$cache->clear();

					$this->addFlash(
						'notice',
						'Подкатегория была обновлена!'
					);
					return $this->redirectToRoute('Subcategorylist');
				}
				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'notice',
						'Подкатегория с таким именем уже существует в базе данных!'
					);
					return $this->redirectToRoute('Subcategorylist');
				} /*end catch duplicate*/
			}/*END if Action is requested*/

			return $this->redirectToRoute('Subcategorylist');
		} /*END access checker*/
	}

	/**
	 * @Route("/rmsubcat/{subcatid}", name="rmsubcat") 
	 */
	public function rmsubcatAction($subcatid)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			/*                   Work with cahce
			------------------------------------------------------------------------------*/
			$cache = new FilesystemCache();

			// if Action is requested from form with post methods make functionality bellow 
			if ($subcatid) {
				// get doctrine
				$em = $this->getDoctrine()->getManager();
				// get repo
				$repository = $this->getDoctrine()->getRepository(subcategory::class);
				// get products repo 
				$prod_repository = $this->getDoctrine()->getRepository(products::class);

				// get subcategory entity
				$subcategory = $repository->find($subcatid);

				// get subcategory entity
				$on_prod = $prod_repository->findOneBy(array("subcategoryId" => $subcatid), array());

				// if was finded entry on production table with this field catid = give her value 0
				if ($on_prod) {
					$on_prod->setSubcategoryId(null);
				}

				// rm entry
				// $em ->remove($subcategory);
				$subcategory->setEntryStatus(0);
				$em ->flush();

				$cache->clear();

				$this->addFlash(
					'success',
					'Подкатегория была успешно удалена!'
				);

				return $this->redirectToRoute('Subcategorylist');
			} else {
				return $this->redirectToRoute('Subcategorylist');
			}
		}
	}



	/*---------------------------------------------------------------------------------------------------------------------------
	                                                 PRODUCTS ACTIONS
	---------------------------------------------------------------------------------------------------------------------------*/

	/**
	 * @Route("/Productslist", name="Productslist")
	 */
	public function ProductlistAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// get doctrine
			$em = $this->getDoctrine()->getManager();

			/*                   Work with cahce
			------------------------------------------------------------------------------*/
			$cache = new FilesystemCache();

			/*                                                              */
						// Manufacturers
			/*                                                              */

			// get products repo
			$manufacturer_repo = $em->getRepository(Manufacturers::class);

			if(!$cache->get('start.Manufacturers')){
				// get Manufacturers entity
				$manufacturer_list = $manufacturer_repo->findBy([
					'entry_status' => 1
				], ['id' => 'DESC']);

				if ($manufacturer_list) {
					$cache->set('start.Manufacturers', $manufacturer_list);
				}
			}else {
				$manufacturer_list = $cache->get('start.Manufacturers');
			}

			/*                                                              */
						// category
			/*                                                              */

			// get cat repo
			$cat_repo = $em->getRepository(category::class);

			if(!$cache->get('start.category')){
				// get cat entity
				$cat_list = $cat_repo->findBy([
					'entry_status' => 1
				], ['id' => 'DESC']);

				if ($cat_list) {
					$cache->set('start.category', $cat_list);
				}
			}else {
				$cat_list = $cache->get('start.category');
			}


			/*                                                              */
						// subcategory
			/*                                                              */
			// get subcat repo
			$subcat_repo = $em->getRepository(subcategory::class);

			if(!$cache->get('cpanel.subcategory')){
				// get cat entity
				$subcat_list = $subcat_repo->findBy([
					'entry_status' => 1
				], ['id' => 'DESC']);

				if ($subcat_list) {
					$cache->set('cpanel.subcategory', $subcat_list);
				}
			}else {
				$subcat_list =  $cache->get('cpanel.subcategory');
			}


			/*                                                              */
						// products
			/*                                                              */
			/*get products repo*/
			$prod_repo = $em->getRepository(products::class);
			/*get products entity*/

			if(!$cache->get('cpanel.Productslist')){
				$prod_list = $prod_repo->findBy(array('entry_status' => 1), array('id' => 'DESC'));
				if ($prod_list) {
					$cache->set('cpanel.Productslist', $prod_list);
				}
			}else {
				$prod_list =  $cache->get('cpanel.Productslist');
			}

			/*                                                              */
						// Delivery_cost
			/*                                                              */
			$Delivery_cost_repo = $em->getRepository(Delivery_cost::class);
			if(!$cache->get('cpanel.Delivery_cost')){
				$Delivery_cost = $Delivery_cost_repo->findBy([], ['id' => 'ASC']);
				if ($Delivery_cost) {
					$cache->set('cpanel.Delivery_cost', $Delivery_cost);
				}
			}else {
				$Delivery_cost =  $cache->get('cpanel.Delivery_cost');
			}

			// return $this->render('@CP/Default/index.html.twig', array(
			return $this->render('@CP/Production/productslist.html.twig', array(
				'prod_list' => $prod_list,
				'cat_list' => $cat_list,
				'subcat_list' => $subcat_list,
				'manufacturer_list' => $manufacturer_list,
				'Delivery_cost' => $Delivery_cost
			));
		}
	}

	/**
	 * @Route("/Newproduct", name="Newproduct")
	 */
	public function NewproductAction(Request $request)
	{
		// security part of this Action bellow
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {
			// get doctrine
			$em = $this->getDoctrine()->getManager();
			// get user entity
			$products = new products();

			// get cat repo
			$cat_repo = $em->getRepository(category::class);
			$subcat_repo = $em->getRepository(subcategory::class);

			// get cat entity
			$cat_list = $cat_repo->findBy(array('entry_status' => 1), array('id' => 'DESC'));
			$subcat_list = $subcat_repo->findBy(array('entry_status' => 1), array('id' => 'DESC'));

			// get user entity
			$man_repo = $em->getRepository(Manufacturers::class);
			$man_list = $man_repo->findby([
				'entry_status' => 1
			], []);

			// if Action is requested from form with post methods make functionality bellow 
			if ( $request->request->get('product_name') ) {

				// get data from input forms
				$product_name = $request->request->get('product_name');

				// upload images from computer
				$files = $request->files->get('files');

				// upload images from link 
				$alter_img = $request->request->get('alter_img');

				$category = $request->request->get('category');
				$subcategory = $request->request->get('subcategory');
				$manufacturer = $request->request->get('manufacturer');
				$count_dosage = $request->request->get('count_dosage');
				$active_substance = $request->request->get('active_substance');
				$status = $request->request->get('status');
				$product_quantity = $request->request->get('product_quantity');
				$price = $request->request->get('price');
				$content = $request->request->get('wysiwyg_content');


				try {
					// set product_name
					$products->setName($product_name);

					/* Set upload_images */
					$target_dir = $this->container->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR ."web" . DIRECTORY_SEPARATOR . "img" . DIRECTORY_SEPARATOR . "products" . DIRECTORY_SEPARATOR ;
					$images_name = [];

					if (!empty($files)) {
						foreach ($files as $key => $file) {
							$orginal_name = $file->getClientOriginalName();
							$uniq = uniqid();
							$images_name[] = "img/products/". $uniq . $orginal_name;

							$file->move($target_dir, $uniq . $orginal_name);
						}
					}

					/*prod_images*/
					if ($alter_img) {
						$alter_img = explode(",", $alter_img);
						foreach ($alter_img as $key => $url) {
							if (filter_var($url, FILTER_VALIDATE_URL)) {
								$images_name[] = $url;
							}
						}
					}

					$products->setImages($images_name);

					// set category
					$products->setCategoryId($category);

					// set subcategory
					$products->setsubcategoryId($subcategory);

					// set manufacturer
					$products->setManufacturerid($manufacturer);

					// set count_dosage
					$products->setCount_dosage($count_dosage);

					// set active_substance
					$products->setcActive_substance($active_substance);

					// set status
					$products->setStatus($status);

					// set ProductQuantity
					if ($product_quantity > 0) {
						$products->setProductQuantity($product_quantity);

						// set in_stock
						$products->setInStock(1);
					} else {
						$products->setProductQuantity(0);
						// set in_stock
						$products->setInStock(0);
					}

					// set Rating
					$products->setRating(0);

					// set content
					$products->setContent($content);

					// set price
					$products->setPrice($price);

					// set Entry Status
					$products->setEntryStatus(1);

					$em->persist($products);
					$em->flush();

					/*                   Work with cahce
					------------------------------------------------------------------------------*/
					$cache = new FilesystemCache();
					$cache->clear();

					$this->addFlash(
						'success',
						'Продукт был успешно добавлен!'
					);

					return $this->redirectToRoute('Newproduct');

				}
				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'Ошибка дублирования!'
					);
					return $this->redirectToRoute('Newproduct');
				}
			} else { 

				return $this->render('@CP/Production/newproduct.html.twig', array(
					'man_list' => $man_list,
					'cat_list' => $cat_list,
					'subcat_list' => $subcat_list
				));
			}

		}
	}

	/**
	 * @Route("/rmprod/{prodid}", name="rmprod") 
	 */
	public function rmprodAction($prodid)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			if ($prodid) {
				// get doctrine
				$em = $this->getDoctrine()->getManager();

				// get products repo 
				$prod_repository = $this->getDoctrine()->getRepository(products::class);

				// get basket repo 
				$Basket_repository = $this->getDoctrine()->getRepository(Basket::class);

				// get products entity
				$on_prod = $prod_repository->findOneBy(array("id" => $prodid), array());

				// if was finded entry on production table with this field catid = give her value 0
				if ($on_prod) {
					// $em ->remove($on_prod);
					/*----------------------------------------------------
						remove product images from sistem
					----------------------------------------------------*/
					$prod_image = $on_prod->getImages();
					if ($prod_image) {
						foreach ($prod_image as $key => $image) {
							$target_dir = $this->container->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR ."web" . DIRECTORY_SEPARATOR . $image;

							if (file_exists($target_dir)) {
								unlink($target_dir);
							}
						}
					}

					$on_prod->setEntryStatus(0);
					// rm entry
					$em ->persist($on_prod);
					$em ->flush();

					/*                   Work with cahce
					------------------------------------------------------------------------------*/
					$cache = new FilesystemCache();
					$cache->clear();

					/*----------------------------------------------------
							After remove product remove and 
							entry on basket for prevent erros
					----------------------------------------------------*/
					$product_on_basket = $Basket_repository->findBy(array(
						'productid' => $prodid
					), array('id' => 'DESC'));

					if ($product_on_basket) {
						$rm_obj = $Basket_repository->find($product_on_basket[0]->getId());
						$em->remove($rm_obj);
						$em->flush();
					}

					$this->addFlash(
						'success',
						'Продукт был успешно удален!'
					);
				}else {
					$this->addFlash(
						'error',
						'Продукт с таким id не был найден!'
					);
				}

				return $this->redirectToRoute('Productslist');
			} else {
				return $this->redirectToRoute('Productslist');
			}
		}
	}

	/**
	 * @Route("/sortProdBy/{sect}/{id}", name="sortProdBy") 
	 */
	public function sortProdByAction($sect, $id)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// get doctrine
			$em = $this->getDoctrine()->getManager();

			// get Manufacturers repo
			$manufacturer_repo = $em->getRepository(Manufacturers::class);
			// get Manufacturer entity
			$manufacturer_list = $manufacturer_repo->findBy(array(
				'entry_status' => 1
			), array('id' => 'DESC'));

			// get cat repo
			$cat_repo = $em->getRepository(category::class);
			// get cat entity
			$cat_list = $cat_repo->findBy(array(), array('id' => 'DESC'));

			// get subcat repo
			$subcat_repo = $em->getRepository(subcategory::class);
			// get cat entity
			$subcat_list = $subcat_repo->findBy(array(
				'entry_status' => 1
			), array('id' => 'DESC'));

			// get products repo 
			$prod_repository = $this->getDoctrine()->getRepository(products::class);

			if ($sect && $id) {
				try {

					switch ($sect) {
						case 'category':
							$prod_list = $prod_repository->findBy(array('categoryId' => $id, 'entry_status' => 1));
							break;
						case 'subcategory':
							$prod_list = $prod_repository->findBy(array('subcategoryId' => $id, 'entry_status' => 1));
							break;
						case 'manufacturer':
							$prod_list = $prod_repository->findBy(array('manufacturerid' => $id, 'entry_status' => 1));
							break;
					}

					if (empty($prod_list)) {
						$this->addFlash(
							'warning',
							'Не найдено'
						);
					} else {
						$this->addFlash(
							'default',
							'Результаты найдены'
						);
					}
					// return $this->render('@CP/Default/index.html.twig', array(
					return $this->render('@CP/Production/productslist.html.twig', array(
						'prod_list' => $prod_list,
						'cat_list' => $cat_list,
						'subcat_list' => $subcat_list,
						'manufacturer_list' => $manufacturer_list
					));
				}
				/*if appear error with duplicate of entity redirect user to beginning.*/
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'Ошибка дублирования!'
					);
					return $this->redirectToRoute('Productslist');
				}

			} else {
				$this->addFlash(
					'error',
					'sort prod by, check!'
				);
			} /*END if $sec && pordid isset*/

		}/*END SECURITY IF */
	}

	/**
	 * @Route("/prodUpdate", name="prodUpdate") 
	 */
	public function prodUpdateAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// if Action is requested from form with post methods make functionality bellow 
			/*subcatid*/
			if ( $request->request->get('prod_id') ) {
				try{
					// get doctrine
					$em = $this->getDoctrine()->getManager();

					/*                   Work with cahce
					------------------------------------------------------------------------------*/
					$cache = new FilesystemCache();

					// get repo
					$repository = $this->getDoctrine()->getRepository(products::class);
					// get entity
					$prod_id = $request->request->get('prod_id');

					if ($prod_id) {
						$product = $repository->find($prod_id);
					} else {
						$this->addFlash(
							'error',
							'Продукт не был найден.'
						);
						return $this->redirectToRoute('Productslist');
					}

					// UPDATE LOGIC
					/*prod_name*/
					if ( $request->request->get('prod_name') ) {
						$prod_name = $request->request->get('prod_name');
						$product->setName($prod_name);
					}

					/*count_dosage*/
					if ( $request->request->get('count_dosage') ) {
						$count_dosage = $request->request->get('count_dosage');
						$product->setCount_dosage($count_dosage);
					}


					// print "<pre>";
					// var_dump( $request->request->get('zina') );
					// print "</pre>";
					// die;

					/*sale*/
					if ( $request->request->get('sale') ) {
						$sale = $request->request->get('sale');
						$product->setStatus($sale);
					}else {
						$product->setStatus(null);
					} 


					/*rating*/
					if ( $request->request->get('rating') ) {
						$product->setRating(1);
					}else {
						$product->setRating(0);
					} 



					/*category*/
					if ( $request->request->get('category') ) {
						$category = $request->request->get('category');
						$product->setCategoryId($category);
					}

					/*subcategory*/
					if ( $request->request->get('subcategory') ) {
						$subcategory = $request->request->get('subcategory');
						$product->setSubcategoryId($subcategory);
					}

					/*manufacturer*/
					if ( $request->request->get('manufacturer') ) {
						$manufacturer = $request->request->get('manufacturer');
						$product->setManufacturerid($manufacturer);
					}

					/*prod_descr*/
					$prod_descr = $request->request->get('prod_descr');
					$product->setContent($prod_descr);

					/*prod_images*/
					if ( $request->request->get('alter_img') ) {
						$curresnt_images = $product->getImages();
						$alter_img = $request->request->get('alter_img');
						$alter_img = explode(",", $alter_img);


						// link validation
						$link_valid = [];
						foreach ($alter_img as $key => $url) {
							if (filter_var($url, FILTER_VALIDATE_URL)) {
								$link_valid[] = $url;
							}
						}
						$alter_img = $link_valid;

						//merge results
						if (is_array($curresnt_images)) {
							$new_prod_img = array_merge($curresnt_images , $alter_img);
						}else {
							$new_prod_img = $alter_img;
						}

						$product->setImages($new_prod_img);
					}

					/*price*/
					if ( $request->request->get('price') ) {
						// set old price
						$old_price = $product->getPrice();
						$product->setOldprice($old_price);

						// set new price
						$price = $request->request->get('price');
						$product->setPrice($price);
					}

					/*   Discount  */
					// Prepare data for usage

					$curent_price = (float)$product->getPrice();
					$discount = $request->request->get('discount');

					if (!empty($discount) || gettype($discount) == 'string' && $discount == '0') {

						$pdisc = [
							"real_price" => $curent_price,
							"discount" => (float)$request->request->get('discount')
						];

						if (gettype($discount) == 'string' && $discount == '0') {
							$cur_disc = $product->getDiscount();
							$new_price = $cur_disc['real_price'];

						}else {
							$amount_of_discount = ((float)$discount / 100) * $curent_price;
							$new_price = $curent_price - $amount_of_discount;
						}

						// Insert on db
						$product->setPrice($new_price);
						$product->setDiscount($pdisc);

					}

					$product->setDate();

					$em ->persist($product);

					$em->flush();

					$cache->clear();

					$this->addFlash(
						'success',
						'Данные были обновлены!'
					);
					return $this->redirectToRoute('Productslist');
				}
				/*if appear error with duplicate of entity redirect user to beginning.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'ошибка дублирования.'
					);
					return $this->redirectToRoute('Productslist');
				} /*end catch duplicate*/
			}/*END if Action is requested*/


			return $this->redirectToRoute('Productslist');
		} /*END access checker*/
	}


	/**
	 * @Route("/rmvProdImg", name="rmvProdImg")
	 */
	public function rmvProdImgAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		$img_href = $request->request->get('img_href');
		$prod_id = $request->request->get('prod_id');

		// if parametres of search is not defined
		if ( empty($img_href) || empty($prod_id) ) {
			$arr_data = [
				'type' => 'error',
				'output' => 'Не заданы параметры для поиска'
			];
			return new JsonResponse($arr_data);
		}

		try{
			// get doctrine
			$em = $this->getDoctrine()->getManager();
			// get repo
			$repository = $this->getDoctrine()->getRepository(products::class);
			$product = $repository->find($prod_id);

			$prod_images = $product->getImages();

			// on bellow line  posiible will appear error
			$img_arr_pos = array_search($img_href, $prod_images );

			// unset href of image from arr prod images
			unset($prod_images[$img_arr_pos]);

			// rewrite array for prevent errors with key of array
			$new_prod_images = [];
			foreach ($prod_images as $key => $value) {
				$new_prod_images[] = $value;
			}
			$prod_images = $new_prod_images;

			// check if image is drived on server, remove hes
			$target_dir = $this->container->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR ."web" . DIRECTORY_SEPARATOR . $img_href;

			if (file_exists($target_dir)) {
				unlink($target_dir);
			}

			// Set new prod img arr
			$product->setImages($prod_images);
			$em->persist($product);
			$em->flush();

			$arr_data = [
				'type' => 'success',
				'output' => 'Фото было Успешно удаленно '
			];

			return new JsonResponse($arr_data);
		}
		catch(DBALException $e){
			$arr_data = [
				'type' => 'error',
				'output' => $e->getMessage()
			];
			return new JsonResponse($arr_data);
		}

	}

	/**
	 * @Route("/addProdImg", name="addProdImg")
	 */
	public function addProdImgAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();
		// get repo
		$repository = $this->getDoctrine()->getRepository(products::class);

		$prod_id = $request->request->get('prodid_img');
		$prod_files = $request->files->get('prod_files');

		// if parametres of search is not defined
		if ( empty($prod_files) || empty($prod_id) ) {
			$this->addFlash(
				'error',
				'Не заданы параметры для добавления'
			);
			return $this->redirectToRoute('Productslist');
		}

		try{

			/* --------------------------------------
				Check if not exceeded of images
			----------------------------------------*/
			// get needed product
			$product = $repository->find($prod_id);

			/*--------------------------
			LIMIT : if curent count of product plus
			uploaded count of image, more what 4 return error message
			-----------------------*/
			$limit = sizeOf($product->getImages()) + sizeOf($prod_files);

			if ( $limit > 4 ) {
				$this->addFlash(
					'error',
					'Превышено максимальное количество фото'
				);
				return $this->redirectToRoute('Productslist');
			}

			/* --------------------------------
				Prepare uploaded images
			----------------------------------*/
			/* Set upload_images directory */
			$target_dir = $this->container->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR ."web" . DIRECTORY_SEPARATOR . "img" . DIRECTORY_SEPARATOR . "products" . DIRECTORY_SEPARATOR;
			$images_name = [];

			foreach ($prod_files as $key => $file) {
				$orginal_name = $file->getClientOriginalName();

				// uniqid
				$uniq = uniqid();
				// file extention
				$file_extention = $file->getMimeType();
				$file_ext = explode('/', $file_extention)[1];

				$images_name[] = "img/products/" . $uniq . '.' . $file_ext;

				$file->move($target_dir , $uniq . '.' . $file_ext);
			}

			/* ------------------------------------
				Prepare current product images
			-------------------------------------*/
			if (!$product->getImages() || empty($product->getImages()) ) {
				$product->setImages($images_name);
			}else {
				$new_prod_images = array_merge($images_name, $product->getImages());
				$product->setImages($new_prod_images);
			}

			/* --------------------------------
					Inserting
			----------------------------------*/
			$em->persist($product);
			$em->flush();

			$this->addFlash(
				'success',
				'Фото успешно добавлено'
			);
			return $this->redirectToRoute('Productslist');
		}
		catch(DBALException $e){
			$this->addFlash(
				'success',
				$e->getMessage()
			);
			return $this->redirectToRoute('Productslist');
		}
	}


}/*END OF CLASS*/
