<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Entity\subscribers;

class SubscribersController extends Controller
{
	/**
	 * @Route("/productsubslist")
	 */
	public function productsubslistAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {
			// get doctrine
			$em = $this->getDoctrine()->getManager();

			// get user entity
			$subscribers_repo = $em->getRepository(subscribers::class);
			$prod_subs_list = $subscribers_repo->findBy(
				array(
				'target' => 'products'
			), array(
				'id' => 'DESC'
			));

			return $this->render('@CP/Subscribers/productsubslist.html.twig', array(
				'prod_subs_list' => $prod_subs_list
			));
		}
	}

	/**
	 * @Route("newsletsubslist")
	 */
	public function newsletsubsAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {
			// get doctrine
			$em = $this->getDoctrine()->getManager();

			// get user entity
			$subscribers_repo = $em->getRepository(subscribers::class);
			$news_subs_list = $subscribers_repo->findBy(
				array(
				'target' => 'newsletter'
			), array(
				'id' => 'DESC'
			));

			return $this->render('@CP/Subscribers/newsletsubslist.html.twig', array(
				"news_subs_list" => $news_subs_list,
			));
		}

	}

	/**
	 * @Route("/notifyproductsubs")
	 */
	public function notifyproductsubsAction()
	{
		return $this->render('@CP/Subscribers/notifyproductsubs.html.twig', array(
			// ...
		));
	}

	/**
	 * @Route("/notifynewsletsubs")
	 */
	public function notifynewsletsubsAction()
	{
		return $this->render('@CP/Subscribers/notifynewsletsubs.html.twig', array(
			// ...
		));
	}

}
