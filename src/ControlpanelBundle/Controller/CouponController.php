<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Session\Session;

use \Datetime;

// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
// Catch db extention
use Doctrine\DBAL\DBALException;

use AppBundle\Entity\Coupon; 

class CouponController extends Controller
{
	/**
	 * @Route("/listCoupon", name="listCoupon")
	 */
	public function listCouponAction()
	{

		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();
		$Coupon_repo = $em->getRepository(Coupon::class);

		$list_coupon = $Coupon_repo->findBy([
			'entry_status' => 1
		], ['id' => 'DESC']);


		return $this->render('@CP/Coupon/list_coupon.html.twig', array(
			'list_coupon' => $list_coupon
		));
	}

	/**
	 * @Route("/newCoupon")
	 */
	public function newCouponAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();

		$coupon_name = $request->request->get('coupon_name');
		$fromDate = $request->request->get('fromDate');
		$toDate = $request->request->get('toDate');
		(float)$discount_percent = $request->request->get('discount_percent');

		/*----------------------------------------------------
			check if all field is not empty
		-----------------------------------------------------*/

		if (empty($coupon_name) || empty($fromDate) || empty($toDate) || empty($discount_percent)) {
			$this->addFlash(
				'error',
				"Проверьте чтобы все поля были заполнены"
			);

			return $this->redirectToRoute('listCoupon');
		}

		/*----------------------------------------------------
			check if all field is not empty
		-----------------------------------------------------*/
		try{

			$Coupon = new Coupon();

			$Coupon->setName($coupon_name);

			// fromDate
			$Coupon->setFromdate($fromDate);

			// toDate
			$Coupon->setTodate($toDate);

			// setPercent
			$Coupon->setPercent($discount_percent);

			$em->persist($Coupon);
			$em->flush();

			$this->addFlash(
				'success',
				'Купон был успешно создан'
			);
			return $this->redirectToRoute('listCoupon');

		}

		/*if appear error with duplicate of entity redirect user to beginning.
		need to make notify*/
		catch(UniqueConstraintViolationException $e) {
			$this->addFlash(
				'error',
				'Купон с таким названием уже существует'
			);

			return $this->redirectToRoute('listCoupon');
		}
		catch(DBALException $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('listCoupon');
		}

		return $this->redirectToRoute('listCoupon');
	}

	/**
	 * @Route("/rmvCoupon/{couponid}")
	 */
	public function rmvCouponAction($couponid)
	{

		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();

		$Coupon_repo = $em->getRepository(Coupon::class);
		$Coupon = $Coupon_repo->find($couponid);

		if (!$Coupon) {
			$this->addFlash(
				'error',
				'Не найден купон с таким ИД'
			);
			return $this->redirectToRoute('listCoupon');
		}

		$Coupon_name = $Coupon->getName();

		try{

			$Coupon->setEntryStatus(0);
			$em->persist($Coupon);
			$em->flush();

			$this->addFlash(
				'success',
				'Купон :' . $Coupon_name . " был успешно деактивирован" 
			);
			return $this->redirectToRoute('listCoupon');

		}
		catch(DBALException $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('listCoupon');
		}
	}


	/**
	 * @Route("/applyCoupon", name="applyCoupon")
	 */
	public function applyCouponAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_USER")) {
			return $this->redirectToRoute('redirectrole');
		}

		$coupon_name = $request->request->get('coupon_name');

		if (!$coupon_name) {
			$this->addFlash(
				'error',
				'Не указано имя купона'
			);
			return $this->redirectToRoute('shop_basket');
		}

		$em = $this->getDoctrine()->getManager();
		$Coupon_repo = $em->getRepository(Coupon::class);

		$session = new Session();

		$Coupon = $Coupon_repo->findBy(['name' => $coupon_name], [])[0];

		if (!$Coupon) {
			$this->addFlash(
				'error',
				'Не найден купон с таким названием'
			);
			return $this->redirectToRoute('shop_basket');
		}

		$entry_status = $Coupon->getEntryStatus();

		/*-------------------------------------------------------
				Check if is actual
		--------------------------------------------------------*/

		if ($entry_status !== true) {
			$this->addFlash(
				'error',
				'Недействительный купон'
			);
			return $this->redirectToRoute('shop_basket');
		}

		/*-------------------------------------------------------
				Check at validity
		--------------------------------------------------------*/
		$cd = new Datetime();

		$fromDate = $Coupon->getFromdate();
		$todate = $Coupon->getTodate();

		$available_days = $fromDate->diff($todate);

		// $test = $todate->diff($cd);
		$remain_days = $todate->diff($cd)->format('%r%a');

		if ($remain_days > 0 ) {
			$Coupon->setEntryStatus(0);
			$em->persist($Coupon);
			$em->flush();

			$this->addFlash(
				'error',
				'Период активности купона истёк.'
			);
			return $this->redirectToRoute('shop_basket');
		}

		$session->set('coupon', $Coupon);

		$this->addFlash(
			'success',
			'Купон успешно добавлен!'
		);
		return $this->redirectToRoute('shop_basket');

	}

}
