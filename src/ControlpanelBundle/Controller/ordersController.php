<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// Entity
use AppBundle\Entity\Orderr;
use AppBundle\Entity\products;

// Catch db extention
use Doctrine\DBAL\DBALException;

class ordersController extends Controller
{
	/**
	 * @Route("/allorders", name="allorders")
	 */
	public function allordersAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN") && !$this->get('security.authorization_checker')->isGranted("ROLE_COURIER")) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();

		/*------------------------------------------------------------
						PREPARE DATA FOR USING 
		--------------------------------------------------------------*/

		/* --------------Orders repo --------- */
		$Oreder_repo = $this->getDoctrine()->getRepository(Orderr::class);
		$orders_statistics = $Oreder_repo->ordersStatistics();


		if ($this->get('security.authorization_checker')->isGranted('ROLE_COURIER')) {
			if ($this->getUser()->getMain() == 1) {
				$orders = $Oreder_repo->findBy([
					'entry_status' => 1,
					'paymentStatus' => 'paid'
				], []);
			}else {

				$this->addFlash(
					'error',
					'Вы не являетесь главным курьером для получения заказов. Свяжитесь с администрацией.'
				);
				$orders = [];
			}

		} else {
			$orders = $Oreder_repo->findBy([
				'entry_status' => 1
			], ['id' => 'DESC']);
		}

		return $this->render('@CP/orders/allorders.html.twig', array(
			'orders' => $orders,
			'orders_statistics' => $orders_statistics
		));
	}

	/**
	 * @Route("/updateOrder", name="updateOrder")
	 */
	public function updateOrderAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN") && !$this->get('security.authorization_checker')->isGranted("ROLE_COURIER")) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();

		/*------------------------------------------------------------
						PREPARE DATA FOR USING 
		--------------------------------------------------------------*/
		/* --------------Orders repo --------- */
		$Oreder_repo = $this->getDoctrine()->getRepository(Orderr::class);

		/*--------------- get uniqid -------------- */
		$uniqid = $request->request->get('uniqid');

		/*--------------- get tackCode -------------- */
		$tackCode = $request->request->get('tackCode');

		/*--------------- get payment_status -------------- */
		$payment_status = $request->request->get('payment_status');

		/*--------------- get delivery_status -------------- */
		$delivery_status = $request->request->get('delivery_status');

		/*--------------- get order_status -------------- */
		$order_status = $request->request->get('order_status');

		/*   rate */
		$rate = $request->request->get('rate');

		// // if not defined uniq id
		if (!$uniqid) {
			$this->addFlash(
				'error',
				'Ошибка системы, не указан UNIQ_ID!'
			);

			return $this->redirectToRoute('allorders');
		}

		try{
			// Find entry on table orders with defined uniq id
			$order = $Oreder_repo->findOneBy([
				'uniqid' => $uniqid,
				'entry_status' => 1
			], []);

			if (empty($order)) {

				$this->addFlash(
					'error',
					'Ошибка системы, не найден заказ с таким UNIQ_ID!'
				);

				return $this->redirectToRoute('allorders');
			}


			/*--------------- current_date -------------- */
			$current_date = $order->getCurrentDate();

			/*------------- difference between current date and create date---------- */
			$cd = (int)date('d');
			$sd = (int)$order->getDate()->format('d');
			$res = abs($cd - $sd);



			/*------------------------------------------------------------
							INSERT DATA ON DB
			--------------------------------------------------------------*/
			/*---------------  Set track code  ----------------*/
			$order->setTrackСode($tackCode);

			/*---------------  Set track inserted date  ----------------*/
			$order->setTrackDate();


			/*--------------------  Set rate ------------------*/
			$order->setRate($rate);

			/*--------------------  payment_status ------------------*/
			if ($payment_status) {
				switch ($payment_status) {
					case 'paid':
							$order->setPaymentStatus($payment_status);
							$order->setPayment_date($current_date);

							// no mistakes on order
							$order->setOrderStatus('accepted ');

							// if order is paid , sent her to courier 
							$order->setDeliveryStatus('to_courier');
						break;

					case 'pending_payment':
							/*if order is paid but admin manual change payment status don't remove
							 her from sistem just update date of order*/
							if ($order->getPaymentStatus() == "paid" || $res > 2) {
								$order->setDate();
							}

							// set current payment status
							$order->setPaymentStatus($payment_status);
							// set date when is changed status of payment of order
							$order->setCustomPayment_date(null);

							/*if adminisrator manual set stauts of payment
							change and delivery status*/
							$order->setDeliveryStatus(null);

							// no mistakes on order
							$order->setOrderStatus('accepted ');

						break;
					case 'pending_confirmation':
							/*if order is paid but admin manual change payment status don't remove
							 her from sistem just update date of order*/
							if ($order->getPaymentStatus() == "paid") {
								$order->setDate();
							}

							$order->setPaymentStatus($payment_status);
							$order->setCustomPayment_date(null);

							// if user manual set stauts of payment
							$order->setDeliveryStatus(null);
							// no mistakes on order
							$order->setOrderStatus('accepted ');
						break;

					/*--------------------  order_status ------------------*/
					case 'order_failed':
							/*if order is paid but admin manual change payment status don't remove
							 her from sistem just update date of order*/
							if ($order->getPaymentStatus() == "paid") {
								$order->setDate();
							}

							$order->setOrderStatus($payment_status);

							// if user manual set stauts of payment
							$order->setDeliveryStatus(null);

						break;

					case 'accepted':
							$cd = (int)date('d');
							$sd = (int)$order->getDate()->format('d');
							$res = abs($cd - $sd);

							/* if current date - create date will return result more what 2 day 
							update create date for prevent removing order*/
							if ($res > 2) {
								$order->setDate();
							}

							// no mistakes on order
							$order->setOrderStatus($payment_status);
							$order->setDeliveryStatus('to_courier');

						break;

					default:
						$order->setPaymentStatus($payment_status);
						break;
				} /*./ END switch*/
			} /*./ END if*/

			/*--------------------  delivery_status ------------------*/
			if ($delivery_status && $payment_status !== 'pending_payment' && $payment_status !== 'pending_confirmation' &&  $order->getOrderStatus() !== 'order_failed') {

				switch ($delivery_status) {
					case 'order_failed':

						/*if order is paid but admin manual change payment status don't remove
						 her from sistem just update date of order*/
						if ($order->getPaymentStatus() == "paid") {
							$order->setDate();
						}

						$order->setOrderStatus($delivery_status);

						// if user manual set stauts of payment
						$order->setDeliveryStatus(null);

						break;

					default:
						$order->setDeliveryStatus($delivery_status);
						break;
				}
			}

			$em->persist($order);
			$em->flush();


			$this->addFlash(
				'default',
				'Данные заказа были успешно обновлены'
			);

			return $this->redirectToRoute('allorders');

		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);

			return $this->redirectToRoute('allorders');
		}

	}

	/**
	 * @Route("/removeOrder", name="removeOrder")
	 */
	public function removeOrderAction(Request $request)
	{
		/*-----------------------------------------------
				Security checker
		-----------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		/*-----------------------------------------------
				Prepare data for usage
		-----------------------------------------------*/

		$order_id = $request->request->get('order_id');

		if (empty($order_id)) {
			$arr_data = [
				'type' => 'error',
				'output' => 'Не были заданны параметры для поиска'
			];
			return new JsonResponse($arr_data);
		}

		$em = $this->getDoctrine()->getManager();
		$Oreder_repo = $this->getDoctrine()->getRepository(Orderr::class);
		$products_repo = $this->getDoctrine()->getRepository(products::class);
		$order = $Oreder_repo->find($order_id);

		if (empty($order)) {
			$arr_data = [
				'type' => 'error',
				'output' => 'Не найдено результатов с заданными параметрами поиска'
			];
			return new JsonResponse($arr_data);
		}

		try{
			/*-----------------------------------------------
					Remove order from sistem
			-----------------------------------------------*/
			// $order->setEntryStatus(0);
			$em->remove($order);
			$em->flush();


			/*-----------------------------------------------
					if order is removed but not paid 
					return products on stock
			-----------------------------------------------*/
			if ($order->getPaymentStatus() !== "paid") {
				$order_prod = $order->getProducts()['products'];
				$order_courses = $order->getProducts()['course'];

				/*---------------------------
					Loop for order_prod
				---------------------------*/
				foreach ($order_prod as $key => $prod) {

					// get actual product count
					$actual_prod = $products_repo->find($prod['productid']);
					if ($actual_prod) {
						$actual_prod_quantity = $actual_prod->getProductQuantity();
						$on_order_prod_quantity = $prod['count'];

						$new_prod_quantity = $actual_prod_quantity + $on_order_prod_quantity;

						$actual_prod->setProductQuantity($new_prod_quantity);
						$em->persist($actual_prod);
						$em->flush();
					}
				}

				/*---------------------------
					Loop for order_prod
				---------------------------*/
				foreach ($order_courses as $course) {

					foreach ($course[0]['content'] as $course_prod) {
						// get actual product count
						$c_actual_prod = $products_repo->find($course_prod['prodid']);

						$c_actual_prod_quantity = $c_actual_prod->getProductQuantity();

						if ($course['count'] > 1 ) {
							$c_on_order_prod_quantity = $course_prod['count'] * $course['count'];
						}else {
							$c_on_order_prod_quantity = $course_prod['count'];
						}

						$c_new_prod_quantity = $c_actual_prod_quantity + $c_on_order_prod_quantity;

						$c_actual_prod->setProductQuantity($c_new_prod_quantity);
						$em->persist($c_actual_prod);
						$em->flush();

					} /*./ End loop order course products*/

				} /*./ End loop courses*/
			}

			$arr_data = [
				'type' => 'success',
				'output' => 'Заказ был успешно удалён'
			];

			return new JsonResponse($arr_data);
		}
		catch(DBALException $e){
			$arr_data = [
				'type' => 'error',
				'output' => $e->getMessage()
			];
			return new JsonResponse($arr_data);
		}
	}

}
