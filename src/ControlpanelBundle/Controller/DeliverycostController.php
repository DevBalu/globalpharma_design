<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
// Catch db extention
use Doctrine\DBAL\DBALException;

use AppBundle\Entity\Delivery_cost; 
// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class DeliverycostController extends Controller
{

	/**
	 * @Route("/newDeliverycost", name="newDeliverycost")
	 */
	public function newDeliverycostAction(Request $request)
	{

		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();

		$price_delimiter = $request->request->get('price_delimiter');
		$delivery_cost = $request->request->get('delivery_cost');

		if (empty($price_delimiter)) {
			$this->addFlash(
				'error',
				'Поле "ценовым разделителем" не было заполнено'
			);

			return $this->redirectToRoute('Productslist');
		}

		if ( empty($delivery_cost)) {
			$this->addFlash(
				'error',
				'Поле "цена за доставку" не было заполнено'
			);

			return $this->redirectToRoute('Productslist');
		}


		$Delivery_cost_repo = $em->getRepository(Delivery_cost::class);

		$newDelveryCost = new Delivery_cost();

		try {


			$newDelveryCost->setPriceDelimiter($price_delimiter);
			$newDelveryCost->setDeliveryCost($delivery_cost);
			$em->persist($newDelveryCost);
			$em->flush();

			$cache->clear();

			$this->addFlash(
				'success',
				'Данные были успешно добавлены'
			);
			return $this->redirectToRoute('Productslist');

		}
		/*if appear error with duplicate of entity redirect user to beginning.
		need to male notify */
		catch(UniqueConstraintViolationException $e) {
			$this->addFlash(
				'error',
				'Ошибка дублирования, такой ценовой разделитель уже существует'
			);

			return $this->redirectToRoute('Productslist');

		}
		catch(DBALException $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('Productslist');

		}

	}

	/**
	 * @Route("/rmvDeliverycost/{dcid}")
	 * @Route("/rmvDeliverycost", )
	 */
	public function rmvDeliverycostAction($dcid = 1)
	{
		if ($dcid == null) {
			$this->addFlash(
				'error',
				'Не задан ид записи для поиска'
			);
			return $this->redirectToRoute('Productslist');
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();


		$Delivery_cost_repo = $em->getRepository(Delivery_cost::class);
		$Delivery_cost = $Delivery_cost_repo->find($dcid);

		if (!$Delivery_cost) {
				$this->addFlash(
					'error',
					'Запись с таким id не была найдена'
				);
				return $this->redirectToRoute('Productslist');
		}

		try {

			$em ->remove($Delivery_cost);
			$em ->flush();

			$cache->clear();

			$this->addFlash(
				'success',
				'ценовой разделитель успешно удалён'
			);
			return $this->redirectToRoute('Productslist');
		}
		catch(DBALException $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('Productslist');

		}

	}

}
