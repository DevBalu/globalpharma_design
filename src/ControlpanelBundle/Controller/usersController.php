<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\User;

/*for encode password*/
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class usersController extends Controller
{

	/*---------------------------------------------------------------------------------------------------------------------------
	                                              COURIERS ACTIONS
	--------------------------------------------------------------------------------------------------------------------*/

		/**
	 * @Route("/Couriers", name="Couriers")
	 */
	public function CouriersAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// example of query builder on symfony 
			$query = $this->getDoctrine()->getManager()
			->createQuery('SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.isActive = 1 ORDER BY u.id DESC'
			)->setParameter('role', '%"ROLE_COURIER"%' );

			$couriers = $query->getResult();

			return $this->render('@CP/users/courierslist.html.twig', array(
				'couriers' 	=> $couriers,
				'us_role'	=> 'courier',
			));
		}
	}

	/**
	 * @Route("/courierupdate", name="courierupdate")
	 */
	public function courierupdateAction(Request $request, UserPasswordEncoderInterface $encoder)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			try {
				// from form
				$courier_id = $request->request->get('courier_id');
				$courier_name = $request->request->get('courier_name');
				$courier_password = $request->request->get('courier_password');
				$main_courier = $request->request->get('main_courier');

				if (!empty($courier_id) ) {
					// get doctrine
					$em = $this->getDoctrine()->getManager();

					// get user entity
					$user = $em->getRepository(User::class)->find($courier_id);

					if ($main_courier) {
						$mains = $em->getRepository(User::class)->findby(
							['main' => 1],
							[]
						);


						foreach ($mains as $key => $main) {

							if (in_array('ROLE_COURIER', $main->getRoles() )) {
								$main->setMain(0);
								$em->persist($main);
								$em->flush();
							}

						}


						// print"<pre>";
						// print_r(in_array('ROLE_COURIER', $main->getRoles() ) );
						// print"</pre>";
						// die;

						$user->setMain(1);
					}



					if (!empty($courier_password)) {
						// encode new pass
						$encoded_pass = $encoder->encodePassword($user, $courier_password);
						$user->setPassword($encoded_pass);

					} else if ( !empty($courier_name) ){
						$user->setUsername($courier_name);
					}

					$em->flush();

					return $this->redirectToRoute('courierupdate');
				} else {
					return $this->redirectToRoute('Couriers');
				}
			}
			/*if appear error with duplicate of entity redirect user to registration page.
			need to make notify */
			catch(UniqueConstraintViolationException $e) {
				return $this->redirectToRoute('Couriers');

				$this->addFlash(
					'error',
					'Ошибка дублирования!'
				);
			}
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------------
	                                              MANAGER ACTIONS
	-------------------------------------------------------------------------------------------------------------------*/


	/**
	 * @Route("/Managers", name="managers")
	 */
	public function ManagersAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// example of query builder on symfony 
			$query = $this->getDoctrine()->getManager()
			->createQuery('SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.isActive = 1 ORDER BY u.id DESC'
			)->setParameter('role', '%"ROLE_ADMIN"%' );

			$users_list = $query->getResult();


			$query_superadmin = $this->getDoctrine()->getManager()
			->createQuery('SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.isActive = 1 ORDER BY u.id DESC'
			)->setParameter('role', '%"ROLE_SUPER_ADMIN"%' );
			$super_admins = $query_superadmin->getResult();

			return $this->render('@CP/users/managerslist.html.twig', array(
				'super_admins'	=> $super_admins,
				'users_list'	=> $users_list,
				'us_role'		=> 'managers',
			));
		}

	}




	/**
	 * @Route("/managerupdate", name="mupdate")
	 */
	public function managerupdateAction(Request $request,  UserPasswordEncoderInterface $encoder)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		try {
			// from form
			$admin_id = $request->request->get('admin_id');
			$client = $request->request->get('client');

			$admin_name = $request->request->get('admin_name');
			$admin_password = $request->request->get('admin_password');

			if (!empty($admin_id) ) {
				// get doctrine
				$em = $this->getDoctrine()->getManager();

				// get user entity
				$user = $em->getRepository(User::class)->find($admin_id);

				if (!empty($admin_password)) {
					// encode new pass
					$encoded_pass = $encoder->encodePassword($user, $admin_password);
					$user->setPassword($encoded_pass);
				}

				if ( !empty($admin_name) ){
					$user->setUsername($admin_name);
				}

				$em->persist($user);
				$em->flush();

				$this->addFlash(
					'success',
					'Данные пользователя были успешно обновлены!'
				);

				if (!empty($client) ) {
					return $this->redirectToRoute('clients');
				}else {
					return $this->redirectToRoute('managers');
				}

			} else {
				if (!empty($client) ) {
					return $this->redirectToRoute('clients');
				}else {
					return $this->redirectToRoute('managers');
				}
			}
		}

		/*if appear error with duplicate of entity redirect user to registration page.
		need to make notify */
		catch(UniqueConstraintViolationException $e) {
			$this->addFlash(
				'error',
				'Ошибка дублирования!'
			);
			return $this->redirectToRoute('managers');
		}

	}

	/*---------------------------------------------------------------------------------------------------------------------------
	                                              SUPER ADMIN ACTIONS
	---------------------------------------------------------------------------------------------------------------------------*/

	/**
	 * @Route("/sadm", name="sadm")
	 */
	public function sadmAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			$query_superadmin = $this->getDoctrine()->getManager()
			->createQuery('SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.isActive = 1 ORDER BY u.id DESC'
			)->setParameter('role', '%"ROLE_SUPER_ADMIN"%' );
			$super_admins = $query_superadmin->getResult();

			return $this->render('@CP/users/superadministrators.html.twig', array(
				'super_admins'	=> $super_admins,
				'us_role'		=> 'managers',
			));
		}

	}

	/**
	 * @Route("/saupdate", name="saupdate")
	 */
	public function saupdateAction(Request $request,  UserPasswordEncoderInterface $encoder)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			try {
				// from form
				$admin_id = $request->request->get('admin_id');
				$admin_name = $request->request->get('admin_name');
				$admin_password = $request->request->get('admin_password');

				if (!empty($admin_id) ) {
					// get doctrine
					$em = $this->getDoctrine()->getManager();

					// get user entity
					$user = $em->getRepository(User::class)->find($admin_id);

					if (!empty($admin_password)) {
						// encode new pass
						$encoded_pass = $encoder->encodePassword($user, $admin_password);
						$user->setPassword($encoded_pass);

					} else if ( !empty($admin_name) ){
						$user->setUsername($admin_name);
					}

					$em->flush();

					$this->addFlash(
						'success',
						'Данные пользователя были успешно обновлены!'
					);

					return $this->redirectToRoute('sadm');
				} else {
					$this->addFlash(
						'error',
						'Не указан User id!'
					);
					return $this->redirectToRoute('sadm');
				}
			}

			/*if appear error with duplicate of entity redirect user to registration page.
			need to make notify */
			catch(UniqueConstraintViolationException $e) {
				$this->addFlash(
					'error',
					'Ошибка дублирования!'
				);
				return $this->redirectToRoute('sadm');
			}
		}

	}

	/*---------------------------------------------------------------------------------------------------------------------------
	                                              CLIENTS ACTIONS
	---------------------------------------------------------------------------------------------------------------------------*/

	/**
	 * @Route("/Clients", name="clients")
	 */
	public function ClientsAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			// example of query builder on symfony 
			$query = $this->getDoctrine()->getManager()
			->createQuery('
				SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.isActive = 1 ORDER BY u.id DESC'
			)->setParameter('role', '%[]%');

			$users_list = $query->getResult();

			return $this->render('@CP/users/clientslist.html.twig', array(
				'users_list'	=> $users_list,
				'us_role'		=> 'clients',
			));
		}
	}


	/*---------------------------------------------------------------------------------------------------------------------------
	                                              COMUN ACTIONS
	----------------------------------------------------------------------------------------------------------------------*/

	/**
	 * @Route("/rmuser/{role}/{userid}", name="rmuser")
	 */
	public function rmuserAction($role, $userid)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		} else {

			if ($role && $userid) {
				// get doctrine
				$em = $this->getDoctrine()->getManager();
				// get category repo 
				$user_repository = $this->getDoctrine()->getRepository(User::class);

				// get user entity
				$on_user = $user_repository->find($userid);

				if ($on_user) {
					$on_user->isDisabled();
					$em ->persist($on_user);
					$em ->flush();

					$this->addFlash(
						'success',
						'Пользователь успешно был удален!'
					);
				} else {

					$this->addFlash(
						'error',
						'Пользователь с таким ид не был найден!'
					);
				}


				if ($role == "clients") {
					return $this->redirectToRoute('clients');
				}

				switch ($role) {
					case 'courier':
						return $this->redirectToRoute('Couriers');
						break;
					case 'clients':
						return $this->redirectToRoute('clients');
						break;
					case 'manager':
						return $this->redirectToRoute('managers');
						break;
					default:
						return $this->redirectToRoute('cpanel');
						break;
				}
			}

			$this->addFlash(
				'error',
				'Пользователь не был найден в системе!'
			);

			return $this->redirectToRoute('clients');
		}/*END access if*/

	}


}/*END CLASS*/
