<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// Entity 
use AppBundle\Entity\Course; 
use AppBundle\Entity\products; 
use AppBundle\Entity\Basket;
use AppBundle\Entity\favorites;

// sevices
use AppBundle\Service\pChecker;

// Catch error when insert on DB
use Doctrine\DBAL\DBALException;

class CourseController extends Controller
{ 
	/*-------------------------------------------------------------------------------------------------------------------
								Custom Methods AG
	---------------------------------------------------------------------------------------------------------------------*/

	public function coursesRelevance()
	{
		$em = $this->getDoctrine()->getManager();
		// Popular products
		$products_repo = $em->getRepository(products::class);
		$Course_repo = $em->getRepository(Course::class);

		/*-----------------------------------------------------------------------------
						COURSE RELEVANCE
		------------------------------------------------------------------------------*/
		$products_list = $Course_repo->coursesRelevance();

		// make course instock = 0 if at least one product of course not in stock
		$courses_not_instock = [];
		foreach ($products_list as $key => $product) {

			$prod_ent = $products_repo->findBy([
				'id' => $product['pid'],
				'inStock' => 1
			],[]);

			$prod_ent_rmved = $products_repo->findBy([
				'id' => $product['pid'],
				'entry_status' => 0
			],[]);

			// if at least one product of course is removed notify customer
			if ($prod_ent_rmved) {
				$cours_ent = $Course_repo->find($product['cid']);

				$warning_message = ['product_removed' => 'продукт(ы) нах-ся в курсе был(и) удалён(ы)'];
				$cours_ent->setWarning($warning_message);
				$em->persist($cours_ent);
				$em->flush();
			}

			if (!$prod_ent || $prod_ent_rmved) {
				$courses_not_instock[] = $product['cid'];
			}
		}

		$courses_not_instock = array_unique($courses_not_instock);
		foreach ($courses_not_instock as $key => $courseid) {
			$cours_ent = $Course_repo->find($courseid);
			$cours_ent->setInstock(0);
			$em->persist($cours_ent);
			$em->flush();
		}


		// check if all product of course is in stock change course status "instock" = 1
		$all_courses =  $Course_repo->findBy(['entry_status' => 1, ],[]);

		foreach ($all_courses as $key => $course) {
			$count = 0;

			foreach ($course->getContent() as $key => $cprod) {

				$prod_ent = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'inStock' => 0,
					'entry_status' => 1
				],[]);

				$prod_ent_rmved = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'entry_status' => 0
				],[]);

				if ($prod_ent || $prod_ent_rmved) {
					$count++;
				}
			}

			if ($count == 0) {
				$change_course = $Course_repo->find($course->getId());
				$change_course->setInstock(1);
				$em->persist($change_course);
				$em->flush();
			}
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------
								ACTIONS OF CONTROLLER
	---------------------------------------------------------------------------------------------------------------------*/
	/**
	* @Route("/courseDetail/{cid}", name="courseDetail")
	*/
	public function courseDetailAction(Request $request,  pChecker $pChecker, $cid)
	{

		if (!$cid) {
			$this->addFlash(
				'error',
				'Системные неполадки, не заданы параметры для поиска курса. Обратитесь к администрации.'
			);
			return $this->redirectToRoute('homepage');
		}

		/*                 Courses
		------------------------------------------------------------------------------*/
		$Course_repo = $this->getDoctrine()->getRepository(Course::class);
		$course = $Course_repo->findBy(array("id" => $cid, "instock" => 1, "entry_status" => 1), []);

		// Basket
		$Basket_repo = $this->getDoctrine()->getRepository(Basket::class);

		if (!$course) {
			$this->addFlash(
				'error',
				'Курс не найден.'
			);
			return $this->redirectToRoute('homepage');
		}

		if ($this->getUser()) {
			$user_id = $this->getUser()->getId();

			// Check relevance of courses
			$this->coursesRelevance();

			// check if course is added on basket
			$course = $pChecker->ifCourseAdded("onbasket", $course, $Basket_repo, $user_id);

			$favorites_repo = $this->getDoctrine()->getRepository(favorites::class);
			$user_favorites = $favorites_repo->userfavorites($user_id);
			$user_favorites = $pChecker->ifNewOne($user_favorites);
		}else {
			$user_favorites = null;
		}

		return $this->render('@Client/course_detail/course_detail.html.twig', array(
			'course' => $course,
			'user_favorites' => $user_favorites
		));
	}

	/**
	* @Route("/listCourse", name="listCourse")
	*/
	public function listCourseAction(Request $request)
	{

		if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		$this->coursesRelevance();

		// get reviews entity
		$course_repo = $em->getRepository(Course::class);
		$courses = $course_repo->findBy(
			array(
				'entry_status' => 1,
			), array('id' => 'DESC'));

		return $this->render('@CP/Course/list_course.html.twig', array(
			'courses' => $courses
		));
	}

	/**
	* @Route("/newcourse", name="newcourse")
	*/
	public function newcourseAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		}

		$name = $request->request->get('name');
		$price = $request->request->get('price');
		$content = $request->request->get('content');
		$best = $request->request->get('best');

		if (empty($name) || empty($price) || empty($content) ) {
			$arr = [
				'type' => 'error',
				'output' => 'Не заданы все параметры для создания нового курса. Обратитесь к администрации.'
			];

			return new JsonResponse($arr);
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		try {
			$course = new Course();

			$course->setName($name);
			$course->setPrice($price);
			$course->setContent($content);
			$course->setBest($best);
			$course->setEntryStatus(1);

			$em->persist($course);
			$em->flush();

			$arr = [
				'type' => 'success',
				'output' => $name . 'был успешно добавлен!'
			];

			return new JsonResponse($arr);

		} catch (Exception $e) {
			$arr = [
				'type' => 'error',
				'output' => $e->getMessage()
			];

			return new JsonResponse($arr);
		}


		$arr = [
			'type' => 'error',
			'output' => 'MZFC.'
		];

		return new JsonResponse($arr);
	}

	/**
	 * @Route("/rmvCourse/{course_id}", name="rmvCourse")
	 */
	public function rmvCourseAction($course_id)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		}


		if (!$course_id) {
			$this->addFlash(
				'error',
				'Не заданы параметры для поиска!'
			);
			return $this->redirectToRoute('listCourse');
		}

		// get doctrine
		$em = $this->getDoctrine()->getManager();

		try{
			// get basket entity
			$Basket_repo = $em->getRepository(Basket::class);
			// get course entity
			$course_repo = $em->getRepository(Course::class);
			$course = $course_repo->find($course_id);

			if (!$course) {
				$this->addFlash(
					'error',
					'Курс с таким id не был найден в системе!'
				);
				return $this->redirectToRoute('listCourse');
			}

			$course->setEntryStatus(0);
			$em->persist($course);
			$em->flush();

			$curs_id = $course->getId();
			$curs_name = $course->getName();

			/*----------------------------------------------------
					After remove course remove and 
					entry on basket for prevent erros
			----------------------------------------------------*/
			$course_on_basket = $Basket_repo->findBy(
			array(
				'courseid' => $curs_id
			), array('id' => 'DESC'));

			// print_r($course_on_basket[0]->getId());
			// die;

			if ($course_on_basket) {
				$rm_obj = $Basket_repo->find($course_on_basket[0]->getId());
				$em->remove($rm_obj);
				$em->flush();
			}

			$this->addFlash(
				'success',
				'Курс с ' . $curs_id . ')' .$curs_name . ' был успешно удалён!'
			);

		}
		catch( DBALException $e ){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('listCourse');
		}

		return $this->redirectToRoute('listCourse');
	}

	/**
	 * @Route("/updateCourse")
	 */
	public function updateCourseAction()
	{
		return $this->render('ControlpanelBundle:Course:update_course.html.twig', array(
			// ...
		));
	}

}
