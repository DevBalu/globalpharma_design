<?php

namespace ControlpanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// Entity
use AppBundle\Entity\home_carousel; 

// Catch error when insert on DB
use Doctrine\DBAL\DBALException;
// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class InfoContentController extends Controller
{
	public function __construct()
	{
	} 

	/*-----------------------------------------------------
			CUSTOM METHODS
	-----------------------------------------------------*/
	public function accessDenied($role){
		if (!$this->get('security.authorization_checker')->isGranted($role)) {
			return $this->redirectToRoute('redirectrole');
		}
	}

	/*-----------------------------------------------------
			Home Carousel
	-----------------------------------------------------*/

	/**
	 * @Route("/listHomeCarousel", name="listHomeCarousel")
	 */
	public function listHomeCarouselAction()
	{
		// Security limitation
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();

		$home_carousel_repo = $this->getDoctrine()->getRepository(home_carousel::class);
		$home_carousel = $home_carousel_repo->findBy([
			'entry_status' => 1
		], []);

		return $this->render('@CP/infoContent/listHomeCarousel.html.twig', [
			'home_carousel' => $home_carousel
		]);
	}

	/**
	 * @Route("/newHomeCarousel", name="newHomeCarousel")
	 */
	public function newHomeCarouselAction(Request $request)
	{

		// Security limitation
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();
		$cache = new FilesystemCache();

		$home_carousel_repo = $this->getDoctrine()->getRepository(home_carousel::class);

		// files from PC
		$files = $request->files->get('files');
		// links of images
		$alter_img = $request->request->get('alter_img');

		try{
			if (!empty($files)) {
				/* Set upload_images */
				$target_dir = $this->container->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR ."web" . DIRECTORY_SEPARATOR . "img" . DIRECTORY_SEPARATOR . "home_carousel" . DIRECTORY_SEPARATOR ;
				$images_name = [];

				foreach ($files as $key => $file) {
					$orginal_name = $file->getClientOriginalName();
					$image_name = "img/home_carousel/" .$orginal_name;

					$home_carousel = new home_carousel();
					$home_carousel->setSrc($image_name);
					$home_carousel->setEntryStatus(1);
					$em->persist($home_carousel);
					$em->flush();

					$file->move($target_dir,  $orginal_name);
				}
			}

			if ($alter_img) {
				$alter_img = explode(',', $alter_img);

				foreach ($alter_img as $key => $img) {
					$home_carousel = new home_carousel();
					$home_carousel->setSrc($img);
					$home_carousel->setEntryStatus(1);
					$em->persist($home_carousel);
					$em->flush();
				}


				$cache->clear();
				$cache->set('start.home_carousel', $home_carousel);

			}

			$this->addFlash(
				'success',
				'Баннер(ы) успешно добавлены.'
			);
		}
		catch( DBALException $e ){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
		}
		$cache->clear();
		return $this->redirectToRoute('listHomeCarousel');
	}

	/**
	 * @Route("/removeHomeCarousel", name="removeHomeCarousel")
	 */
	public function removeHomeCarouselAction(Request $request)
	{

		// Security limitation
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();
		$cache = new FilesystemCache();

		$bannerId = $request->request->get('bannerId');

		if (empty($bannerId)) {
			$this->addFlash(
				'error',
				'не указаны параметры для поиска баннера.'
			);
			return $this->redirectToRoute('listHomeCarousel');
		}

		$home_carousel_repo = $this->getDoctrine()->getRepository(home_carousel::class);
		try{
			$banner = $home_carousel_repo->find($bannerId);
			if ($banner) {
				$banner->setEntryStatus(0);
				$em->persist($banner);
				$em->flush();

				$home_carousel = $home_carousel_repo->findBy([
					'entry_status' => 1
				], []);

			
				$cache->set('start.home_carousel', $home_carousel);

				// remove banner from system
				$target_dir = $this->container->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR ."web" . DIRECTORY_SEPARATOR . $banner->getSrc();
				if (file_exists($target_dir)) {
					unlink($target_dir);

					$this->addFlash(
						'warning',
						'Баннер(ы) успешно удалён.'
					);
				}
			}else{
				$this->addFlash(
					'error',
					'Не найден Баннер для удаления.'
				);
			}

		}
		catch( DBALException $e ){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
		}

		if ($cache->clear()) {
			$this->addFlash(
				'warning',
				'Кэш был очищен.'
			);
		}
		return $this->redirectToRoute('listHomeCarousel');

	}

}
