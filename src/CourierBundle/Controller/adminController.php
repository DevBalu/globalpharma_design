<?php

namespace CourierBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
// Entity
use AppBundle\Entity\Orderr;

class adminController extends Controller
{

	/**
	 * @Route("/courierorders", name="courierorders")
	 */
	public function courierordersAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();

		/*------------------------------------------------------------
						PREPARE DATA FOR USING 
		--------------------------------------------------------------*/

		/* --------------Orders repo --------- */
		$Oreder_repo = $this->getDoctrine()->getRepository(Orderr::class);
		$orders_statistics = $Oreder_repo->ordersStatistics();

		$orders = $Oreder_repo->findBy([
			'entry_status' => 1,
			'payment_status' => 'paid'
		], []);

		return $this->render('@CP/orders/allorders.html.twig', array(
			'orders' => $orders,
			'orders_statistics' => $orders_statistics
		));
	}

	/**
	* @Route("/orders")
	*/
	public function adminAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_COURIER')) {
			return $this->redirectToRoute('redirectrole');
		} else {
			return $this->render('@Courier/admin/admin.html.twig', array(
			// ...
			));
		}

	}

}
