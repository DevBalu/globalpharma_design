<?php

namespace CourierBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
	/**
	* @Route("/courier", name="courier")
	*/
	public function adminAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_COURIER')) {
			return $this->redirectToRoute('redirectrole');
		} else {
			return $this->render('@Courier/Default/index.html.twig');
		}
	}

}
