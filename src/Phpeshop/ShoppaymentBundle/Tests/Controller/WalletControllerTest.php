<?php

namespace Phpeshop\ShoppaymentBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WalletControllerTest extends WebTestCase
{
    public function testNewwallet()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/newWallet');
    }

    public function testRemovewallet()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/removeWallet');
    }

    public function testUpdatewallet()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/updateWallet');
    }

}
