<?php

namespace Phpeshop\ShoppaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\Token;

// (name="payment_token")
// (repositoryClass="Phpeshop\ShoppaymentBundle\Repository\PaymentTokenRepository")

/**
 * PaymentToken
 *
 * @ORM\Table
 * @ORM\Entity
 */
class PaymentToken extends Token
{

}

