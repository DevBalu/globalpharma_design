<?php

namespace Phpeshop\ShoppaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wallet
 *
 * @ORM\Table(name="wallet")
 * @ORM\Entity(repositoryClass="Phpeshop\ShoppaymentBundle\Repository\WalletRepository")
 */
class Wallet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="detailWallet", type="array", nullable=true)
     */
    private $detailWallet;

    /**
     * @var string
     *
     * @ORM\Column(name="name_wallet", type="string", length=255)
     */
    private $nameWallet;

    /**
     * @var int
     *
     * @ORM\Column(name="mainWallet", type="integer", nullable=true)
     */
    private $mainWallet;

    /**
     * @var int
     *
     * @ORM\Column(name="entry_status", type="boolean")
     */
    public $entry_status;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

/*-------------------------------------------------
                nameWallet
--------------------------------------------------*/

    /**
     * Set nameWallet
     *
     * @param string $nameWallet
     *
     * @return Wallet
     */
    public function setNamewallet($nameWallet)
    {
        $this->nameWallet = $nameWallet;

        return $this;
    }

    /**
     * Get nameWallet
     *
     * @return string
     */
    public function getNamewallet()
    {
        return $this->nameWallet;
    }


/*-------------------------------------------------
                nrWallet
--------------------------------------------------*/

    /**
     * Set detailWallet
     *
     * @param string $detailWallet
     *
     * @return Wallet
     */
    public function setDetailwallet($detailWallet)
    {
        $this->detailWallet = $detailWallet;

        return $this;
    }

    /**
     * Get detailWallet
     *
     * @return string
     */
    public function getDetailwallet()
    {
        return $this->detailWallet;
    }

/*-------------------------------------------------
                mainWallet
--------------------------------------------------*/

    /**
     * Set mainWallet
     *
     * @param integer $mainWallet
     *
     * @return Wallet
     */
    public function setMainWallet($mainWallet)
    {
        $this->mainWallet = $mainWallet;

        return $this;
    }

    /**
     * Get mainWallet
     *
     * @return integer
     */
    public function getMainWallet()
    {
        return $this->mainWallet;
    }

    /*-------------------------------------------------------*/
    /*              entry_status
     -------------------------------------------------------- */
    /**
     * Set entry_status
     *
     * @param integer $entry_status
     *
     * @return entry_status
     */
    public function setEntryStatus($entry_status)
    {
        $this->entry_status = $entry_status;

        return $this;
    }

    /**
     * Get entry_status
     *
     * @return booelan
     */
    public function getEntryStatus()
    {
        return $this->entry_status;
    }


/*-------------------------------------------------
                date
--------------------------------------------------*/

    /**
     * Set date
     *
     * @param string $date
     *
     * @return category
     */
    public function setDate()
    {
        $this->date = new \DateTime();

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }


}

