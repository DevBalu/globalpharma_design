<?php

namespace Phpeshop\ShoppaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\Payment as BasePayment;

// (name="payment")
// (repositoryClass="Phpeshop\ShoppaymentBundle\Repository\PaymentRepository")

/**
 * Payment
 *
 * @ORM\Table
 * @ORM\Entity
 */
class Payment extends BasePayment
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer $id
     */
    protected $id;
}

