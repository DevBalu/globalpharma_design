<?php

namespace Phpeshop\ShoppaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse; 
use azkdev\WexnzApi\WexnzApi;


class DefaultController extends Controller
{

	/* --------------------- payRedirect ----------------------------------
		Action which redirect user in
		dependece of defined payment method
	---------------------------------------------------------------------- */

	/**
	 * @Route("/payRedirect/{order_uniqid}", name="payRedirect")
	 */
	public function payRedirectAction(Request $request, $order_uniqid)
	{
		if ($request->request->get('payment_method')) {
			$payment_method = $request->request->get('payment_method');

			switch ($payment_method) {
				case 'qiwi':
					return $this->redirectToRoute('payOrderQiwi', [ 'order_uniqid' => $order_uniqid]);
					break;
				case 'wex':
					return $this->redirectToRoute('insertWexCoupon', [ 'order_uniqid' => $order_uniqid]);
					break;
				default:
					return $this->redirectToRoute('customer_orders');
					break;
			}

		}else {
			return $this->redirectToRoute('customer_orders');
		}

	}

	/* --------------------------------------------------------------------
					azkDev WEX payment
	---------------------------------------------------------------------- */

	/**
	 * @Route("/testWex", name="testWex")
	 */
	public function testWexAction()
	{
		$WexnzApi =  new WexnzApi('https://wex.nz/tapi', 'SF9TJHLG-F354KQT4-863JMCAD-SGJL7OMS-0ZXXROIY', 'ae08e777a9f9ea77ae4b9ae8bb0d14a21e92327f6cb50e10af92838c4ffd8ca4');

		print "<pre>";
		print_r($WexnzApi->trade()->userInfo());
		print "</pre>";

		return new JsonResponse();
	}
}
