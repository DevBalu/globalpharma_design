<?php

namespace Phpeshop\ShoppaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Phpeshop\ShoppaymentBundle\Entity\QiwiInfo;
use Symfony\Component\HttpFoundation\Request;

// Catch db extention
use Doctrine\DBAL\DBALException;

class SpQiwiInfoController extends Controller
{
	/**
	 * @Route("/newQuestion/{order_uniqid}", name="newQuestion")
	 */
	public function newQuestionAction($order_uniqid, Request $request)
	{

		if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
			return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
		}

		$question = $request->request->get('question');
		$answer = $request->request->get('answer');

		if (empty($question ) || empty($answer )) {
			return $this->redirectToRoute('hotQuesions');
		}

		$em = $this->getDoctrine()->getManager();

		try{
			$questions = new QiwiInfo();

			// set question
			$questions->setQuestion($question);
			// set answer
			$questions->setAnswer($answer);

			$em->persist($questions);
			$em->flush();

			$this->addFlash(
				'success',
				'Данные были сохранены'
			);
			return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
		}

		return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
	}

	/**
	 * @Route("/rmvQuestion/{order_uniqid}", name="rmvQuestion")
	 */
	public function rmvQuestionAction( $order_uniqid, Request $request )
	{

		if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
			return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
		}

		$entry_id = $request->request->get('entry_id');

		if (empty($entry_id ) ) {
			return $this->redirectToRoute('hotQuesions');
		}

		$em = $this->getDoctrine()->getManager();

		try{
			$questions_repo = $this->getDoctrine()->getRepository(QiwiInfo::class);
			$question = $questions_repo->find($entry_id);

			if (!$question) {
				$this->addFlash(
					'error',
					'Не найденно записи для удаления '
				);
				return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
			}

			// remove question
			$em->remove($question);
			$em->flush();

			$this->addFlash(
				'success',
				'Данные были сохранены'
			);
			return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
		}

		return $this->redirectToRoute('payOrderQiwi', ['order_uniqid' => $order_uniqid]);
	}

}
