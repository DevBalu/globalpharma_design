<?php

namespace Phpeshop\ShoppaymentBundle\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// Catch wex extention
use azkdev\WexnzApi\Exception\ClientErrorException;

// WEX
use azkdev\WexnzApi\WexnzApi;

// Entity
use Phpeshop\ShoppaymentBundle\Entity\Wallet;
use AppBundle\Entity\Orderr;

// Catch extention
use Doctrine\DBAL\DBALException;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;


class SpWexController extends Controller
{
	public function accessDenied(){
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}
	}

	/*-----------------------------------------------------
				Pay order with WEX 
	-----------------------------------------------------*/

	/**
	* @Route("/insertWexCoupon/{order_uniqid}", name="insertWexCoupon")
	*/
	public function insertWexCoupon($order_uniqid = null)
	{
		if ($order_uniqid !== null) {
			return $this->render('@ShoppaymentView/SpWex/pay_order_wex.html.twig', array(
				'order_uniqid' => $order_uniqid
			));
		}else {
			return $this->redirectToRoute('customer_orders');
		}
	}

	/*-----------------------------------------------------
				Pay order with WEX 
	-----------------------------------------------------*/

	/**
	* @Route("/payOrderWex", name="payOrderWex")
	*/
	public function payOrderWexAction(Request $request)
	{
		$order_uniqid = $request->request->get('order_uniqid');
		$coupon = $request->request->get('coupon');

		// prevent accessing controller without needed parameters
		if (empty($order_uniqid)) {
			$this->addFlash(
				'error',
				'Технические неполадки в системе. Не заданы параметры для поиска заказа . Свяжитесь с администрацией.'
			);

			return $this->redirectToRoute('customer_orders');
		}

		// prevent accessing controller without needed parameters
		if (empty($coupon)) {
			$this->addFlash(
				'error',
				'Поле для введения купона не было заполнено.'
			);

			return $this->redirectToRoute('customer_orders');
		}

		// get main WEX wallet
		$em = $this->getDoctrine()->getManager();
		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);
		$Wallet_wex = $Wallet_repo->findOneby(
			['nameWallet' => "wex", 'entry_status' => 1, 'mainWallet' => 1],
			['id' => "DESC"],
			1
		);

		$wallet_detail = $Wallet_wex->getDetailwallet();

		// If wallet WEX is not inserted on DB
		if (empty($Wallet_wex)) {
			$this->addFlash(
				'error',
				'Системные неполадки. На данный момент оплата через WEX купон не может быть осуществлена. Попробуйте позже или свяжитесь с администрацией.'
			);

			return $this->redirectToRoute('customer_orders');
		}

		try {
			$WexnzApi =  new WexnzApi('https://wex.nz/tapi', $wallet_detail['key'], $wallet_detail['secret_key']);

			$Order_repo = $this->getDoctrine()->getRepository(Orderr::class);
			$order = $Order_repo->findOneBy(['uniqid' => $order_uniqid], []);

			/*------------------------------------------------------------------
					Сollect current data about order
			------------------------------------------------------------------*/
			$current_order_status = $order->getOrderStatus();
			$current_order_payment_status = $order->getPaymentStatus();
			// get order final cost
			$order_final_cost = $order->getFinalCost();
			// get current errors message
			$current_errors  = $order->getErrorMessage();


			/*------------------------------------------------------------------
						if order with this uniqid not found
			------------------------------------------------------------------*/
			if (!$order) {
				$this->addFlash(
					'error',
					'Заказ с таким UNIQID не был найден для проведения его оплаты. Свяжитесь с Администрацией сайта'
				);

				return $this->redirectToRoute('customer_orders');
			}

			/*------------------------------------------------------------------
				If field order_status of order not equal "order_failed"
			------------------------------------------------------------------*/
			if ($current_order_status == 'order_failed') {
				$this->addFlash(
					'error',
					'Заказ с таким UNIQID уже оплачен. Если у вас есть неразрешённые вопросы, обратитесь к администрации.'
				);

				return $this->redirectToRoute('customer_orders');
			}

			/*------------------------------------------------------------------
						If order already paid
			------------------------------------------------------------------*/
			if ($current_order_payment_status == 'paid') {
				$this->addFlash(
					'error',
					'Заказ с таким UNIQID уже оплачен. Если у вас есть неразрешённые вопросы, обратитесь к администрации.'
				);

				return $this->redirectToRoute('customer_orders');
			}


			/*------------------------------------------------------------------
					After all validattion, change order state
			------------------------------------------------------------------*/
			try {
				// send cupon to validation
				$request = $WexnzApi->trade()->redeemCoupon($coupon, false, 'http://fixie:o57wFpWW4CLNllr@velodrome.usefixie.com:80');

				/*------------------------------------------------------
					Get and prepare data about transaction 
				-----------------------------------------------------*/
				// check request status
				if ($request['success'] = 1) {
					$status = 'success';
				}else {
					$status = 'failed';
				}

				// check couponAmount
				$couponAmount = $request['return']['couponAmount'];

				// check couponCurrency
				$couponCurrency = $request['return']['couponCurrency'];

				// transID
				$transID = $request['return']['transID'];

				// 'Способ оплаты' => 'wex',

				$checked = [
					'payment_method' => 'wex',
					'status' => $status,
					'order_final_cost' => $order_final_cost . 'RUB',
					'trn_amount' => $couponAmount . $couponCurrency,
					'transID' => $transID
				];

				// functionlity for change order payment_status
				if ((float)$order_final_cost <= $couponAmount && $couponCurrency == 'RUR') {
					$order->setPaymentMethod('wex');
					$order->setPaymentStatus('paid');
					$order->setDeliveryStatus('to_courier');
					$order->setPayment_date();

					$order->setChecked($checked);

				} else {
					$order->setOrderStatus('order_failed');
					$new_error = [];

					// if error is related with Currency of transfer
					if ($couponCurrency !== 'RUB' || (float)$order_final_cost > (float)$couponAmount) {
						$new_err_message ='Cтоимость заказа : ' .  $order_final_cost . " RUB  <br> " .
									'Сумма купона WEX : ' . $couponAmount . " " . $couponCurrency ." <br> ";

						$new_error[] = $new_err_message;
						$order->setErrorMessage($new_error);
					}
				}
				$em->persist($order);
				$em->flush();

				/*                   Work with cahce
				------------------------------------------------------------------------------*/
				$cache = new FilesystemCache();

				$cache->clear();

				$new_payment_status = $order->getPaymentStatus();

				if ($new_payment_status == 'paid') {
					$this->addFlash(
						'success',
						'Заказ был успешно оплачен. В будущем вы можете отслеживать статус заказа в разделе «Личный кабинет".'
					);

				}else {
					$this->addFlash(
						'error',
						'Заказ не прошёл валидацию . Свяжитесь с администрацией для разрешения проблемы.'
					);
				}

				return $this->redirectToRoute('customer_orders');

			} catch (DBALException $e) {

				if ($this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
					$this->addFlash(
						'error',
						$e->getMessage()
					);

				}else {
					$this->addFlash(
						'error',
						"Невозможно осуществить оплату через систему WEX. Свяжитесь с администрацией сайта."
					);
				}

				return $this->redirectToRoute('customer_orders');

			}


			/*------------------------------------------------------------------
					notify customer about successful paying of order
			------------------------------------------------------------------*/

		} catch (ClientErrorException $e) {

			switch ($e->getMessage()) {
				case "invalid coupon":
						$this->addFlash(
							'error',
							'invalid coupon'
						);

						return $this->redirectToRoute('insertWexCoupon', ['order_uniqid' => $order_uniqid]);
					break;

				case "api key dont have coupon permission":

						if ($this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
							$this->addFlash(
								'error',
								$e->getMessage()
							);

						}else {
							$this->addFlash(
								'error',
								'Системные неполадки. На данный момент оплата через WEX купон не может быть осуществлена. СВЯЖИТЕСЬ С АДМИНИСТРАЦИЕЙ.'
							);
						}

						return $this->redirectToRoute('customer_orders');
					break;

				default:
					if ($this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
						$this->addFlash(
							'error',
							$e->getMessage()
						);

						return $this->redirectToRoute('insertWexCoupon', ['order_uniqid' => $order_uniqid]);

					}else {
						$this->addFlash(
							'error',
							"Невозможно осуществить оплату через систему WEX. Свяжитесь с администрацией сайта."
						);

						return $this->redirectToRoute('customer_orders');
					}

					break;
			}
		}

		return $this->redirectToRoute('customer_orders');
	}


	/*-----------------------------------------------------
				wex Wallet info
	-----------------------------------------------------*/

	/**
	* @Route("/wexWalletinfo", name="wexWalletinfo")
	*/
	public function wexWalletinfoAction()
	{
		// security access
		$this->accessDenied();

		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);
		$Wallet_wex = $Wallet_repo->findOneby(
			['nameWallet' => "wex", 'entry_status' => 1, 'mainWallet' => 1],
			['id' => "DESC"],
			1
		);

		$wallet_detail = $Wallet_wex->getDetailwallet();

		$WexnzApi =  new WexnzApi('https://wex.nz/tapi', $wallet_detail['key'], $wallet_detail['secret_key']);

		try {
			print "<pre>";
			print_r($WexnzApi->trade()->userInfo(false, 'http://fixie:o57wFpWW4CLNllr@velodrome.usefixie.com:80'));
			print "</pre>";

		} catch (ClientErrorException $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);

			return $this->redirectToRoute('wexListWallet');
		}

		return new JsonResponse();

	}

	/*-----------------------------------------------------
			Display List of  WEX wallets 
	-----------------------------------------------------*/

	/**
	* @Route("/wexListWallet", name="wexListWallet")
	*/
	public function wexListWalletAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);

		$Wallet = $Wallet_repo->findby(
			['nameWallet' => "wex", 'entry_status' => 1],
			['id' => "DESC"]);

		return $this->render('@ShoppaymentView/SpWex/wex_list_wallets.html.twig', array(
			'wex_wallets' => $Wallet
		));
	}

	/*-----------------------------------------------------
				New  WEX wallet 
	-----------------------------------------------------*/

	/**
	* @Route("/newWexWallet", name="newWexWallet")
	*/
	public function newWexWalletAction(Request $request)
	{

		if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();
		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);
		$Wallet = new Wallet();

		$key = $request->request->get('key');
		$secret_key = $request->request->get('secret_key');


		//  INSERT ON DB
		try{
			/*------------Set Name wallet----------------------*/
			$Wallet->setNamewallet('wex');

			/*------------Set Name wallet----------------------*/
			$Wallet_detail = [
				'key' => $key,
				'secret_key' => $secret_key
			];
			$Wallet->setDetailwallet($Wallet_detail);

			/*------------Wallet Main----------------------*/

			/* ---------------------------------------------------------------
						declate another walllet secondary
			--------------------------------------------------------------*/
			$current_wallets_main = $Wallet_repo->findBy([
				'nameWallet' => "wex",
				"mainWallet" => 1
			], []);

			if ($current_wallets_main) {
				// methodfindBy return arrray,
				// posiible we have several wallets with status main.
				// Nedd to change her status to secondary 
				foreach ($current_wallets_main as $key => $value) {

					$wallet_main_to_second = $Wallet_repo->find($value->getId());
					if ($wallet_main_to_second) {
						$wallet_main_to_second->setMainWallet(null);
						$em->persist($wallet_main_to_second);
						$em->flush();
					}
				}
			}

			/*  At this moment when we add new wallet he make main for payments */
			$Wallet->setMainWallet(1);
			$Wallet->setEntryStatus(1);

			$em->persist($Wallet);
			$em->flush();
		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
		}

		return $this->redirectToRoute('wexListWallet');
	}

	/*-----------------------------------------------------
				New  WEX wallet 
	-----------------------------------------------------*/

	/**
	 * @Route("/updateWexWallet", name="updateWexWallet")
	 */
	public function updateWexWalletAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('redirectrole');
		}

		// Get parametres
		$wallet_id = $request->request->get('wallet_id');
		$key = $request->request->get('key');
		$secret_key = $request->request->get('secret_key');
		$main_wallet = $request->request->get('main_wallet');

		$Detail_wallet = [
			'key' => $key,
			'secret_key' => $secret_key
		];

		$em = $this->getDoctrine()->getManager();

		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);

		try {
			$wallet = $Wallet_repo->find($wallet_id);

			if ($wallet) {
				$wallet->setDetailwallet( $Detail_wallet );

				/*------------Wallet Main----------------------*/
				if ($main_wallet) {

					/* ---------------------------------------------------------------
								declate another walllet secondary
					--------------------------------------------------------------*/
					$current_wallets_main = $Wallet_repo->findBy([
						'nameWallet' => "wex",
						"mainWallet" => 1
					], []);

					if ($current_wallets_main) {
						// methodfindBy return arrray,
						// posiible we have several wallets with status main.
						// Nedd to change her status to secondary 
						foreach ($current_wallets_main as $key => $value) {

							$wallet_main_to_second = $Wallet_repo->find($value->getId());
							if ($wallet_main_to_second) {
								$wallet_main_to_second->setMainWallet(null);
								$em->persist($wallet_main_to_second);
								$em->flush();
							}
						}
					}

					/*  At this moment when we add new wallet he make main for payments */
					$wallet->setMainWallet(1);
				}
				/*------------END Wallet Main----------------------*/


				$em->persist($wallet);
				$em->flush();

				$this->addFlash(
					'default',
					"Информация успешно обновлена"
				);
			}else{
				$this->addFlash(
					'error',
					"Не найден кошелёк с таким ИД"
				);
			}
				return $this->redirectToRoute('wexListWallet');

		} catch (Exception $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('wexListWallet');
		}

	}


	/*-----------------------------------------------------
				New  WEX wallet 
	-----------------------------------------------------*/

	/**
	 * @Route("/removeWexWallet/{wid}", name="removeWexWallet")
	 */
	public function removeWexWalletAction(Request $request, $wid)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();

		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);

		try {
			$wallet = $Wallet_repo->find($wid);

			if ($wallet) {
				$wallet->setEntryStatus(0);
				$em->persist($wallet);
				$em->flush();

				$this->addFlash(
					'success',
					"Кошелек был успешно удалён"
				);
			}else{
				$this->addFlash(
					'error',
					"Не найден кошелёк с таким ИД"
				);
			}

			return $this->redirectToRoute('wexListWallet');

		} catch (Exception $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('wexListWallet');
		}

		return $this->redirectToRoute('wexListWallet');
	}


}
