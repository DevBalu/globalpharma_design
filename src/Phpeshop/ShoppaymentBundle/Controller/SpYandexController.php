<?php

namespace Phpeshop\ShoppaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Response;

// Yandex API
use \YandexMoney\API;

// Payments from bank cards without authorization
use \YandexMoney\ExternalPayment;


class SpYandexController extends Controller
{
    /**
     * @Route("/testYandex")
     */
    public function testAction()
    {

        /* Register your app on link bellow for optain client_id, client_secret
             link : https://money.yandex.ru/myservices/new.xml
        */
        $instance_name = 'new2';

        $client_id = '7584321683C54C5468E5B050B3568D475BD2B77E96CC8362CCAFE40A9526CCF2';

        $redirect_uri = 'http://todolrv.ga/';

        $client_secret = 'B95CE2CC41EA498AA9D6200EB63326F628B03DF5F846642F3431B26D8EFD16EDFC7E44325D3ABA23E5A884C607A140607903E4CB64981F72ED74D0E7B401FDA2';

        /*-------------------------------------------------------------------------------------------------------------
                 Scope options:
        'account-info', 'operation-history', 'operation-details', 'incoming-transfers', 'payment', 'payment-shop', 'payment-p2p', 'money-source'
        -------------------------------------------------------------------------------------------------------------*/
        // $scope = ['account-info' , 'payment-p2p'];


        /*-------------------------------------------------------------------------------------------------------------               Get code with setted permission ------------------------------------------------------------------------------------------------------------- */
        // $auth_url = API::buildObtainTokenUrl($client_id, $redirect_uri, $scope, $instance_name);

        // result of buildObtainTokenUrl
        $code = 'BF052B696D082C6BE1CCA176DC8F8B2D2BCA540C0E57629B2A6DAFADE6DD42C027D57437D59BB3DA95410744EC2C73036BB976990A24C4F341A5295CBC07796AD7D9ACF77E6538B0CB8DFD2073F3D73D0D167BD780B88BC706FCC48CA6F44D6BDBAAAF0EBD331C93E149C203FB9BA7A21EC2D4BFEFF914EBEFC53F990BF8715C';


        /*-------------------------------------------------------------------------------------------------------------
                     Get Access token 
        -------------------------------------------------------------------------------------------------------------*/
        // $access_token_response = API::getAccessToken($client_id, $code, $redirect_uri, $client_secret);


        $access_token = '410017539597816.8E1FAD68BC386D8D4D0C340CC735C829AC8BF7F8002368608CAA7D03E8317C0FC246363E7BA2D79A070686288F462093AF361855ABF0DBC1CA6E2C9A4187FD19FAC32E6F98903EACE6B3CCF56A6AC6CB3C6FBF01CEF851C72B5302C338BFD48732D7BA73DCDFC2BAB7298C115CA609E8AAE6AA3B5B019D5B255DA2D88A100B01';


        /*--------------------------------------------------------------------------------------
                Send money from wallet which have access token to wallet declared on variable $money_wallet
-------------------------------------------------------------------------------------------------*/
        $api = new API($access_token);

        /* Get account info */
        $acount_info = $api->accountInfo();


        /* Get operation history with last 3 records*/
        // $operation_history = $api->operationHistory(array("records"=>3));


        /* Request payment options */
        /* DevBalu wallet */
        // $money_wallet = '410017539597816';

        /* AG wallet */
        // $money_wallet = '410016393699015';

        // $amount_due = '10';
        // $comment = 'comment';
        // $message = 'message';
        // $label = 'label';

        // $request_payment = $api->requestPayment(array(
        //     "pattern_id" => "p2p",
        //     "to" => $money_wallet,
        //     "amount_due" => $amount_due,
        //     "comment" => $comment,
        //     "message" => $message,
        //     "label" => $label,
        // ));

        /* call process payment to finish payment */
        // $process_payment = $api->processPayment(array(
        //     "request_id" => $request_payment->request_id,
        // ));



        // return $this->redirect( $acount_info );
        return new JsonResponse($acount_info);
    }

}
