<?php

namespace Phpeshop\ShoppaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse; 

use Symfony\Component\HttpFoundation\Session\SessionInterface;

// Catch db extention
use Doctrine\DBAL\DBALException;

// Entity
use AppBundle\Entity\Orderr;
use Phpeshop\ShoppaymentBundle\Entity\Wallet;
use Phpeshop\ShoppaymentBundle\Entity\QiwiInfo;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class SpQiwiController extends Controller
{
	/* --------------------- payOrderQiwi ----------------------------------
			Pay order acrosss qiwi payment sistem
	---------------------------------------------------------------------- */

	/**
	 * @Route("/payOrderQiwi/{order_uniqid}", name="payOrderQiwi")
	 */
	public function payOrderQiwiAction($order_uniqid, SessionInterface $session)
	{
		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		if (!$order_uniqid) {
			return $this->redirectToRoute('customer_orders');
		}

		$em = $this->getDoctrine()->getManager();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();

		/* ------------------id_user------------- */
		$user = $this->getUser();
		$id_user = $user->getId();

		/* --------------Order products --------- */
		$Order_repo = $this->getDoctrine()->getRepository(Orderr::class);
		$Order = $Order_repo->findOneBy([
			"uniqid" => $order_uniqid,
			'idUser' => $id_user
		], []);

		/* ------------------------------------------
			if not found order redirect user to
			page "customer orders" with error message
		-------------------------------------------- */
		if (empty($Order)) {
			$this->addFlash(
				'error',
				'Технические неполадки в системе. Не был найден заказ с таким ИД. Свяжитесь с администрацией.'
			);

			return $this->redirectToRoute('customer_orders');
		}

		/* ------------------------------------------
			If customer access this page but order
			is already paid
		-------------------------------------------- */
		if ($Order->getPaymentStatus() == "paid") {
			$this->addFlash(
				'success',
				'Заказ был успешно оплачен'
			);

			return $this->redirectToRoute('customer_orders');
		}

		/* --------------Get number of wallet --------- */
		$wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);
		$wallet = $wallet_repo->findOneBy(array(
			'nameWallet' => 'qiwi',
			'mainWallet' => 1
		), array('id' => 'DESC'), 1);

		/* ------------------------------------------
			Get wallet where will be send money,
			if wallet is not defined order will be not paid
			redirect user to homepage with error message
		-------------------------------------------- */
		if (empty($wallet)) {
			$this->addFlash(
				'error',
				'Из-за технических неполадок, оплата не может быть выполнена. Свяжитесь с администрацией, Код ошибки: pw'
			);

			return $this->redirectToRoute('homepage');
		}

		$Detailwallet = $wallet->getDetailwallet();

		$nrWallet = $Detailwallet['walletNr'];
		$comment = $Order->getUniqid();

		/* Calculate final amount */
		$products = $Order->getProducts();
		$final_cost = (float)$Order->getFinalCost();

		if ($final_cost > 14900) {
			$final_cost = 14900;
		}

		/*   INSERT TO DB   */
		$em->persist($Order);
		$em->flush();

		$cache->clear();

		/* -------------- Generated link for payment ------------------- */
		$url = 'https://qiwi.com/payment/form/99?extra%5B%27account%27%5D=' . $nrWallet . '&amountInteger=' . $final_cost . '&extra%5B%27comment%27%5D=' . $comment . '&currency=643';
		// return $this->redirect($url);

		/* -------------- get qiwi instructions ------------------- */
		$QiwiInfo_repo = $this->getDoctrine()->getRepository(QiwiInfo::class);
		$QiwiInfo = $QiwiInfo_repo->findby([], ['id' => 'DESC']);

		return  $this->render('@ShoppaymentView/Qiwi/new_qiwi_payment.html.twig', array(
			'qiwi_url' => $url,
			'order_uniqid' => $order_uniqid,
			'QiwiInfo' => $QiwiInfo
		));

	}


	/**
	 * @Route("/changeOrderQiwiStatus", name="changeOrderQiwiStatus")
	 */
	public function changeOrderQiwiStatusAction(Request $request)
	{
		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		$order_uniqid = $request->request->get('order_uniqid');
		$payment_status = $request->request->get('payment_status');

		if (!$order_uniqid) {
			$arrData = [
				'type' => 'error',
				'output' => 'Параметр поиска, для изменения статуса заказа, не был задан.'
			];
			return new JsonResponse($arrData);
		}

		$em = $this->getDoctrine()->getManager();
		/* --------------Order products --------- */
		$Order_repo = $this->getDoctrine()->getRepository(Orderr::class);

		try{
			$Order = $Order_repo->findOneBy([
				"uniqid" => $order_uniqid
			], []);

					/* Set Payment Status */
			$Order->setPaymentStatus($payment_status);

			if (empty($Order)) {
				$arrData = [
					'type' => 'error',
					'output' => 'Заказ с таким Ид не был найден. Статус заказа не был изменён'
				];
				return new JsonResponse($arrData);
			}

			$em->persist($Order);
			$em->flush();

			$arrData = [
				'type' => 'success',
				'output' => 'Статус заказа был успешно обновлен'
			];
			return new JsonResponse($arrData);

		}
		catch(DBALException $e){
			$arrData = [
				'type' => 'error',
				'output' => $e->getMessage()
			];
			return new JsonResponse($arrData);
		}
	}


	/*------------------------------------------------------------------------------------------
						CUSTOM METHOD AG
				Return list of transaction from qiwi wallet of user
	-------------------------------------------------------------------------------------------*/
	public function getQiwiListTransaction()
	{

		$em = $this->getDoctrine()->getManager();

		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);

		$wallet = $Wallet_repo->findOneBy(array(
			'nameWallet' => 'qiwi',
			'mainWallet' => 1
		), array('id' => 'DESC'), 1);

		$Detailwallet = $wallet->getDetailwallet();

		if (!$wallet) {
			return "not_found_wallet";
		}

		$nrWallet = $Detailwallet['walletNr'];
		$token = $Detailwallet['token'];

		$qurl = "https://edge.qiwi.com/payment-history/v2/persons/" . $nrWallet . "/payments?rows=50";

		// Create a stream
		$opts = array(
			'http'=>array(
				'method'=>"GET",
				'header'=>
					"Content-Type: application/json\r\n" .
					"Accept: application/json\r\n" .
					"Authorization:Bearer " . $token . "\r\n" .
					"Host: edge.qiwi.com\r\n"
			)
		);

		$context = stream_context_create($opts);

		// Open the file using the HTTP headers set above
		$file = @file_get_contents($qurl, false, $context);
		$data = json_decode($file,true);

		return $data;
	}


	/*------------------------------------------------------------------------------------------
						CUSTOM METHOD AG
				Return list of transaction from qiwi wallet of user
	-------------------------------------------------------------------------------------------*/

	/**
	 * @Route("/validQiwiWallet", name="validQiwiWallet")
	 */
	public function validQiwiWalletAction()
	{
		// $this->addFlash(
		// 	'error',
		// 	'Нет доступа к данным QIWI кошелька. Проверьте правильность заполнения номера кошелька и токена . '
		// );
		// return $this->redirectToRoute('qiwiSettings');

		$data = $this->getQiwiListTransaction();

		if ($data == null) {
			$this->addFlash(
				'error',
				'Нет доступа к данным QIWI кошелька. Проверьте правильность заполнения номера кошелька и токена .'
			);

			return $this->redirectToRoute('qiwiSettings');

		} else {
			print "<pre>";
			print_r( $data);
			print "</pre>";

			return new JsonResponse();
		}
	}


	/**
	 * @Route("/qiwiPaymentValidation", name="qiwiPaymentValidation")
	 */
	public function qiwiPaymentValidationAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();


		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();


		/* --------------Order products --------- */
		$Order_repo = $this->getDoctrine()->getRepository(Orderr::class);

		$tranHistory = $this->getQiwiListTransaction()['data'];

		// is case when wallet not be found
		if ($tranHistory == "not_found_wallet") {
			$this->addFlash(
				'error',
				'Системные неполадки. На данный момент оплата через Qiwi недоступна. Приносим наши извинения.'
			);

			return $this->redirectToRoute('customer_orders');
		}

		/*----------------------------------------------------------------------------------------------------------------------------------------
					FUNCTIONALITY FOR COLLECT Transaction OBJECTS WHICH DUPLICATE IN HISTORY OF TRANSACTION
		-----------------------------------------------------------------------------------------------------------------------------------------*/
		/*-----------------------------------------
			Check if order not was paid double
		-----------------------------------------*/
		// array_filter-  remove empty value from arr
		$trans_comments = array_filter( array_column($tranHistory, 'comment') );
		// get all uniqid of orders stored on "message"
		$values_count = array_count_values($trans_comments);

		// Prepare elements which is duplicated on history transaction
		$repeated_uniqid = [];
		foreach ($values_count as $VC_key => $VC_value) {
			if ($VC_value > 1) {
				$repeated_uniqid[] = $VC_key;
			}
		}

		// Collet  items  with needed elements for calcuate count of transaction for order and recalculate totals

		/*-------------------------------------------------*/
		$repeated_payments = [];
		$rp_comments  = [];
		/*-------------------------------------------------*/
		foreach ($repeated_uniqid as $rep_value) {

			$collet_transaction = [];
			foreach ($tranHistory as $key => $trns_value) {

				if ($rep_value == $trns_value['comment'] && $trns_value['status'] == "SUCCESS" ) {
					$collet_transaction[] = $trns_value;
					$rp_comments[] = $trns_value['comment'];
				}

			}
			$repeated_payments[] = $collet_transaction;
		}

		/*----------------------------------------------
			Part of code above return varible
				" $repeated_payments "
			He contand all transaction in which 
			field comment is repeated with  another
		-----------------------------------------------*/


		/*------------------------------------------------------------------------------------------------------------------------------------------------
									Functionality for to figure out what order is paid
									for change he status and relevance
		-------------------------------------------------------------------------------------------------------------------------------------------------*/
		try{
			$orders_changed = [];
			$transaction_duplicates = [];


			foreach ($tranHistory as $key => $transaction) {

				/* -----------------------------------------------------------
					if order transaction status is SUCCESS and 
					"amount" of transaction is equal with order
					"final cost" change order payment status/method/date
				-------------------------------------------------------------*/

				// checker at duplicates
				$DCH = in_array($transaction['comment'], $rp_comments);

				if ($transaction['status'] == "SUCCESS" && !$DCH) {
					$transaction_duplicates[] = $transaction['comment'];

					$Order = $Order_repo->findOneBy([
						"uniqid" => $transaction['comment']
					], []);

					if ($Order) {
						$current_payment_method = $Order->getPaymentMethod();
						$current_payment_status = $Order->getPaymentStatus();
						$current_entry_status = $Order->getEntryStatus();
						$current_order_status = $Order->getOrderStatus();

						$error_message = [];

						if (empty($current_payment_method) && $current_payment_status !== 'paid' && $current_order_status !== 'order_failed' ) {
							(float)$order_final_cost = $Order->getFinalCost();
							(float)$transaction_amount = $transaction['total']['amount'];

							if ( $order_final_cost <= $transaction_amount ) {

									$Order->setPaymentStatus('paid');

									$Order->setDeliveryStatus('to_courier');

									$checked = [
										'status' => 'success',
										'payment_method' => 'qiwi',
										'sender' => $transaction['account'],
										'transaction_amount' => $transaction_amount,
										'order_final_cost' => $order_final_cost,
										'qiwi_payment_date' => $transaction['date']
									];

									$cache->clear();

									$Order->setChecked($checked);

									// if paid order at moment is removed, resote he on system
									if ($current_entry_status == 0) {
										$error_message[] = 'Заказ был собран -> проверен -> оплачен и удалён из системы';
										$Order->setEntryStatus(1);
									}

									// if $transaction_amount more than order_final_cost
									if ($order_final_cost < $transaction_amount) {
										$error_message[] = 'Сумма к оплате : ' . $order_final_cost . ". Сумма транзакции Qiwi: " . $transaction_amount ;
									}

									$Order->setPayment_date();

									$orders_changed[] = $transaction['comment'];

							} //./END amount coincidence
							else {

								$Order->setOrderStatus('order_failed');

								// collet trns detail in to array
								$TD = [
									'trns_amount' => $transaction_amount,
									'trns_date' => $transaction['date'],
									'trns_sender' => $transaction['account']

								];
								$Order->setTrnsHistory($TD);


								// calculate difference of payment
								$amount_difference = $order_final_cost - $transaction_amount;
								$Order->setAmountDifference($amount_difference);

								$error_message[] = 'Сумма к оплате :' . $order_final_cost . ". Сумма транзакции  Qiwi: " . $transaction_amount ;
							}

							$Order->setPaymentMethod('qiwi');
							$Order->setErrorMessage($error_message);
							$em->persist($Order);
							$em->flush();
						} /*./ END if order already not paid*/

					} //END if fined order

				} // ENd if transaction status is SUCCESS

			} /*END foreach*/

			/*-----------------------------------------------
				RECALCULATE TRANSACTION AMOUNT FOR ORDER
			-------------------------------------------------*/
			// If have duplicates order relevance will be change bellow
			foreach ($repeated_payments as $trns_order_key => $trns_order) {
				$ORDER_UNIQID =  array_column($trns_order, 'comment')[0];
				$trns_collet_amount = 0;
				$HISTORY = [];

				foreach ( $trns_order as $trns_det_key => $trns_detail) {
					// collet trns detail in to array
					$TD = [
						'trns_amount' => $trns_detail['total']['amount'],
						'date' => $trns_detail['date'],
						'account' => $trns_detail['account']
					];

					// set he on global arr about history trns
					$HISTORY[] = $TD;

					// calculate trns_collet_amount
					$trns_collet_amount += (float)$trns_detail['total']['amount'];
				}


				if (strlen($ORDER_UNIQID) == 13) {

					$Order = $Order_repo->findOneBy([
						"uniqid" => $ORDER_UNIQID
					], []);


					if (!empty($Order)) {
						$current_payment_method = $Order->getPaymentMethod();
						$current_payment_status = $Order->getPaymentStatus();
						$current_entry_status = $Order->getEntryStatus();
						$current_order_status = $Order->getOrderStatus();

						$order_final_cost = (float)$Order->getFinalCost();

						if ($current_payment_status !== 'paid') {
							if ($trns_collet_amount >= $order_final_cost ) {

								// NEW_ORDER_PAYMENT_STATUS
								$Order->setPaymentStatus('paid');
								$Order->setDeliveryStatus('to_courier');

								$checked = [
									'status' => 'success',
									'payment_method' => 'qiwi',
									'sender' =>  $trns_detail['account'],
									'transaction_amount' => $trns_collet_amount,
									'order_final_cost' => $order_final_cost
								];

								$Order->setChecked($checked);

								$Order->setPayment_date();

								$Order->setPaymentMethod('qiwi');

								$Order->setOrderStatus('accepted');

								$error_message = null;
								$orders_changed[] = $ORDER_UNIQID;

							} /*./ END IF trns_collet amount is more or equal if final cost*/
							else {

								/* IF ORDER IS NOT PAID COMPLETELY*/
								$error_message[] = 'Сумма к оплате: ' . $order_final_cost . ". Сумма за транзакции(ю) Qiwi: " . $trns_collet_amount ;
								$Order->setChecked(null);
								$Order->setCustomPayment_date(null);
								$Order->setDeliveryStatus(null);
								$Order->setOrderStatus('order_failed');
							}

							// $error_message
							$Order->setErrorMessage($error_message);

							// $amount_difference
							$amount_difference = $order_final_cost - $trns_collet_amount;
							$Order->setAmountDifference($amount_difference);

							// TrnsHistory
							$Order->setTrnsHistory($HISTORY);

							$em->persist($Order);
							$em->flush();

						}/*./ END if order already not paid*/


					} /*./ END IF ORDER FINED ON SISTEM*/
				} /*If Comment is valid and is uniqid from shop*/

			} /*END loop of transaction for specific order*/

			/*-----------------------------------------
				Return notification about results
			-----------------------------------------*/
			if (sizeof($orders_changed) > 0 && $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
				$cache->clear();

				$arr_data = [
					'type' => 'success',
					'output' => implode( ", ", $orders_changed)
				];
				return new JsonResponse($arr_data);

			} elseif(sizeof($orders_changed) <= 0 && $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
				$arr_data = [
					'type' => 'none',
					'output' => 'Изминений по оплате QIWI нет'
				];
				return new JsonResponse($arr_data);
			}

			/**/
			return "vasea";
			// return $this->redirectToRoute('customer_orders');
			/**/

		}
		catch(DBALException $e){

			if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
				$arr_data = [
					'type' => 'error',
					'output' => $e->getMessage()
				];

				return new JsonResponse($arr_data);
			}
			// return $this->redirectToRoute('customer_orders');
		}

	} /*./ END qiwiPaymentValidation*/

}