<?php

namespace Phpeshop\ShoppaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/* Status of payment */
use Payum\Core\Request\GetHumanStatus;

/* Authorize payment */
use Payum\Core\Storage\StorageInterface;
use Payum\Core\Security\TokenInterface;

/* For PayPal Rest */
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

use PayPal\Api\Address;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;

class SpPaypalController extends Controller
{

/*------------------------------------------------------------------------------------------------------------------------------*/
//									 paypal_express_checkout
/*------------------------------------------------------------------------------------------------------------------------------*/

	/**
	* @Route("/sp_prepare_order", name="sp_prepare_order")
	*/
	public function sp_prepare_orderAction()
	{
		$gatewayName = 'paypal_express_checkout';

		/** @var StorageInterface $storage */
		$storage = $this->get('payum')->getStorage('Phpeshop\ShoppaymentBundle\Entity\Payment');

		$payment = $storage->create();
		$payment->setNumber(uniqid());
		$payment->setCurrencyCode('EUR');
		$payment->setTotalAmount("1.00"); // 1.23 EUR
		$payment->setDescription('A description');
		$payment->setClientId(uniqid());
		$payment->setClientEmail('foo@example.com');

		$payment->setDetails(array(
			// Api::FIELD_QUICKPAY_FORM => Api::QUICKPAY_FORM_SHOP,
			// Api::FIELD_PAYMENT_TYPE => Api::PAYMENT_AC
		));

		$storage->update($payment);

		/** @var TokenInterface $captureToken */
		$captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
			$gatewayName, 
			$payment, 
			'sp_done' // the route to redirect after capture
		);

		return $this->redirect($captureToken->getTargetUrl());
	}

/*------------------------------------------------------------------------------------------------------------------------------*/
//									 IT IS NECESSARY TO MORE WORK
/*------------------------------------------------------------------------------------------------------------------------------*/
	/**
	* @Route("/sp_paypal_rest", name="sp_paypal_rest")
	*/
	public function sp_paypal_restAction()
	{
		$payum = $this->get('payum');

		/** @var \Payum\Core\Payum $payum */
		$storage = $payum->getStorage('Payum\Paypal\Rest\Model\PaymentDetails');

		$payment = $storage->create();
		$storage->update($payment);
		/*credit_card*/
		// $address = new Address();
		// $address->line1 = "Dacia 18";
		// $address->line2 = "";
		// $address->city = "Chisinau";
		// $address->state = "Moldova";
		// $address->postal_code = "2043";
		// $address->country_code = "MD";
		// $address->phone = "69492602";

		// $card = new CreditCard();
		// $card->type = "mastercard";
		// $card->number = "5574840033582887";
		// $card->expire_month = "10";
		// $card->expire_year = "2019";
		// $card->cvv2 = "130";
		// $card->first_name = "Andrei";
		// $card->last_name = "Ghenov";
		// $card->billing_address = $address;

		// $fi = new FundingInstrument();
		// $fi->credit_card = $card;
		/* ./credit_card*/


		$payer = new Payer();
		// paypal
		$payer->payment_method = "paypal";
		// ./paypal

		// credit_card
		// $payer->payment_method = "credit_card";
		// $payer->funding_instruments = array($fi);
		// ./credit_card

		$amount = new Amount();
		$amount->currency = "EUR";
		$amount->total = "1.00";

		$transaction = new Transaction();
		$transaction->amount = $amount;
		$transaction->description = "This is the payment description.";

		$captureToken = $payum->getTokenFactory()->createCaptureToken('paypal_rest', $payment, 'sp_done');

		$redirectUrls = new RedirectUrls();
		$redirectUrls->return_url = $captureToken->getTargetUrl();
		$redirectUrls->cancel_url = $captureToken->getTargetUrl();

		$payment->intent = "sale";
		$payment->payer = $payer;
		$payment->redirect_urls = $redirectUrls;
		$payment->transactions = array($transaction);

		$storage->update($payment);

		return $this->redirect($captureToken->getTargetUrl());
	}

/*------------------------------------------------------------------------------------------------------------------------------*/
//									 DONE ACTION
// 					After transaction is effectuated redirect to done Action
/*------------------------------------------------------------------------------------------------------------------------------*/

	/**
	* @Route("/sp_done", name="sp_done")
	*/
	public function sp_doneAction(Request $request)
	{
		$token = $this->get('payum')->getHttpRequestVerifier()->verify($request);

		$gateway = $this->get('payum')->getGateway($token->getGatewayName());

		/* You can invalidate the token, so that the URL cannot be requested any more: */
		$this->get('payum')->getHttpRequestVerifier()->invalidate($token);

		/* Once you have the token, you can get the payment entity from the storage directly. */
		// $identity = $token->getDetails();
		// $payment = $this->get('payum')->getStorage($identity->getClass())->find($identity);

		/*Or Payum can fetch the entity for you while executing a request (preferred).*/
		$gateway->execute($status = new GetHumanStatus($token));
		$payment = $status->getFirstModel();

		if ($status->isCaptured() || $status->isAuthorized()) {
		  // success
			return new JsonResponse(array(
				'status' => $status->getValue(),
				'payment' => array(
					'total_amount' => $payment->getTotalAmount(),
					'currency_code' => $payment->getCurrencyCode(),
					'details' => $payment->getDetails(),
				)
			));
		}

		if ($status->isPending()) {
		  // most likely success, but you have to wait for a push notification.
			return new JsonResponse(array(
				'status' => $status->getValue(),
				'payment' => array(
					'total_amount' => $payment->getTotalAmount(),
					'currency_code' => $payment->getCurrencyCode(),
					'details' => $payment->getDetails(),
				)
			));
		}

		if ($status->isFailed() || $status->isCanceled()) {
		  // the payment has failed or user canceled it.
			return new JsonResponse(array(
				'status' => $status->getValue(),
					'total_amount' => $payment->getTotalAmount(),
					'currency_code' => $payment->getCurrencyCode(),
					'details' => $payment->getDetails(),
			));
		}

	}

}
