<?php

namespace Phpeshop\ShoppaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse; 

use Symfony\Component\HttpFoundation\Session\SessionInterface;

// Entity
use Phpeshop\ShoppaymentBundle\Entity\Wallet;
// Catch db extention
use Doctrine\DBAL\DBALException;

class WalletController extends Controller
{

	/**
	 * @Route("/qiwiSettings", name="qiwiSettings")
	 */
	public function qiwiSettingsAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('cpanel');
		}

		$em = $this->getDoctrine()->getManager();
		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);

		$qiwi_wallets = $Wallet_repo->findBy([
			'nameWallet' => "qiwi",
			'entry_status' => 1
		],[]);


		return $this->render('@ShoppaymentView/Qiwi/qiwi_settings.html.twig', [
			'qiwi_wallets' => $qiwi_wallets
		]);

		// return new JsonResponse();

	}


	/**
	 * @Route("/newWallet", name="newWallet")
	 */
	public function newWalletAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);
		$wallet = new Wallet();

		$qiwi_number = $request->request->get('number');
		$token = $request->request->get('token');

		if (empty($qiwi_number) ) {
			$this->addFlash(
				'error',
				'Не указан номер qiwi кошелька'
			);

			return $this->redirectToRoute('qiwiSettings');
		}

		if (empty($token) ) {
			$this->addFlash(
				'default',
				'Не указан Qiwi token, Невозможно будет определить заказ оплачен или нет'
			);
		}

		/*------------------------------------------------------------
								INSERT ON DB
		--------------------------------------------------------------*/

		try{
			/*------------Set Name wallet----------------------*/
			$wallet->setNamewallet('qiwi');

			/*------------Set Detail wallet----------------------*/
			$Detail_wallet = [
				'walletNr' => $qiwi_number,
				'token' => $token,
			];

			$wallet->setDetailwallet( $Detail_wallet );

			/*------------Wallet Main----------------------*/

			/* ---------------------------------------------------------------
						declate another walllet secondary
			--------------------------------------------------------------*/
			$current_wallets_main = $Wallet_repo->findBy([
				'nameWallet' => "qiwi",
				"mainWallet" => 1
			], []);

			if ($current_wallets_main) {
				// methodfindBy return arrray,
				// posiible we have several wallets with status main.
				// Nedd to change her status to secondary 
				foreach ($current_wallets_main as $key => $value) {

					$wallet_main_to_second = $Wallet_repo->find($value->getId());
					if ($wallet_main_to_second) {
						$wallet_main_to_second->setMainWallet(null);
						$em->persist($wallet_main_to_second);
						$em->flush();
					}
				}
			}

			/*  At this moment when we add new wallet he make main for payments */
			$wallet->setMainWallet(1);
			$wallet->setEntryStatus(1);

			$em->persist($wallet);
			$em->flush();

		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('qiwiSettings');
		}

		return $this->redirectToRoute('qiwiSettings');

	}

	/**
	 * @Route("/updateWallet", name="updateWallet")
	 */
	public function updateWalletAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('homepage');
		}

		// Get parametres
		$wallet_id = $request->request->get('wallet_id');
		$wallet_nr = $request->request->get('wallet_nr');
		$token = $request->request->get('token');

		$main_wallet = $request->request->get('main_wallet');

		$Detail_wallet = [
			'walletNr' => $wallet_nr,
			'token' => $token
		];

		$em = $this->getDoctrine()->getManager();

		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);

		try {
			$wallet = $Wallet_repo->find($wallet_id);

			if ($wallet) {
				$wallet->setDetailwallet( $Detail_wallet );

				/*------------Wallet Main----------------------*/
				if ($main_wallet) {

					/* ---------------------------------------------------------------
								declate another walllet secondary
					--------------------------------------------------------------*/
					$current_wallets_main = $Wallet_repo->findBy([
						'nameWallet' => "qiwi",
						"mainWallet" => 1
					], []);

					if ($current_wallets_main) {
						// methodfindBy return arrray,
						// posiible we have several wallets with status main.
						// Nedd to change her status to secondary 
						foreach ($current_wallets_main as $key => $value) {

							$wallet_main_to_second = $Wallet_repo->find($value->getId());
							if ($wallet_main_to_second) {
								$wallet_main_to_second->setMainWallet(null);
								$em->persist($wallet_main_to_second);
								$em->flush();
							}
						}
					}

					/*  At this moment when we add new wallet he make main for payments */
					$wallet->setMainWallet(1);
				}
				/*------------END Wallet Main----------------------*/

				$em->persist($wallet);
				$em->flush();

				$this->addFlash(
					'default',
					"Информация успешно обновлена"
				);
			}else{
				$this->addFlash(
					'error',
					"Не найден кошелёк с таким ИД"
				);
			}
				return $this->redirectToRoute('qiwiSettings');

		} catch (Exception $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('qiwiSettings');
		}

	}

	/**
	 * @Route("/removeWallet/{wid}", name="removeWallet")
	 */
	public function removeWalletAction(Request $request, $wid)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();

		$Wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);

		try {
			$wallet = $Wallet_repo->find($wid);

			if ($wallet) {
				$wallet->setEntryStatus(0);
				$em->persist($wallet);
				$em->flush();

				$this->addFlash(
					'success',
					"Кошелек был успешно удалён"
				);
			}else{
				$this->addFlash(
					'error',
					"Не найден кошелёк с таким ИД"
				);
			}

			return $this->redirectToRoute('qiwiSettings');

		} catch (Exception $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('qiwiSettings');
		}

		return $this->redirectToRoute('qiwiSettings');
	}


}
