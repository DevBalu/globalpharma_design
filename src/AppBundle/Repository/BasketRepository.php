<?php

namespace AppBundle\Repository;
use AppBundle\Entity\products;
use AppBundle\Entity\Basket;
use AppBundle\Entity\Course;

/**
 * BasketRepository
 */

class BasketRepository extends \Doctrine\ORM\EntityRepository
{
	/*---------------------------------------------------------------------------
		Product added by user inclusive and product
		which at moment not in stock
	--------------------------------------------------------------------------*/
	public function userBasketProducts($uid)
	{
		return $this->getEntityManager()->createQuery('
			SELECT p, b.idUser, b.productid, b.count, b.id
				FROM AppBundle:products p
					JOIN AppBundle:Basket b
					WITH b.productid = p.id AND p.entry_status = 1 
			WHERE b.idUser='.$uid.'
		')->getArrayResult();
	}

	/*---------------------------------------------------------------------------
					Product added by user on basket 
						STRONG 
					which is in stock
	--------------------------------------------------------------------------*/
	public function userBasketProductsChecked($uid)
	{
		return $this->getEntityManager()->createQuery('
			SELECT p, b.idUser, b.productid, b.count, b.id
			FROM AppBundle:products p
				JOIN AppBundle:Basket b
				WITH b.productid = p.id AND p.entry_status = 1 AND p.inStock = 1
			WHERE b.idUser='.$uid.'
		')->getArrayResult();
	}

	/*---------------------------------------------------------------------------
				Courses added by user on basket
	--------------------------------------------------------------------------*/
	public function userBasketCourse($uid)
	{
		$result =  $this->getEntityManager()->createQuery('
			SELECT c, b.idUser, b.courseid, b.count, b.id
				FROM AppBundle:Course c
					JOIN AppBundle:Basket b
					WITH b.courseid = c.id AND c.entry_status = 1
			WHERE b.idUser = ' . $uid)->getArrayResult();

		return $result;
	}

	/*---------------------------------------------------------------------------
					Courses added by user on basket 
						STRONG 
					which is in stock
	--------------------------------------------------------------------------*/

	public function userBasketCourseChecked($uid)
	{
		$result =  $this->getEntityManager()->createQuery('
			SELECT c, b.idUser, b.courseid, b.count, b.id
				FROM AppBundle:Course c
					JOIN AppBundle:Basket b
					WITH b.courseid = c.id AND c.entry_status = 1 AND c.instock = 1
			WHERE b.idUser = ' . $uid)->getArrayResult();

		return $result;
	}

	/*---------------------------------------------------------------------------
			Merge products and courses added by user on basket
	--------------------------------------------------------------------------*/
	public function fullUserBasket($uid){
		$products = $this->userBasketProducts($uid);
		$course = $this->userBasketCourse($uid);

		$result = [
			'products' => [],
			'course' => []
		];

		foreach ($products as $key => $value) {
			$result['products'][] =  $value;
		}

		foreach ($course as  $key => $value) {
			$result['course'][] = $value;
		}

		return $result;
	}

	/*---------------------------------------------------------------------------
		Actual full user basket with products which STRONG "in stock"
	--------------------------------------------------------------------------*/
	public function strongFullUserBasket($uid){
		$products = $this->userBasketProductsChecked($uid);
		$course = $this->userBasketCourseChecked($uid);

		$result = [
			'products' => [],
			'course' => []
		];

		foreach ($products as $value) {
			$result['products'][] = $value;
		}

		foreach ($course as $value) {
			$result['course'][] = $value;
		}
		return $result;
	}

	/*---------------------------------------------------------------------------
			Method calculate total amount for current state of basket
	--------------------------------------------------------------------------*/
	public function recalculateSubtotals($uid)
	{
		$products = $this->userBasketProductsChecked($uid);
		$course = $this->userBasketCourse($uid);

		$sum = 0;

		foreach ($products as $key => $item) {
			$sum += $item['count'] * $item[0]['price'];
		}


		foreach ($course as $key => $item) {
			$sum += $item['count'] * $item[0]['price'];
		}

		return $sum;
	}

	/*---------------------------------------------------------------------------
		* Custom method which empty count of product added on basket
		* Use which product is added on basket but at moment not in stock
		* maked for prevent more error with collection of order
	--------------------------------------------------------------------------*/
	public function emptyCountProd($uid){
		$em = $this->getEntityManager();

		$notInStock = $em->createQuery('
			SELECT p, b.idUser, b.productid, b.count, b.id
			FROM AppBundle:products p
				JOIN AppBundle:Basket b
				WITH b.productid = p.id AND p.entry_status = 1 AND p.inStock = 0
			WHERE b.idUser='.$uid.'
		')->getArrayResult();

		foreach ($notInStock as $product) {
			$this->getEntityManager()
			->createQuery('
					UPDATE AppBundle:Basket b
					SET b.count = 1
					WHERE b.productid = ' . $product['productid']
				)
			->execute();
		}
	}
	/*---------------------------------------------------------------------------
		* Custom method which empty count of courses added on basket
		* Use which product is added on basket but at moment not in stock
		* maked for prevent more error with collection of order
	--------------------------------------------------------------------------*/
	public function emptyCountCourse($uid){
		$em = $this->getEntityManager();

		$notInStock = $em->createQuery('
			SELECT c, b.idUser, b.courseid, b.count, b.id
			FROM AppBundle:Course c
				JOIN AppBundle:Basket b
				WITH b.courseid = c.id AND c.entry_status = 1 AND c.instock = 0
			WHERE b.idUser='.$uid.'
		')->getArrayResult();

		foreach ($notInStock as $course) {
			if ($course['count'] > 1) {
				$this->getEntityManager()
				->createQuery('
						UPDATE AppBundle:Basket b
						SET b.count = 1
						WHERE b.courseid = ' . $course['id']
					)
				->execute();
			}
		}
	}


}
