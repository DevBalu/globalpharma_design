<?php

namespace AppBundle\Repository;

use AppBundle\Entity\category;
use AppBundle\Entity\subcategory;

class categoryRepository extends \Doctrine\ORM\EntityRepository
{
	/*
	* A method that returns category and all subcategories at this category 
	*/
	public function catRell()
	{
		$cat = $this->getEntityManager()
		->createQuery('
				SELECT c.id, c.name
				FROM AppBundle:category c
				WHERE c.entry_status = 1
				ORDER BY c.name ASC
			')
		->getarrayResult();

		$result = [];

		foreach ($cat as $key => $value) {

			$sub = $this->getEntityManager()
			->createQuery('
				SELECT s.id, s.name
				FROM AppBundle:subcategory s
				WHERE s.categoryid = '. $value["id"] .'
				AND s.entry_status = 1
			')
			->getarrayResult();

			$category = [
				'cat_id' => $value['id'],
				'name' => $value['name'],
				'subcategory' => $sub
			];
			$result[] = $category;
		}

		return $result;
	}
}

