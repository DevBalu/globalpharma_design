<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderControllerControllerTest extends WebTestCase
{
    public function testNeworder()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/newOrder');
    }

    public function testListorders()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/listOrders');
    }

    public function testRemoveorder()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/removeOrder');
    }

    public function testCancelorder()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/cancelOrder');
    }

}
