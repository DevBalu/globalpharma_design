<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class customer_accountControllerTest extends WebTestCase
{
    public function testCustomer_account()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customer_account');
    }

    public function testCustomer_orders()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customer_orders');
    }

    public function testCustomer_wishlist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customer_wishlist');
    }

}
