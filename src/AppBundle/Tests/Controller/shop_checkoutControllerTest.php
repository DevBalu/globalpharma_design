<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class shop_checkoutControllerTest extends WebTestCase
{
    public function testShop_checkout_one()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/shop_checkout_one');
    }

    public function testShop_checkout_two()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/shop_checkout_two');
    }

    public function testShop_checkout_three()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/shop_checkout_three');
    }

    public function testShop_checkout_four()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/shop_checkout_four');
    }

}
