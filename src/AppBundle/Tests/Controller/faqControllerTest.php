<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class faqControllerTest extends WebTestCase
{
    public function testFaqmain()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/faqMain');
    }

    public function testFaqdelivery()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/faqDelivery');
    }

    public function testFaqrisks()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/faqRisks');
    }

    public function testFaqpartnership()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/faqPartnership');
    }

    public function testFaqrules()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/faqRules');
    }

    public function testFaqdiscount()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/faqDiscount');
    }

}
