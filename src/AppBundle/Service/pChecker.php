<?php
namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use AppBundle\Entity\category;
use AppBundle\Entity\subcategory;
use AppBundle\Entity\products;
use AppBundle\Entity\Manufacturers;

class pChecker{

	/*                 Check if product is added on  
	*
	------------------------------------------------------------------------------*/
	public function ifAdded($ancor, $products, $repo, $user_id)
	{
		foreach ($products as $prod_key => $prod){
			$any_res = $repo->findBy(
				[
					'productid' => $prod->id,
					'idUser' => $user_id
				],
				[]
			);
			// if user has been add product on basket
			if ($any_res) {
				$products[$prod_key] = (object)array_merge( (array)$products[$prod_key], array( $ancor => true ) );
			}else {
				$products[$prod_key] = (object)array_merge( (array)$products[$prod_key], array( $ancor => false ) );
			}

		}

		return $products;
	}

	public function ifAddedTwo($ancor, $products, $repo, $user_id)
	{
		foreach ($products as $prod_key => $prod){
			$any_res = $repo->findBy(
				[
					'productid' => $prod[0]->id,
					'idUser' => $user_id
				],
				[]
			);
			// if user has been add product on basket
			if (!empty($any_res)) {
				$products[$prod_key] = (array)array_merge( (array)$products[$prod_key], array( $ancor => true ) );
			}else {
				$products[$prod_key] = (array)array_merge( (array)$products[$prod_key], array( $ancor => false ) );
			}

		}

		return $products; 
	}

	public function ifCourseAdded($ancor, $courses, $repo, $user_id)
	{
		foreach ($courses as $course_key => $course){
			$any_res = $repo->findBy(
				[
					'courseid' => $course->getId(),
					'idUser' => $user_id
				],
				[]
			);

			// if user has been add product on basket
			if (!empty($any_res)) {
				$courses[$course_key] = (array)array_merge( (array)$courses[$course_key], array( $ancor => true ) );
			}else {
				$courses[$course_key] = (array)array_merge( (array)$courses[$course_key], array( $ancor => false ) );
			}

		}

		return $courses; 
	}

	/*                 return count of entries on this table  
	------------------------------------------------------------------------------*/
	public function entries_count($products, $repo, $user_id)
	{
		$count = 0;
		foreach ($products as $prod_key => $prod){
			$any_res = $repo->findBy(
				[
					'productid' => $prod->id,
					'idUser' => $user_id
				],
				[]
			);
			// if user has been add product on basket
			if ($any_res) {
				$count++;
			}
		}

		return $count;
	}

	// if products is type array
	public function ifNewOne($products)
	{
		foreach ($products as $prod_key => $prod){
			// if date when product is added is less than one week 
			$cd = (int)date('d');
			$sd = (int)$prod[0]->date->format('d');
			$res = abs($cd - $sd);

			if ($res <= 7) {
				$products[$prod_key] = (array)array_merge( (array)$products[$prod_key], array( 'new' => true ) );
			}else {
				$products[$prod_key] = (array)array_merge( (array)$products[$prod_key], array( 'new' => false ) );
			}
		}
		return $products;
	}

	// if products is type object
	public function ifNewTwo($products)
	{
		// $products = (array)array_merge( (array)$products);

		foreach ($products as $prod_key => $prod){
			// if date when product is added is less than one week 
			$cd = (int)date('d');

			$sd = (int)$prod->date->format('d');
			$res = abs($cd - $sd);

			if ($res <= 7) {
				$products[$prod_key] = (array)array_merge( (array)$products[$prod_key], array( 'new' => true ) );
			}else {
				$products[$prod_key] = (array)array_merge( (array)$products[$prod_key], array( 'new' => false ) );
			}
		}
		return $products;
	}


	public function catProdCount($catrepo, $prodrepo){
		foreach ($catrepo as $ckey => $cat) {
			$prod_ent = $prodrepo->findBy([
				'categoryId' => $cat['cat_id']
			], []);
			$catrepo[$ckey]['pcount'] = 0;
			foreach ($cat['subcategory'] as $skey => $subcat) {
				$catrepo[$ckey]['subcategory'][$skey]['pcount'] = 0;
			}
		}

		foreach ($catrepo as $ckey => $cat) {
			$prod_ent = $prodrepo->findBy([
				'categoryId' => $cat['cat_id'],
				'entry_status' => 1
			], []);

			if ($prod_ent) {
				$catrepo[$ckey]['pcount'] += sizeof($prod_ent);
			}

			foreach ($cat['subcategory'] as $skey => $subcat) {
				$sprod_ent = $prodrepo->findBy([
					'categoryId' => $cat['cat_id'],
					'subcategoryId' => $subcat['id'],
					'entry_status' => 1
				], []);

				if ($sprod_ent) {
					$catrepo[$ckey]['subcategory'][$skey]['pcount'] += sizeof($sprod_ent);
				}

			}
		}

		return $catrepo;
	}

} /*./ END class*/