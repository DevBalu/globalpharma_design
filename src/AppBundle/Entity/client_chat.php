<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * client_chat
 *
 * @ORM\Table(name="client_chat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\client_chatRepository")
 */
class client_chat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer")
     */
    private $idUser;

    /**
     * @var int
     *
     * @ORM\Column(name="id_manager", type="integer")
     */
    private $idManager;

    /**
     * @var array
     *
     * @ORM\Column(name="correspondence", type="array")
     */
    private $correspondence;

    /**
     * @var int
     *
     * @ORM\Column(name="assesment_of_correspondece", type="integer")
     */
    private $assesmentOfCorrespondece;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return client_chat
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idManager
     *
     * @param integer $idManager
     *
     * @return client_chat
     */
    public function setIdManager($idManager)
    {
        $this->idManager = $idManager;

        return $this;
    }

    /**
     * Get idManager
     *
     * @return int
     */
    public function getIdManager()
    {
        return $this->idManager;
    }

    /**
     * Set correspondence
     *
     * @param array $correspondence
     *
     * @return client_chat
     */
    public function setCorrespondence($correspondence)
    {
        $this->correspondence = $correspondence;

        return $this;
    }

    /**
     * Get correspondence
     *
     * @return array
     */
    public function getCorrespondence()
    {
        return $this->correspondence;
    }

    /**
     * Set assesmentOfCorrespondece
     *
     * @param integer $assesmentOfCorrespondece
     *
     * @return client_chat
     */
    public function setAssesmentOfCorrespondece($assesmentOfCorrespondece)
    {
        $this->assesmentOfCorrespondece = $assesmentOfCorrespondece;

        return $this;
    }

    /**
     * Get assesmentOfCorrespondece
     *
     * @return int
     */
    public function getAssesmentOfCorrespondece()
    {
        return $this->assesmentOfCorrespondece;
    }

    /*              date
     -------------------------------------------------------- */
    /**
     * Set date
     *
     * @param string $date
     *
     * @return client_chat
     */
    public function setDate()
    {
        $this->date = new \DateTime();

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }
}

