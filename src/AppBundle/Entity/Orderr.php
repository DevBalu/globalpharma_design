<?php
namespace AppBundle\Entity; 
use Doctrine\ORM\Mapping as ORM; 

/** 
 * Order
 *
 * @ORM\Table(name="Orderr")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 */
class Orderr
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer")
     */
    public $idUser;

    /**
     * @var int
     *
     * @ORM\Column(name="id_recipient", type="integer")
     */
    public $idRecipient;

    /**
     * @var array
     *
     * @ORM\Column(name="delivery_address", type="array", nullable=true)
     */
    public $delivery_address;

    /**
     * @var array
     *
     * @ORM\Column(name="products", type="array", nullable=true)
     */
    public $products;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_method", type="string", nullable=true)
     */
    public $paymentMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_status", type="string", nullable=true)
     */
    public $paymentStatus;

    /**
     * @var int
     *
     * @ORM\Column(name="deliveryMethod",  type="string", length=255, nullable=true)
     */
    public $deliveryMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_status", type="string", length=255, nullable=true)
     */
    public $deliveryStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="order_status", type="string", length=255, nullable=true)
     */
    public $order_status;

    /**
     * @var error_message
     *
     * @ORM\Column(name="error_message", type="array", length=255, nullable=true)
     */
    public $error_message;

    /**
     * @var string
     *
     * @ORM\Column(name="track_сode", type="string", length=255, nullable=true)
     */
    public $trackСode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trackDate", type="datetime", nullable=true)
     */
    public $trackDate;

    /**
     * @var string
     *
     * @ORM\Column(name="rate", type="string", length=255, nullable=true)
     */
    public $rate;

    /**
     * @var array
     *
     * @ORM\Column(name="coupon", type="array", nullable=true)
     */
    public $coupon;

    /**
     * @var float
     *
     * @ORM\Column(name="delivery_cost", type="integer", nullable=true)
     */
    public $deliveryCost;


    /**
     * @var string
     *
     * @ORM\Column(name="final_cost", type="string", length=255, nullable=true)
     */
    public $finalCost;

    /**
     * @var string
     *
     * @ORM\Column(name="amount_difference", type="string", length=255, nullable=true)
     */
    public $amount_difference;

    /**
     * @var string
     *
     * @ORM\Column(name="uniqid", type="string", length=255)
     */
    public $uniqid;


    /**
     * @var int
     *
     * @ORM\Column(name="entry_status", type="boolean")
     */
    public $entry_status;


    /**
     * @var array
     *
     * @ORM\Column(name="checked", type="array", nullable=true)
     */
    public $checked;

    /**
     * @var array
     *
     * @ORM\Column(name="trns_history", type="array", nullable=true)
     */
    public $trns_history;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date", type="datetime", nullable=true)
     */
    public $payment_date;

    public function __construct()
    {
        $this->entry_status = true;
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


/*-------------------------------------------------------*/
/*                      idUser */
/*-------------------------------------------------------*/

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

/*-------------------------------------------------------*/
/*                      idRecipient */
/*-------------------------------------------------------*/

    /**
     * Set idRecipient
     *
     * @param integer $idRecipient
     *
     * @return setIdRecipient
     */
    public function setIdRecipient($idRecipient)
    {
        $this->idRecipient = $idRecipient;

        return $this;
    }

    /**
     * Get idRecipient
     *
     * @return int
     */
    public function getIdRecipient()
    {
        return $this->idRecipient;
    }

/*-------------------------------------------------------*/
/*                      delivery_address */
/*-------------------------------------------------------*/

    /**
     * Set delivery_address
     *
     * @param integer $delivery_address
     *
     * @return delivery_address
     */
    public function setDeliveryaddress($delivery_address)
    {
        $this->delivery_address = $delivery_address;

        return $this;
    }

    /**
     * Get delivery_address
     *
     * @return array
     */
    public function getDeliveryaddress()
    {
        return $this->delivery_address;
    }



/*-------------------------------------------------------*/
/*                      products */
/*-------------------------------------------------------*/
    /**
     * Set products
     *
     * @param array $products
     *
     * @return orders
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Get products
     *
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

/*-------------------------------------------------------*/
/*                      paymentMethod */
/*-------------------------------------------------------*/

    /**
     * Set paymentMethod
     *
     * @param integer $paymentMethod
     *
     * @return string
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

/*-------------------------------------------------------*/
/*                      paymentStatus */
/*-------------------------------------------------------*/
    /**
     * Set paymentStatus
     *
     * @param string $paymentStatus
     *
     * @return string
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

/*-------------------------------------------------------*/
/*                      deliveryMethod */
/*-------------------------------------------------------*/

    /**
     * Set deliveryMethod
     *
     * @param integer $deliveryMethod
     *
     * @return deliveryMethod
     */
    public function setDeliveryMethod($deliveryMethod)
    {
        $this->deliveryMethod = $deliveryMethod;

        return $this;
    }

    /**
     * Get deliveryMethod
     *
     * @return int
     */
    public function getDeliveryMethod()
    {
        return $this->deliveryMethod;
    }

/*-------------------------------------------------------*/
/*                      deliveryStatus */
/*-------------------------------------------------------*/

    /**
     * Set deliveryStatus
     *
     * @param string $deliveryStatus
     *
     * @return orders
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return string
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

/*-------------------------------------------------------*/
/*                      order_status */
/*-------------------------------------------------------*/

    /**
     * Set order_status
     *
     * @param string $order_status
     *
     * @return orders
     */
    public function setOrderStatus($order_status)
    {
        $this->order_status = $order_status;

        return $this;
    }

    /**
     * Get order_status
     *
     * @return string
     */
    public function getOrderStatus()
    {
        return $this->order_status;
    }


/*-------------------------------------------------------*/
/*                      error_message */
/*-------------------------------------------------------*/

    /**
     * Set error_message
     *
     * @param array $error_message
     *
     * @return orders
     */
    public function setErrorMessage($error_message)
    {
        $this->error_message = $error_message;

        return $this;
    }

    /**
     * Get error_message
     *
     * @return array
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }

/*-------------------------------------------------------*/
/*                      trackСode */
/*-------------------------------------------------------*/

    /**
     * Set trackСode
     *
     * @param string $trackСode
     *
     * @return orders
     */
    public function setTrackСode($trackСode)
    {
        $this->trackСode = $trackСode;

        return $this;
    }

    /**
     * Get trackСode
     *
     * @return string
     */
    public function getTrackСode()
    {
        return $this->trackСode;
    }

/*-------------------------------------------------------*/
/*                      trackDate */
/*-------------------------------------------------------*/
    /**
     * Set trackDate
     *
     * @param date $trackDate
     *
     * @return orders
     */
    public function setTrackDate()
    {
        $this->trackDate = new \DateTime();

        return $this;
    }

    /**
     * Get trackDate
     *
     * @return date
     */
    public function getTrackDate()
    {
        return $this->trackDate;
    }

/*-------------------------------------------------------*/
/*                      rate */
/*-------------------------------------------------------*/

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return orders
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get trackСode
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }


/*-------------------------------------------------------*/
/*                      finalCost */
/*-------------------------------------------------------*/

    /**
     * Set finalCost
     *
     * @param string $finalCost
     *
     * @return string
     */
    public function setFinalCost($finalCost)
    {
        $this->finalCost = $finalCost;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return string
     */
    public function getFinalCost()
    {
        return $this->finalCost;
    }


/*-------------------------------------------------------*/
/*                      amount_difference */
/*-------------------------------------------------------*/

    /**
     * Set amount_difference
     *
     * @param string $amount_difference
     *
     * @return string
     */
    public function setAmountDifference($amount_difference)
    {
        $this->amount_difference = $amount_difference;

        return $this;
    }

    /**
     * Get amount_difference
     *
     * @return string
     */
    public function getAmountDifference()
    {
        return $this->amount_difference;
    }



/*-------------------------------------------------------*/
/*                      coupon */
/*-------------------------------------------------------*/

    /**
     * Set coupon
     *
     * @param float $coupon
     *
     * @return orders
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;

        return $this;
    }

    /**
     * Get coupon
     *
     * @return array
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

/*-------------------------------------------------------*/
/*                      deliveryCost */
/*-------------------------------------------------------*/

    /**
     * Set deliveryCost
     *
     * @param float $deliveryCost
     *
     * @return orders
     */
    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    /**
     * Get deliveryCost
     *
     * @return float
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }

/*-------------------------------------------------------*/
/*                      uniqid */
/*-------------------------------------------------------*/

    /**
     * Set deliveryCost
     *
     * @param float $deliveryCost
     *
     * @return uniqid
     */
    public function setUniqid($uniqid)
    {
        $this->uniqid = $uniqid;

        return $this;
    }

    /**
     * Get uniqid
     *
     * @return string
     */
    public function getUniqid()
    {
        return $this->uniqid;
    }

    /*-------------------------------------------------------*/
    /*              entry_status
     -------------------------------------------------------- */
    /**
     * Set entry_status
     *
     * @param integer $entry_status
     *
     * @return entry_status
     */
    public function setEntryStatus($entry_status)
    {
        $this->entry_status = $entry_status;

        return $this;
    }

    /**
     * Get entry_status
     *
     * @return booelan
     */
    public function getEntryStatus()
    {
        return $this->entry_status;
    }

/*-------------------------------------------------------*/
/*                      checked */
/*-------------------------------------------------------*/

    /**
     * Set checked
     *
     * @param array $checked
     *
     * @return checked
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get checked
     *
     * @return array
     */
    public function getChecked()
    {
        return $this->checked;
    }

/*-------------------------------------------------------*/
/*                      trns_history */
/*-------------------------------------------------------*/

    /**
     * Set trns_history
     *
     * @param array $trns_history
     *
     * @return trns_history
     */
    public function setTrnsHistory($trns_history)
    {
        $this->trns_history = $trns_history;

        return $this;
    }

    /**
     * Get trns_history
     *
     * @return array
     */
    public function getTrnsHistory()
    {
        return $this->trns_history;
    }



    /*              date
     -------------------------------------------------------- */
    /**
     * Set date
     *
     * @param string $date
     *
     * @return orders
     */
    public function setDate()
    {
        $this->date = new \DateTime();

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }


    /*              payment_date
     -------------------------------------------------------- */
    /**
     * Set payment_date
     *
     * @param string $payment_date
     *
     * @return orders
     */
    public function setPayment_date()
    {
        $this->payment_date = new \DateTime();

        return $this;
    }
    /**
     * Set payment_date
     *
     * @param string $payment_date
     *
     * @return orders
     */
    public function setCustomPayment_date($payment_date)
    {
        $this->payment_date = $payment_date;

        return $this;
    }

    public function getCurrentDate()
    {
        return  new \DateTime();
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getPayment_date()
    {
        return $this->payment_date;
    }


}

