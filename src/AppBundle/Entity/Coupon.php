<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coupon
 *
 * @ORM\Table(name="coupon")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CouponRepository")
 */
class Coupon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fromdate", type="datetime")
     */
    private $fromdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="todate", type="datetime")
     */
    private $todate;

    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float")
     */
    private $percent;

    /**
     * @var bool
     *
     * @ORM\Column(name="entry_status", type="boolean", nullable=true)
     */
    private $entry_status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime")
     */
    private $createdate;

    public function __construct()
    {
        $this->entry_status = true;
        $this->createdate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Coupon
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fromdate
     *
     * @param \DateTime $fromdate
     *
     * @return Coupon
     */
    public function setFromdate($fromdate)
    {
        $this->fromdate =  new \DateTime($fromdate);

        return $this;
    }

    /**
     * Get fromdate
     *
     * @return \DateTime
     */
    public function getFromdate()
    {
        return $this->fromdate;
    }

    /**
     * Set todate
     *
     * @param \DateTime $todate
     *
     * @return Coupon
     */
    public function setTodate($todate)
    {
        $this->todate = new \DateTime($todate);

        return $this;
    }

    /**
     * Get todate
     *
     * @return \DateTime
     */
    public function getTodate()
    {
        return $this->todate;
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return Coupon
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set entry_status
     *
     * @param boolean $entry_status
     *
     * @return Coupon
     */
    public function setEntryStatus($entry_status)
    {
        $this->entry_status = $entry_status;

        return $this;
    }

    /**
     * Get entryStatus
     *
     * @return bool
     */
    public function getEntryStatus()
    {
        return $this->entry_status;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return Coupon
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }
}

