<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Delivery_cost
 *
 * @ORM\Table(name="delivery_cost")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Delivery_costRepository")
 */
class Delivery_cost
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="price_delimiter", type="integer", unique=true)
     */
    private $priceDelimiter;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_cost", type="integer", length=255)
     */
    private $deliveryCost;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set priceDelimiter
     *
     * @param string $priceDelimiter
     *
     * @return Delivery_cost
     */
    public function setPriceDelimiter($priceDelimiter)
    {
        $this->priceDelimiter = $priceDelimiter;

        return $this;
    }

    /**
     * Get priceDelimiter
     *
     * @return string
     */
    public function getPriceDelimiter()
    {
        return $this->priceDelimiter;
    }

    /**
     * Set deliveryCost
     *
     * @param string $deliveryCost
     *
     * @return Delivery_cost
     */
    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    /**
     * Get deliveryCost
     *
     * @return string
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }
}

