<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * products
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\productsRepository")
 */
class products
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;


    /**
     * @var int
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;


    /**
     * @var string
     *
     * @ORM\Column(name="categoryId", type="integer", nullable=true)
     */
    public $categoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="subcategoryId", type="integer", nullable=true)
     */
    public $subcategoryId;

    /**
     * @var int
     *
     * @ORM\Column(name="manufacturer_id", type="integer", nullable=true)
     */
    public $manufacturerid;

    /**
     * @var string
     *
     * @ORM\Column(name="count_dosage", type="string")
     */
    public $count_dosage;

    /**
     * @var string
     *
     * @ORM\Column(name="active_substance", type="string", nullable=true)
     */
    public $active_substance;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    public $content;

    /**
     * @var string
     *
     * @ORM\Column(name="images",  type="array", nullable=true)
     */
    public $images;

    /**
     * @var string
     *
     * @ORM\Column(name="status",type="string", length=255, nullable=true)
     */
    public $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rating", type="boolean", nullable=true)
     */
    public $rating;

    /**
    * @var bool
    *
    * @ORM\Column(name="in_stock", type="boolean")
    */
    public $inStock;

    /**
     * @var string
     *
     * @ORM\Column(name="product_quantity", type="text", nullable=true)
     */
    public $product_quantity;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    public $price;

    /**
     * @var int
     *
     * @ORM\Column(name="old_price", type="integer", nullable=true)
     */
    public $oldprice;

    /**
     * @var int
     *
     * @ORM\Column(name="discount", type="array", nullable=true)
     */
    public $discount;

    /**
     * @var int
     *
     * @ORM\Column(name="entry_status", type="boolean")
     */
    public $entry_status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    public function __construct()
    {
        $this->entry_status = true;
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return products
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }
    /**
    * Set categoryId
    *
    * @param integer $categoryId
    *
    * @return categoryId
    */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get subcategoryId
     *
     * @return int
     */
    public function getsubcategoryId()
    {
        return $this->subcategoryId;
    }

    /**
    * Set subcategoryId
    *
    * @param integer $subcategoryId
    *
    * @return subcategoryId
    */
    public function setSubcategoryId($subcategoryId)
    {
        $this->subcategoryId = $subcategoryId;

        return $this;
    }

    /**
     * Get manufacturerid
     *
     * @return int
     */
    public function getManufacturerid()
    {
        return $this->manufacturerid;
    }

    /**
     * Set manufacturerid
     *
     * @param integer $manufacturerid
     *
     * @return manufacturerid
     */
    public function setManufacturerid($manufacturerid)
    {
        $this->manufacturerid = $manufacturerid;

        return $this;
    }

    /**
     * Get count_dosage
     *
     * @return string
     */
    public function getCount_dosage()
    {
        return $this->count_dosage;
    }

    /**
     * Set count_dosage
     *
     * @param string $count_dosage
     *
     * @return count_dosage
     */
    public function setCount_dosage($count_dosage)
    {
        $this->count_dosage = $count_dosage;

        return $this;
    }

    /**
     * Get active_substance
     *
     * @return string
     */
    public function getActive_substance()
    {
        return $this->active_substance;
    }

    /**
     * Set active_substance
     *
     * @param string $active_substance
     *
     * @return active_substance
     */
    public function setcActive_substance($active_substance)
    {
        $this->active_substance = $active_substance;
        return $this;
    }

   /**
     * Set content
     *
     * @param string $content
     *
     * @return products
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set images
     *
     * @param string $images
     *
     * @return products
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set rating
     *
     * @param boolean $rating
     *
     * @return rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * Get rating
     *
     * @return boolean
     */
    public function getRating()
    {
        return $this->rating;
    }

    /*---------------------------------------------------
                    inStock
    ---------------------------------------------------*/

    /**
     * Set inStock
     *
     * @param boolean $inStock
     *
     * @return inStock
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;
        return $this;
    }

    /**
     * Get inStock
     *
     * @return bool
     */
    public function getInStock()
    {
        return $this->inStock;
    }

    /*---------------------------------------------------
                    product_quantity
    ---------------------------------------------------*/
    /**
     * Set product_quantity
     *
     * @param string $product_quantity
     *
     * @return product_quantity
     */
    public function setProductQuantity($product_quantity)
    {
        $this->product_quantity = $product_quantity;
        return $this;
    }

    /**
     * Get product_quantity
     *
     * @return string
     */
    public function getProductQuantity()
    {
        return $this->product_quantity;
    }

    /*---------------------------------------------------
                    price
    ---------------------------------------------------*/

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return products
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /*              oldprice
     -------------------------------------------------------- */

    /**
     * Set oldprice
     *
     * @param integer $oldprice
     *
     * @return oldprice
     */
    public function setOldprice($oldprice)
    {
        $this->oldprice = $oldprice;

        return $this;
    }

    /**
     * Get oldprice 
     *
     * @return int
     */
    public function getOldprice()
    {
        return $this->oldprice;
    }

    /*              discount
     -------------------------------------------------------- */

    /**
     * Set discount
     *
     * @param integer $discount
     *
     * @return discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount 
     *
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /*              entry_status
     -------------------------------------------------------- */

    /**
     * Set entry_status
     *
     * @param integer $entry_status
     *
     * @return entry_status
     */
    public function setEntryStatus($entry_status)
    {
        $this->entry_status = $entry_status;

        return $this;
    }

    /**
     * Get entry_status
     *
     * @return booelan
     */
    public function getEntryStatus()
    {
        return $this->entry_status;
    }

    /*              date
     -------------------------------------------------------- */
    /**
     * Set date
     *
     * @param string $date
     *
     * @return oldprice
     */
    public function setDate()
    {
        $this->date = new \DateTime();

        return $this;
    }

    /**
     * Get date
     *
     * @param datetime $date
     *
     * @return products
     */
    public function getDate($date)
    {
        return $this->date;
    }




}

