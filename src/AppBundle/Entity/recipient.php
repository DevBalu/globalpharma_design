<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * recipient
 *
 * @ORM\Table(name="recipient")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\recipientRepository")
 */
class recipient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser", type="integer")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="potronymic", type="string", length=255, nullable=true)
     */
    private $potronymic;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="district", type="string", length=255, nullable=true)
     */
    private $district;

    /**
     * @var string
     *
     * @ORM\Column(name="locality_type", type="string", length=255, nullable=true)
     */
    private $localityType;

    /**
     * @var string
     *
     * @ORM\Column(name="locality_name", type="string", length=255, nullable=true)
     */
    private $localityName;

    /**
     * @var string
     *
     * @ORM\Column(name="poste_restante", type="string", length=255)
     */
    private $posteRestante;

    /**
     * @var string
     *
     * @ORM\Column(name="street_type", type="string", length=255, nullable=true)
     */
    private $streetType;

    /**
     * @var string
     *
     * @ORM\Column(name="street_name", type="string", length=255, nullable=true)
     */
    private $streetName;

    /**
     * @var string
     *
     * @ORM\Column(name="home", type="string", length=255, nullable=true)
     */
    private $home;

    /**
     * @var string
     *
     * @ORM\Column(name="fraction", type="string", length=255, nullable=true)
     */
    private $fraction;

    /**
     * @var string
     *
     * @ORM\Column(name="housing", type="string", length=255, nullable=true)
     */
    private $housing;

    /**
     * @var string
     *
     * @ORM\Column(name="porch", type="string", length=255, nullable=true)
     */
    private $porch;

    /**
     * @var string
     *
     * @ORM\Column(name="structure", type="string", length=255, nullable=true)
     */
    private $structure;

    /**
     * @var string
     *
     * @ORM\Column(name="room", type="string", length=255, nullable=true)
     */
    private $room;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;


    /**
     * @var int
     *
     * @ORM\Column(name="entry_status", type="boolean")
     */
    public $entry_status;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    public function __construct()
    {
        $this->entry_status = true;
        $this->date = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /*              idUser
     -------------------------------------------------------- */

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return recipient
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }


    /*              surname
     -------------------------------------------------------- */

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return recipient
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }


    /*              name
     -------------------------------------------------------- */

    /**
     * Set name
     *
     * @param string $name
     *
     * @return recipient
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /*              potronymic
     -------------------------------------------------------- */

    /**
     * Set potronymic
     *
     * @param string $potronymic
     *
     * @return recipient
     */
    public function setPotronymic($potronymic)
    {
        $this->potronymic = $potronymic;

        return $this;
    }

    /**
     * Get potronymic
     *
     * @return string
     */
    public function getPotronymic()
    {
        return $this->potronymic;
    }

    /*              phone
     -------------------------------------------------------- */

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return recipient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /*              postcode
     -------------------------------------------------------- */

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return recipient
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /*              region
     -------------------------------------------------------- */

    /**
     * Set region
     *
     * @param string $region
     *
     * @return recipient
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /*              district
     -------------------------------------------------------- */

    /**
     * Set district
     *
     * @param string $district
     *
     * @return recipient
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /*              localityType
     -------------------------------------------------------- */

    /**
     * Set localityType
     *
     * @param string $localityType
     *
     * @return recipient
     */
    public function setLocalityType($localityType)
    {
        $this->localityType = $localityType;

        return $this;
    }

    /**
     * Get localityType
     *
     * @return string
     */
    public function getLocalityType()
    {
        return $this->localityType;
    }

    /*              localityName
     -------------------------------------------------------- */

    /**
     * Set localityName
     *
     * @param string $localityName
     *
     * @return recipient
     */
    public function setLocalityName($localityName)
    {
        $this->localityName = $localityName;

        return $this;
    }

    /**
     * Get localityName
     *
     * @return string
     */
    public function getLocalityName()
    {
        return $this->localityName;
    }

    /*              posteRestante
     -------------------------------------------------------- */

    /**
     * Set posteRestante
     *
     * @param string $posteRestante
     *
     * @return recipient
     */
    public function setPosteRestante($posteRestante)
    {
        $this->posteRestante = $posteRestante;

        return $this;
    }

    /**
     * Get posteRestante
     *
     * @return string
     */
    public function getPosteRestante()
    {
        return $this->posteRestante;
    }

    /*              streetType
     -------------------------------------------------------- */

    /**
     * Set streetType
     *
     * @param string $streetType
     *
     * @return recipient
     */
    public function setStreetType($streetType)
    {
        $this->streetType = $streetType;

        return $this;
    }

    /**
     * Get streetType
     *
     * @return string
     */
    public function getStreetType()
    {
        return $this->streetType;
    }

    /*              streetName
     -------------------------------------------------------- */

    /**
     * Set streetName
     *
     * @param string $streetName
     *
     * @return recipient
     */
    public function setStreetName($streetName)
    {
        $this->streetName = $streetName;

        return $this;
    }

    /**
     * Get streetName
     *
     * @return string
     */
    public function getStreetName()
    {
        return $this->streetName;
    }

    /*              home
     -------------------------------------------------------- */

    /**
     * Set home
     *
     * @param string $home
     *
     * @return recipient
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return string
     */
    public function getHome()
    {
        return $this->home;
    }

    /*              fraction
     -------------------------------------------------------- */

    /**
     * Set fraction
     *
     * @param string $fraction
     *
     * @return recipient
     */
    public function setFraction($fraction)
    {
        $this->fraction = $fraction;

        return $this;
    }

    /**
     * Get fraction
     *
     * @return string
     */
    public function getFraction()
    {
        return $this->fraction;
    }

    /*              housing
     -------------------------------------------------------- */

    /**
     * Set housing
     *
     * @param string $housing
     *
     * @return recipient
     */
    public function setHousing($housing)
    {
        $this->housing = $housing;

        return $this;
    }

    /**
     * Get housing
     *
     * @return string
     */
    public function getHousing()
    {
        return $this->housing;
    }

    /*              porch
     -------------------------------------------------------- */

    /**
     * Set porch
     *
     * @param string $porch
     *
     * @return recipient
     */
    public function setPorch($porch)
    {
        $this->porch = $porch;

        return $this;
    }

    /**
     * Get porch
     *
     * @return string
     */
    public function getPorch()
    {
        return $this->porch;
    }

    /*              structure
     -------------------------------------------------------- */

    /**
     * Set structure
     *
     * @param string $structure
     *
     * @return recipient
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;

        return $this;
    }

    /**
     * Get structure
     *
     * @return string
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /*              room
     -------------------------------------------------------- */

    /**
     * Set room
     *
     * @param string $room
     *
     * @return recipient
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /*              comment
     -------------------------------------------------------- */
    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return recipient
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /*-------------------------------------------------------*/
    /*              entry_status
     -------------------------------------------------------- */
    /**
     * Set entry_status
     *
     * @param integer $entry_status
     *
     * @return entry_status
     */
    public function setEntryStatus($entry_status)
    {
        $this->entry_status = $entry_status;

        return $this;
    }

    /**
     * Get entry_status
     *
     * @return booelan
     */
    public function getEntryStatus()
    {
        return $this->entry_status;
    }


    /*              date
     -------------------------------------------------------- */
    /**
     * Set date
     *
     * @param string $date
     *
     * @return recipient
     */
    public function setDate()
    {
        $this->date = new \DateTime();

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }
}

