<?php

namespace AppBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;

/**
 * reviews 
 *
 * @ORM\Table(name="reviews")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\reviewsRepository")
 */
class reviews
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="name_user", type="string", length=255, nullable=true)
     */
    private $nameUser;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="comment_rating", type="integer", nullable=true)
     */
    private $commentRating;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="entry_status", type="boolean")
     */
    public $entry_status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    public function __construct()
    {
        $this->entry_status = true;
        $this->date = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

/*---------------------------------------------------
                    idUser
----------------------------------------------------*/

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return reviews
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

/*---------------------------------------------------
                    nameUser
----------------------------------------------------*/

    /**
     * Set nameUser
     *
     * @param integer $nameUser
     *
     * @return reviews
     */
    public function setNameUser($nameUser)
    {
        $this->nameUser = $nameUser;

        return $this;
    }

    /**
     * Get nameUser
     *
     * @return int
     */
    public function getNameUser()
    {
        return $this->nameUser;
    }


/*---------------------------------------------------
                    comment
----------------------------------------------------*/

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return reviews
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }


    /*---------------------------------------------------
                        commentRating
    ----------------------------------------------------*/

    /**
     * Set commentRating
     *
     * @param integer $commentRating
     *
     * @return reviews
     */
    public function setCommentRating($commentRating)
    {
        $this->commentRating = $commentRating;

        return $this;
    }

    /**
     * Get commentRating
     *
     * @return int
     */
    public function getCommentRating()
    {
        return $this->commentRating;
    }

    /*--------------------------------------------------------
                              status
     -------------------------------------------------------- */

    /**
     * Set status
     *
     * @param string $status
     *
     * @return reviews
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*              entry_status
     -------------------------------------------------------- */

    /**
     * Set entry_status
     *
     * @param integer $entry_status
     *
     * @return entry_status
     */
    public function setEntryStatus($entry_status)
    {
        $this->entry_status = $entry_status;

        return $this;
    }

    /**
     * Get entry_status
     *
     * @return booelan
     */
    public function getEntryStatus()
    {
        return $this->entry_status;
    }


    /*--------------------------------------------------------
                              date
     -------------------------------------------------------- */
    /**
     * Set date
     *
     * @param string $date
     *
     * @return commentRating
     */
    public function setDate()
    {
        $this->date = new \DateTime();

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }
}

