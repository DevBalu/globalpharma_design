<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CourseRepository")
 */
class Course
{
    /**
     * @var int 
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255)
     */
    public $price;

    /**
     * @var array
     *
     * @ORM\Column(name="content", type="array")
     */
    public $content;

   /**
     * @var int
     *
     * @ORM\Column(name="best", type="boolean")
     */
    public $best;

   /**
     * @var int
     *
     * @ORM\Column(name="entry_status", type="boolean")
     */
    public $entry_status;

   /**
     * @var int
     *
     * @ORM\Column(name="instock", type="boolean")
     */
    public $instock;


    /**
     * @var array
     *
     * @ORM\Column(name="warning", type="array", nullable=true)
     */
    public $warning;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;



    public function __construct()
    {
        $this->date = new \DateTime();
        $this->entry_status = true;
        $this->instock = true;
    }

    /*-----------------------------------------------
                    id
    -----------------------------------------------*/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /*-----------------------------------------------
                    name
    -----------------------------------------------*/

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Course
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /*-----------------------------------------------
                    price
    -----------------------------------------------*/

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Course
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /*-----------------------------------------------
                    content
    -----------------------------------------------*/
    /**
     * Set content
     *
     * @param array $content
     *
     * @return Course
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }

    /*-------------------------------------------------------*/
    /*              best
     -------------------------------------------------------- */
    /**
     * Set best
     *
     * @param integer $best
     *
     * @return best
     */
    public function setBest($best)
    {
        $this->best = $best;

        return $this;
    }

    /**
     * Get best
     *
     * @return booelan
     */
    public function getBest()
    {
        return $this->best;
    }


    /*-------------------------------------------------------*/
    /*              entry_status
     -------------------------------------------------------- */
    /**
     * Set entry_status
     *
     * @param integer $entry_status
     *
     * @return entry_status
     */
    public function setEntryStatus($entry_status)
    {
        $this->entry_status = $entry_status;

        return $this;
    }

    /**
     * Get entry_status
     *
     * @return booelan
     */
    public function getEntryStatus()
    {
        return $this->entry_status;
    }


    /*-------------------------------------------------------*/
    /*              instock
     -------------------------------------------------------- */
    /**
     * Set instock
     *
     * @param integer $instock
     *
     * @return instock
     */
    public function setInstock($instock)
    {
        $this->instock = $instock;

        return $this;
    }

    /**
     * Get instock
     *
     * @return booelan
     */
    public function getInstock()
    {
        return $this->instock;
    }


    /*-----------------------------------------------
                    warning
    -----------------------------------------------*/
    /**
     * Set warning
     *
     * @param array $warning
     *
     * @return Course
     */
    public function setWarning($warning)
    {
        $this->warning = $warning;

        return $this;
    }

    /**
     * Get warning
     *
     * @return array
     */
    public function getWarning()
    {
        return $this->warning;
    }

    /*-----------------------------------------------
                    date
    -----------------------------------------------*/

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Course
     */
    public function setDate()
    {
        $this->date = new \DateTime();
        return $this;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Course
     */
    public function setCustomDate($date)
    {
        $this->date = $date;
        return $this;
    }


    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }


}

