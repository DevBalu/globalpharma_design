<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\hotquestions;

// Catch db extention
use Doctrine\DBAL\DBALException;

class HotQuestionsController extends Controller
{

	//Custom mzf
	public function accessDenied ($role){
		// access denied for anonymous
		if (!$this->get('security.authorization_checker')->isGranted($role)) {
			throw $this->createAccessDeniedException('GET OUT!');
			return $this->redirectToRoute('homepage');
		}
	}

	/**
	* @Route("/hotQuesions", name="hotQuesions")
	*/
	public function hotQuesionsAction()
	{

		$em = $this->getDoctrine()->getManager();

		$hotquestions_repo = $this->getDoctrine()->getRepository(hotquestions::class);
		$hotquestions = $hotquestions_repo->findBy(
			[],
			['id' => "DESC"]);

		return $this->render('@App/hotQuesions/hotQuesions.html.twig', array(
			'hotquestions' => $hotquestions,
		)); 
	}

	/**
	* @Route("/newQuesion", name="newQuesion")
	*/
	public function newQuesionAction(Request $request)
	{
		$this->accessDenied('ROLE_ADMIN');


		$question = $request->request->get('question');
		$answer = $request->request->get('answer');

		if (empty($question ) || empty($answer )) {
			return $this->redirectToRoute('hotQuesions');
		}

		$em = $this->getDoctrine()->getManager();

		try{
			// $hotquestions_repo = $this->getDoctrine()->getRepository(hotquestions::class);
			$hotquestions = new hotquestions();

			// set question
			$hotquestions->setQuestion($question);
			// set answer
			$hotquestions->setAnswer($answer);

			$em->persist($hotquestions);
			$em->flush();

			$this->addFlash(
				'success',
				'Данные были сохранены'
			);
			return $this->redirectToRoute('hotQuesions');
		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('hotQuesions');
		}

		return $this->redirectToRoute('hotQuesions');
	}


	/**
	* @Route("/rmvHotQuestion", name="rmvHotQuestion")
	*/
	public function rmvHotQuestionnAction(Request $request)
	{
		$this->accessDenied('ROLE_ADMIN');

		$entry_id = $request->request->get('entry_id');

		if (empty($entry_id ) ) {
			return $this->redirectToRoute('hotQuesions');
		}

		$em = $this->getDoctrine()->getManager();

		try{
			$hotquestions_repo = $this->getDoctrine()->getRepository(hotquestions::class);
			$hotquestion = $hotquestions_repo->find($entry_id);

			if (!$hotquestion) {
				$this->addFlash(
					'error',
					'Не найденно записи для удаления '
				);
				return $this->redirectToRoute('hotQuesions');
			}

			// remove question
			$em->remove($hotquestion);
			$em->flush();

			$this->addFlash(
				'success',
				'Данные были сохранены'
			);
			return $this->redirectToRoute('hotQuesions');
		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('hotQuesions');
		}

		return $this->redirectToRoute('hotQuesions');
	}

}
