<?php

namespace AppBundle\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BrandsController extends Controller
{
    /**
     * @Route("/allBrands")
     */
    public function get_all_brandsAction()
    {
        return $this->render('AppBundle:Brands:get.all.brands.html.twig', array(
            // ...
        ));

    }

    /**
     * @Route("/sort_by_brand_id")
     */
    public function sort_by_brand_id_Action()
    {
        return $this->render('AppBundle::Brands/sort.by.brand.id..html.twig', array(
            // ...
        ));
    }
}
