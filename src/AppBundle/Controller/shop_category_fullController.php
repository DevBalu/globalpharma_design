<?php

namespace AppBundle\Controller;

// libs
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

// Entity
use AppBundle\Entity\products;
use AppBundle\Entity\Basket;
use AppBundle\Entity\favorites;
use AppBundle\Entity\shopstate;

// sevices
use AppBundle\Service\pChecker;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class shop_category_fullController extends Controller
{
	/**
	* @Route("/shop_category_full" )
	* @Route("/shop_category_full/{catId}/{subcatId}")
	* @Route("/shop_category_full/{catId}/{subcatId}/{manid}")
	*/
	public function shop_category_fullAction(pChecker $pChecker, $catId=null, $subcatId=null, $manid=null)
	{
		/*                   Get current user
		------------------------------------------------------------------------------*/
		if ($this->getUser()) {
			$user_id = $this->getUser()->getId();
		}else {
			$user_id = 0;
		}

		$em = $this->getDoctrine()->getManager();

		$cache = new FilesystemCache();

		/*                   Check at shop state
		------------------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);

		$shopstate =  $shopstate_repo->findOneBy(array(
			'status' => 1
		), []);

		if ($shopstate) {
			// redirect customer to page with message of state
			if ($shopstate->getState() == 'shop_audit' && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
				return $this->redirectToRoute('logout');
			}
		} 

		$products_repo = $this->getDoctrine()->getRepository(products::class);

		if(!is_null($catId) && is_null($subcatId) && is_null($manid)){
			$products = $products_repo->findBy([
				"categoryId" => $catId,
				"entry_status" => 1
			], ['id' => 'DESC']);

		}elseif (!is_null($catId) && !is_null($subcatId) && is_null($manid)){
			$products = $products_repo->findBy([
				"categoryId" => $catId,
				"subcategoryId" => $subcatId,
				"entry_status" => 1
			], ['id' => 'DESC']);

		}elseif(!is_null($manid)) {
			$products = $products_repo->findBy([
				"manufacturerid" => $manid,
				"entry_status" => 1
			], ['id' => 'DESC']);
		}else {
			$products = $products_repo->findBy([
				"entry_status" => 1
			], ['id' => 'DESC']);
		}


		/*                  Check if user has been add product on basket
		------------------------------------------------------------------------------*/
		// Basket
		$Basket_repo = $em->getRepository(Basket::class);
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') ) {
			$products = $pChecker->ifAdded("onbasket", $products, $Basket_repo, $user_id);
		}

		/*                 Check if user has benn add this  producton favorites	------------------------------------------------------------------------------*/
		// favorites
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') ) {
			$favoritess_repo = $em->getRepository(favorites::class);
			$products = $pChecker->ifAdded("favorite", $products, $favoritess_repo, $user_id);
		}

		/*                 Check if product is new on the system
		------------------------------------------------------------------------------*/
		$products = $pChecker->ifNewTwo($products);

		return $this->render('@App/shop_category_full/shop.category.full.html.twig', array(
			"products" => $products
		));
	}

}
