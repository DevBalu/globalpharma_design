<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// Session
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;

use \Datetime;

// Entity
use AppBundle\Entity\Basket;
use AppBundle\Entity\Orderr;
use Phpeshop\ShoppaymentBundle\Entity\Wallet;
use AppBundle\Entity\recipient;
use AppBundle\Entity\products;
use AppBundle\Entity\Course;
use AppBundle\Entity\shopstate;
use AppBundle\Entity\Delivery_cost;
use AppBundle\Entity\Coupon;

// Catch db extention
use Doctrine\DBAL\DBALException;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class OrderControllerController extends Controller
{
	/*-------------------------------------------------------------------------------------------------------------------
								Custom Methods AG
	---------------------------------------------------------------------------------------------------------------------*/

	public function coursesRelevance()
	{
		$em = $this->getDoctrine()->getManager();
		// Popular products
		$products_repo = $em->getRepository(products::class);
		$Course_repo = $em->getRepository(Course::class);

		/*-----------------------------------------------------------------------------
						COURSE RELEVANCE
		------------------------------------------------------------------------------*/
		$products_list = $Course_repo->coursesRelevance();

		// make course instock = 0 if at least one product of course not in stock
		$courses_not_instock = [];
		foreach ($products_list as $key => $product) {

			$prod_ent = $products_repo->findBy([
				'id' => $product['pid'],
				'inStock' => 1
			],[]);

			$prod_ent_rmved = $products_repo->findBy([
				'id' => $product['pid'],
				'entry_status' => 0
			],[]);

			// if at least one product of course is removed notify customer
			if ($prod_ent_rmved) {
				$cours_ent = $Course_repo->find($product['cid']);

				$warning_message = ['product_removed' => 'продукт(ы) нах-ся в курсе был(и) удалён(ы)'];
				$cours_ent->setWarning($warning_message);
				$em->persist($cours_ent);
				$em->flush();
			}

			if (!$prod_ent || $prod_ent_rmved) {
				$courses_not_instock[] = $product['cid'];
			}
		}

		$courses_not_instock = array_unique($courses_not_instock);
		foreach ($courses_not_instock as $key => $courseid) {
			$cours_ent = $Course_repo->find($courseid);
			$cours_ent->setInstock(0);
			$em->persist($cours_ent);
			$em->flush();
		}


		// check if all product of course is in stock change course status "instock" = 1
		$all_courses =  $Course_repo->findBy(['entry_status' => 1, ],[]);

		foreach ($all_courses as $key => $course) {
			$count = 0;

			foreach ($course->getContent() as $key => $cprod) {

				$prod_ent = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'inStock' => 0,
					'entry_status' => 1
				],[]);

				$prod_ent_rmved = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'entry_status' => 0
				],[]);

				if ($prod_ent || $prod_ent_rmved) {
					$count++;
				}
			}

			if ($count == 0) {
				$change_course = $Course_repo->find($course->getId());
				$change_course->setInstock(1);
				$em->persist($change_course);
				$em->flush();
			}
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------
								ACTIONS of ORDER CONTROLLER
	---------------------------------------------------------------------------------------------------------------------*/
	/**
	* @Route("/newOrder/{payStat}", name="newOrder")
	*/
	public function newOrderAction(SessionInterface $session, $payStat)
	{

		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$session = new Session();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();


		/*                   Check at shop state
		------------------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);
		$shopstate =  $shopstate_repo->findOneBy(array(
			'status' => 1
		), []);

		if ($shopstate) {
			// redirect customer to page with message of state
			if ($shopstate->getState() == 'shop_audit' && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
				return $this->redirectToRoute('logout');
			}
		}


		/*------------------------------------------------------------
								PREPARE DATA FOR USING 
		--------------------------------------------------------------*/
		/* ------------------id_user------------- */
		$user = $this->getUser();
		$id_user = $user->getId();

		$Basket_repo = $this->getDoctrine()->getRepository(Basket::class);
		$products_repo = $this->getDoctrine()->getRepository(products::class);

		// get delivery cost list
		$Delivery_cost_repo = $em->getRepository(Delivery_cost::class);
		$Delivery_cost = $Delivery_cost_repo->findBy([], ['id' => 'ASC']);

		// get coupon list
		$Coupon_repo = $em->getRepository(Coupon::class);


		/*------------------------------------------------------------
				 check course relevance
		--------------------------------------------------------------*/
		$this->coursesRelevance();

		$strongFullUserBasket = $Basket_repo->strongFullUserBasket($id_user);

		/*------------------------------------------------------------
			If selected user basket  on step shop_checkout_four
			is equal with actual state of products
		--------------------------------------------------------------*/
		$tmp_into_order = $session->get('tmp_into_order');

		if (!($tmp_into_order == $strongFullUserBasket)) {
			$this->addFlash(
				'error',
				'За время оформления заказа в некоторых из выбранных вами прдуктов произошли изменения. Проверьте товар на наличие.'
			);
			return $this->redirectToRoute('shop_basket');
		}

		$course_prod_quant = [];
		/*---------------------------------------------------
				Step 1
			Check if count of products on COURSE
			added on order
			will be have on stock for save order
		-------------------------------------------------*/
		foreach ($strongFullUserBasket['course'] as $key => $course) {
			$course_count = $course['count'];

			foreach ($course[0]['content'] as $key => $cprod) {
				$prod_ent = $products_repo->find($cprod['prodid']);

				$actual_prod_quantity = $prod_ent->getProductQuantity();
				$course_prod_count = $cprod['count'];

				(int)$order_quant_prod = $course_prod_count * $course_count;

				if ($order_quant_prod > $actual_prod_quantity) {
					$this->addFlash(
						'error',
						'Общее кол-во продуктов указанных в курсе (ах) превышает кол-во продуктов в наличаии.'
					);
					return $this->redirectToRoute('shop_basket');
				}

				$course_prod_quant[$cprod['prodid']] = $order_quant_prod;
			}
		}

		/*---------------------------------------------------
				Step 2
			Check if count of PRODUCTS
			added on order
			will be have on stock for save order
		---------------------------------------------------*/
		foreach ($strongFullUserBasket['products'] as $key => $product) {

			(int)$actual_prod_quantity = $product[0]['product_quantity'];
			(int)$prod_count = $product['count'];

			if ($prod_count > $actual_prod_quantity) {
				$this->addFlash(
					'error',
					'Общее кол-во продуктов указанных "не в курсе (ах)" превышает кол-во продуктов в наличаии'
				);
				return $this->redirectToRoute('shop_basket');
			}

			if (array_key_exists($product['productid'], $course_prod_quant)) {
				$course_prod_quant[$product['productid']] += $prod_count;
			}else {
				$course_prod_quant[$product['productid']] = $prod_count;
			}

		}

		/*---------------------------------------------------
				Step 3
			After we went through step with checker of Course
			and step with checker of Products
			need to check amoun of these

			If their amount not more than product quantity
		---------------------------------------------------*/

		foreach ($course_prod_quant as $prod_id => $prod_quant) {
			$prod_ent = $products_repo->find($prod_id);

			$actual_prod_quantity = $prod_ent->getProductQuantity();

			if ($prod_quant > $actual_prod_quantity) {
				$this->addFlash(
					'error',
					'Общее кол-во продуктов плюс продуктов из курса (ов) превышает кол-ва продукта (ов) в наличае'
				);
				return $this->redirectToRoute('shop_basket');
			}
		}


		// if user access link new Order again , but order already is saved redirect her to basket page
		if (empty($strongFullUserBasket['products']) && empty($strongFullUserBasket['course'])) {
			$this->addFlash(
				'error',
				'Заказ не прошёл проверку на актуальность данных. Перепроверьте наличие продуктов в системе. '
			);
			return $this->redirectToRoute('shop_basket');
		}

		$Order = new Orderr();
		$Order_repo = $this->getDoctrine()->getRepository(Orderr::class);

		/*------------------------------------------------------------
							INSERT ON DB
		--------------------------------------------------------------*/

		try{
			/* ------------------id_user------------------ */
				 $Order->setIdUser($id_user); 

			/* ------------------id_recipient------------- */
			if ($session->get('AddressId')) {
				/* --------------recipient repo --------- */
				$recipient_repo = $this->getDoctrine()->getRepository(recipient::class);

				$id_recipient = $session->get('AddressId');
				// set id_recipient
				$Order->setIdRecipient($id_recipient);

				// set Delivery address
				$recipient_detail = $recipient_repo->find($id_recipient);
				$Order->setDeliveryaddress($recipient_detail);

			} else {
				$this->addFlash(
					'error',
					'Не указан адрес доставки. Пройдите вновь все шаги проверки оформления заказа.'
				);
				return $this->redirectToRoute('shop_checkout_one');
			}

			/* ------------------products-------------------- */
				 $Order->setProducts($strongFullUserBasket);

			/* ------------------delivery_method-------------- */
			if ($session->get('DeliveryMethod')) {
				$delivery_method  = $session->get('DeliveryMethod');
				$Order->setDeliveryMethod($delivery_method);
			}

			/* ------------------delivery_status-------------- */
				 $Order->setDeliveryStatus('');


			/* ------------------final_cost-------------- */
				 $Order->setUniqid( uniqid() );

			/* ------------------final_cost-------------- */
				$sum = $Basket_repo->recalculateSubtotals($id_user);

				// calculate delivery cost in dependence of order final cost of products
				$DC = 0;
				//add to final cost of products delivery cost
				foreach ($Delivery_cost as $key => $value) {
					if ($sum >= (int)$value->getPriceDelimiter()) {
						$DC = $value->getDeliveryCost();
					}
				}

				$final_sum = $DC + $sum;

				/* ------------------------------------------
						Delivery_cost
				--------------------------------------------*/
				 $Order->setDeliveryCost($DC);


				/* ------------------------------------------
						Coupon
				--------------------------------------------*/
				if ($session->get('coupon')) {
					// get inserted coupon from customer
					$coupon_id = $session->get('coupon')->getId();

					/*------------------------------------
						Check coupon at relevance
					-------------------------------------*/
					$Coupon = $Coupon_repo->find($coupon_id);

					// if not found
					if (!$Coupon) {
						$this->addFlash(
							'error',
							'Указанный вами купон не был найден в системе. Введите другой Валидный купон.'
						);
						return $this->redirectToRoute('shop_checkout_four');
					}

					$cd = new Datetime();
					$fromDate = $Coupon->getFromdate();
					$todate = $Coupon->getTodate();

					$available_days = $fromDate->diff($todate);

					$remain_days = $todate->diff($cd)->format('%r%a');

					// in case when coupon is expired
					if ($remain_days > 0 ) {
						$Coupon->setEntryStatus(0);
						$em->persist($Coupon);
						$em->flush();

						$this->addFlash(
							'error',
							'Период активности купона истёк.'
						);
						return $this->redirectToRoute('shop_checkout_four');
					}

					$percent = $Coupon->getPercent();
					if ($percent > 0) {
						$coupon_amount = (int)( ($percent / 100) * $final_sum);
						$final_sum -= $coupon_amount;
						$Order->setCoupon($Coupon);
					}
				}

				$Order->setFinalCost($final_sum);


			/* ------------------payment_method-------------- */
			switch ($payStat) {
				case "after":
					$Order->setPaymentStatus('pending_payment');
					break;
				case "pay":
					$Order->setPaymentStatus('pending_payment');
					break;

				default:
					$this->addFlash(
						'error',
						'MZFK этап заказа прерван '
					);
					 return $this->redirectToRoute('shop_checkout_four');
					break;
			}


			/* ------------------------------------------
					TrackDate
			--------------------------------------------*/
			$Order->setTrackDate();

			/* ------------------ Save order -------------- */
			$em->persist($Order);
			$em->flush();


			/*---------------------------------------------------------------------------------------------------------------
								Clear data after save order
			--------------------------------------------------------------------------------------------------------------- */
			/* ---------------------------------------------------------
				Decrease Products Quantity which is in order
			----------------------------------------------------------- */


			foreach ($strongFullUserBasket['products'] as $key => $product) {

				// get products whcich is used on order
				$needed_products = $products_repo->find($product[0]['id']);

				if ($needed_products) {
					// get current quantity of product
					$current_prod_quantity = $needed_products->getProductQuantity();

					// get count of products which is has be saved on order
					$prod_count_on_order = $product['count'];

					// calculate new quantity of product
					$new_product_quantity = $current_prod_quantity - $prod_count_on_order;
					$needed_products->setProductQuantity($new_product_quantity);

					// update data
					$em->persist($needed_products);
					$em->flush();
				}
			}

			foreach ($strongFullUserBasket['course'] as $key => $course) {

				$course_products = $course[0]['content'];

				foreach ($course_products as $key => $product) {

					$course_needed_product = $products_repo->find($product['prodid']);

					if ($course_needed_product) {
						// get current quantity of product
						$course_current_prod_quantity = $course_needed_product->getProductQuantity();

						// get count of products which is has be saved on order
						$course_prod_count_on_order = $product['count'];

						// calculate new quantity of product
						if ($course['count'] > 1) {
							$course_new_product_quantity = $course_current_prod_quantity - ($course_prod_count_on_order * $course['count']);
						} else {
							$course_new_product_quantity = $course_current_prod_quantity - $course_prod_count_on_order;
						}

						$course_edit_prod = $course_needed_product->setProductQuantity($course_new_product_quantity);
						// update data
						$em->persist($course_edit_prod);
						$em->flush();
					}

				} /*./ End loop of products*/
			} /*./ End loop of course*/


			/* ---------------------------------------------------------
				After create new order remove
				all products from user basket
			----------------------------------------------------------- */
			// remove PRODUCTS & COURSES whcih has been saved on order from basket
			foreach ($strongFullUserBasket as $key => $item) {

				foreach ($item as $key => $prod_on_basket) {
					$prod_on_basket = $Basket_repo->find($prod_on_basket['id']);
					if ($prod_on_basket) {
						$em->remove($prod_on_basket);
						$em->flush();

						$session->set('basket_count', 0);
					}
				}

			}

			$cache->clear();

			/* ----------------------------------
				After order is saved 
				check if payment method is defined
			-------------------------------------- */
			switch ($payStat) {
				case 'after':
					$this->addFlash(
						'warning',
						'Заказ был успешно сохранен. Статус заказа - Ожидает оплаты.'
					);
					return $this->redirectToRoute('customer_orders');
					break;

				case 'pay':
					$this->addFlash(
						'warning',
						'Заказ сохранен. Вы можете оплатить заказ прямо сейчас.'
					);

					return $this->redirectToRoute('shop_checkout_three', ['order_uniqid' => $Order->getUniqid()]);
					break;
			}

		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('homepage');
		}

	}

	/**
	 * @Route("/removeOrder")
	 */
	public function removeOrderAction()
	{
		return $this->render('@App/OrderController/remove_order.html.twig', array(
			// ...
		));
	}

	/**
	 * @Route("/cancelOrder")
	 */
	public function cancelOrderAction()
	{
		return $this->render('@App/OrderController/cancel_order.html.twig', array(
			// ...
		));
	}

}
