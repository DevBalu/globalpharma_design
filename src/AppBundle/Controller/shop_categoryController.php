<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

// Entity
use AppBundle\Entity\products;
use AppBundle\Entity\Basket;
use AppBundle\Entity\category;
use AppBundle\Entity\Manufacturers;
use AppBundle\Entity\shopstate;
// sevices
use AppBundle\Service\pChecker;
// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class shop_categoryController extends Controller
{
	/**
	* @Route("/shop_category", name="shop_category")
	*/
	public function shop_categoryAction(pChecker $pChecker)
	{
		$em = $this->getDoctrine()->getManager();

		$cache = new FilesystemCache();

		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') ){
			$user = $this->getUser();
			$user_id = $user->getId();
		}else{
			$user_id = 0;
		}

		/*----------------------------------------------------------------------
							SHOPSTATE
		----------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);

		if (!$cache->get('shop_category.shopstate')) {
			$shopstate =  $shopstate_repo->findOneBy(array(
				'status' => 1
			), []);

			if ($shopstate) {
				$cache->set('shop_category.shopstate', $shopstate);
			}
		}else {
			$shopstate = $cache->get('shop_category.shopstate');
		}

		if ($shopstate) {
			// redirect customer to page with message of state
			if ($shopstate->getState() == 'shop_audit' && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
				return $this->redirectToRoute('logout');
			}
		}

		/*----------------------------------------------------------------------
							products
		----------------------------------------------------------------------*/
		// section which work with repository
		$products_repo = $this->getDoctrine()->getRepository(products::class);
		// if (!$cache->get('shop_category.products')) {
			$products = $products_repo->fullRel();

			// Basket
			$Basket_repo = $this->getDoctrine()->getRepository(Basket::class);
			$products = $pChecker->ifAddedTwo("onbasket", $products, $Basket_repo, $user_id); 

		// 	if ($products) {
		// 		$cache->set('shop_category.products', $products);
		// 	}

		// }else {
		// 	$products =$cache->get('shop_category.products');
		// }


		// Category
		$category_repo = $this->getDoctrine()->getRepository(category::class);
		if (!$cache->get('shop_category.category')) {
			$category = $category_repo->findBy(['entry_status' => 1], []);
			if ($category) {
				$cache->set('shop_category.category', $category);
			}
		}else {
			$category =$cache->get('shop_category.category');
		}

		// Manufacturers
		$Manufacturers_repo = $this->getDoctrine()->getRepository(Manufacturers::class);
		if (!$cache->get('start.Manufacturers')) {
			$Manufacturers = $Manufacturers_repo->findBy(['entry_status' => 1], []);
			if ($Manufacturers) {
				$cache->set('start.Manufacturers', $Manufacturers);
			}
		}else {
			$Manufacturers =$cache->get('start.Manufacturers');
		}

		return $this->render('@App/shop_category/shop.category.html.twig', array(
			"products" => $products,
			"Manufacturers" => $Manufacturers,
			"category" => $category,
		));
	}
}
