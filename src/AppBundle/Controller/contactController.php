<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;
// Catch db extention
use Doctrine\DBAL\DBALException;
// Entity
use AppBundle\Entity\Contact;
use AppBundle\Entity\toSupport;

// service
use ControlpanelBundle\Service\Mailer;
// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;


class contactController extends Controller
{

	/*............................................................................................................................
									DISPLAY / EDIT contact INFO
	............................................................................................................................*/
	/**
	* @Route("/contact", name="contact")
	*/
	public function contactAction()
	{
		$em = $this->getDoctrine()->getManager();
		$Contact_repo = $this->getDoctrine()->getRepository(Contact::class);
		$Contact = $Contact_repo->findOneBy(
			[] , ['id' => 'DESC'], 1
		);

		if (empty($Contact)) {
			return $this->redirectToRoute('firstContactInsert');
		}else {
			$contact_info = $Contact->info;

			return $this->render('@App/contact/contact.html.twig', array(
				'Contact' => $contact_info
			));
		}
	}

	/**
	* @Route("/setContact", name="setContact")
	*/
	public function setContactAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			$this->addFlash(
				'error',
				'.|.'
			);
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$Contact_repo = $this->getDoctrine()->getRepository(Contact::class);

		$cache = new FilesystemCache();

		$Contact = $Contact_repo->findOneBy(
			[] , ['id' => 'DESC'], 1
		);

		$heading_intro = $request->request->get('heading_intro');
		$heading_value = $request->request->get('heading_value'); 

		$call_center_title = $request->request->get('call_center_title');
		$call_center_intro = $request->request->get('call_center_intro'); 
		$call_center_value = $request->request->get('call_center_value');

		// prepapre mail of support
		$mail_title = $request->request->get('mail_title');
		$mail_intro = $request->request->get('mail_intro');

		$to_support_intro = $request->request->get('to_support_intro');

		$mail_value = str_replace(' ', '', $request->request->get('mail_value') );
		if(!filter_var($mail_value, FILTER_VALIDATE_EMAIL)){
			$this->addFlash(
				'error',
				'Поле "ЭЛЕКТРОННАЯ ПОДДЕРЖКА" является обьязателным. Убедитесь
				в корректности заполнения поля. В ином случае сообщение клиента
				 не будет доставлено'
			);
			return $this->redirectToRoute('contact');
		}

		// prepare password for support
		$mail_pass = $request->request->get('mail_pass');
		if (empty($mail_pass)) {
			$this->addFlash(
				'error',
				'Поле "ЭЛЕКТРОННАЯ ПОДДЕРЖКА" является обьязателным. Убедитесь
				в корректности заполнения поля. В ином случае сообщение клиента
				 не будет доставлено'
			);
			return $this->redirectToRoute('contact');
		}

		$contact_info = [
			'heading_intro' => $heading_intro,
			'heading_value' => $heading_value,

			'call_center_title' => $call_center_title,
			'call_center_intro' => $call_center_intro,
			'call_center_value' => $call_center_value,

			'mail_title' => $mail_title,
			'mail_intro' => $mail_intro,
			'mail_value' => $mail_value,
			'mail_pass' => $mail_pass,

			'to_support_intro' => $to_support_intro
		];

		try{
			if (empty($Contact)) {
				$this->addFlash(
					'error',
					'Не найденно записей для едитирования'
				);
				return $this->redirectToRoute('contact');
			}else {
				$Contact->setInfo($contact_info);
				$em->persist($Contact);
				$em->flush();

				if ($cache->clear()) {

					$Contact_edited = $Contact_repo->findOneBy(
						[] , ['id' => 'DESC'], 1
					);
					if ($Contact_edited) {
						$cache->set('start.Contact', $Contact_edited);
					}
				}

			}
			return $this->redirectToRoute('contact');
		}
		catch(DBALException $e){
			return $this->redirectToRoute('contact');
		}

	}

	/**
	* @Route("/firstContactInsert", name="firstContactInsert")
	*/
	public function firstContactInsertAction()
	{
		// if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
		// 	return new JsonResponse( ['////']);
		// }

		$em = $this->getDoctrine()->getManager();
		$Contact = new Contact();

		$first_insert = [
			'heading_intro' => 'МЫ ЗДЕСЬ ЧТОБЫ ПОМОЧЬ ВАМ',
			'heading_value' => 'Вам что-то интересно? У вас есть какие-то проблемы с нашими продуктами?Lorem Ipsum - это текст-"рыба",
				часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с
				 начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя
				 Lorem Ipsum для распечатки образцов.',

			'call_center_title' => 'колл-центр',
			'call_center_intro' => 'Этот номер является бесплатным, если вы звоните с территории России.',
			'call_center_value' => '+33 555 444 333',

			'mail_title' => ' Электронная поддержка',
			'mail_intro' => 'Пожалуйста, не стесняйтесь писать нам электронные письма или использовать нашу систему электронной поддержки.',
			'mail_value' => 'info@fakeemail.com',
			'mail_pass' => 'vasea',

			'to_support_intro' => 'Написать администрации'
		];

		try{
			$Contact->setInfo($first_insert);
			$em->persist($Contact);
			$em->flush();
		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
		};
		return $this->redirectToRoute('contact');
	}

	/*............................................................................................................................
									SENT MESSAGE TO SUPPORT
	............................................................................................................................*/
	/**
	* @Route("/toSupport", name="toSupport")
	*/
	public function toSupportAction(Request $request, Mailer $mailer)
	{
		$em = $this->getDoctrine()->getManager();
		$toSupport = new toSupport();

		/* ..................................
				Prepare data for usage
		.....................................*/

		// Get firstname
		$firstname = $request->request->get('firstname');
		// Get surname
		$surname = $request->request->get('surname');

		// Get mail
		$mail = $request->request->get('mail');

		if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
			$this->addFlash(
				'error',
				'Поле "Эл.почта" является обьязателным. Убедитесь в корректности заполнения поля'
			);
			return $this->redirectToRoute('contact');
		}

		// Get title
		$title = $request->request->get('title');
		if (empty($title) && sizeOf($title) < 100) {
			$this->addFlash(
				'error',
				'Поле обязательно для заполнения'
			);
			return $this->redirectToRoute('contact');
		}

		// Get message
		$message = $request->request->get('message');
		if (empty($message) && sizeOf($title) < 1000) {
			$this->addFlash(
				'error',
				'Поле Сообщение не было заполнено'
			);
			return $this->redirectToRoute('contact');
		}

		if ( $this->getUser() ) {
			$userid = $this->getUser()->getId();
		} else {
			$userid = 0;
		}

		/* ..................................
			Call mail service for 
			sent message to support gmail
		.....................................*/
		$files = $request->files->get('files');
		$contact_repo = $em->getRepository(Contact::class);

		$contact_prof = $contact_repo->findOneBy([], ['id' => "DESC"], 1)->info;

		/*
			1 - from
			2 - to
			3 - subject
			4 - body
			5 - files
			5 - contact_prof
		*/

		$from = $mail;
		$to = [$contact_prof['mail_value'] ];
		$subject = 'To support:' . $firstname . ' ' . $surname;

		// prepare body of message
		if ($userid !== 0) {
			$user_mail_id = 'User id: ' . $userid . ' <br>';
		}else {
			$user_mail_id = '';
		}

		$body = 'Пользователь:' . $firstname . ' ' . $surname . '<br><br>'
			. $user_mail_id . '<br>'
			. 'Эл.почта: ' . $mail . '<br><br>'
			. 'Сообщение: ' . $message
		;

		$files = $request->files->get('files');


		$send_mail = $mailer->sendMail($from, $to, $subject, $body, $files, $contact_prof);

		if ($send_mail == 'success') {
			$this->addFlash(
				'success',
				'Сообщение было успешно отправлено'
			);
			return $this->redirectToRoute('contact');
		}

		/* ..................................
				INSERT ON DB
		.....................................*/
		try{
			// firstname
			$toSupport->setFirstname($firstname);
			// surname
			$toSupport->setSurname($surname);
			// mail
			$toSupport->setMail($mail);
			// title
			$toSupport->setTitle($title);
			// message
			$toSupport->setMessage($message);
			// user id
			$toSupport->setUserid($userid);

			$em = $this->getDoctrine()->getManager();
			$em->persist($toSupport);
			$em->flush();

			$this->addFlash(
				'warning',
				'Сообщение было сохранено и будет отправлено после разрешения системных ошибок'
			);
			return $this->redirectToRoute('contact');
		}
		catch(DBALException $e){
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('contact');
		}

	}



}
