<?php

namespace AppBundle\Controller;
// libs
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;

// Entitys
use AppBundle\Entity\products;
use AppBundle\Entity\Course;
use AppBundle\Entity\reviews;
use AppBundle\Entity\home_carousel;
use AppBundle\Entity\Basket;
use AppBundle\Entity\favorites;
use AppBundle\Entity\category;
use AppBundle\Entity\Contact;
use AppBundle\Entity\shopstate;
use AppBundle\Entity\Manufacturers;

// sevices
use AppBundle\Service\pChecker;

// Cahce data
use Symfony\Component\Cache\Simple\FilesystemCache;

class startController extends Controller
{ 
	/*-------------------------------------------------------------------------------------------------------------------
								Custom Methods AG
	---------------------------------------------------------------------------------------------------------------------*/

	public function coursesRelevance()
	{
		$em = $this->getDoctrine()->getManager();
		// Popular products
		$products_repo = $em->getRepository(products::class);
		$Course_repo = $em->getRepository(Course::class);

		/*-----------------------------------------------------------------------------
						COURSE RELEVANCE
		------------------------------------------------------------------------------*/
		$products_list = $Course_repo->coursesRelevance();

		// make course instock = 0 if at least one product of course not in stock
		$courses_not_instock = [];
		foreach ($products_list as $key => $product) {

			$prod_ent = $products_repo->findBy([
				'id' => $product['pid'],
				'inStock' => 1
			],[]);

			$prod_ent_rmved = $products_repo->findBy([
				'id' => $product['pid'],
				'entry_status' => 0
			],[]);

			// if at least one product of course is removed notify customer
			if ($prod_ent_rmved) {
				$cours_ent = $Course_repo->find($product['cid']);

				$warning_message = ['product_removed' => 'продукт(ы) нах-ся в курсе был(и) удалён(ы)'];
				$cours_ent->setWarning($warning_message);
				$em->persist($cours_ent);
				$em->flush();
			}

			if (!$prod_ent || $prod_ent_rmved) {
				$courses_not_instock[] = $product['cid'];
			}
		}

		$courses_not_instock = array_unique($courses_not_instock);
		foreach ($courses_not_instock as $key => $courseid) {
			$cours_ent = $Course_repo->find($courseid);
			$cours_ent->setInstock(0);
			$em->persist($cours_ent);
			$em->flush();
		}


		// check if all product of course is in stock change course status "instock" = 1
		$all_courses =  $Course_repo->findBy(['entry_status' => 1, ],[]);

		foreach ($all_courses as $key => $course) {
			$count = 0;

			foreach ($course->getContent() as $key => $cprod) {

				$prod_ent = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'inStock' => 0,
					'entry_status' => 1
				],[]);

				$prod_ent_rmved = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'entry_status' => 0
				],[]);

				if ($prod_ent || $prod_ent_rmved) {
					$count++;
				}
			}

			if ($count == 0) {
				$change_course = $Course_repo->find($course->getId());
				$change_course->setInstock(1);
				$em->persist($change_course);
				$em->flush();
			}
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------
								ACTIONS of START CONTROLLER
	---------------------------------------------------------------------------------------------------------------------*/
	/**
	* @Route("/", name="homepage")
	*/
	public function startAction(pChecker $pChecker)
	{

		/*                   Work with session
		------------------------------------------------------------------------------*/
		$session = new Session();

		/*                   Work with cahce
		------------------------------------------------------------------------------*/
		$cache = new FilesystemCache();

		// $cache->clear();

		/*                  get repo-es from db
		------------------------------------------------------------------------------*/
		$em = $this->getDoctrine()->getManager();

		/*                   Check at shop state
		------------------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);

		if (!$cache->get('start.shopstate')) {
			$shopstate =  $shopstate_repo->findOneBy(array(
				'status' => 1
			), []);

			if ($shopstate) {
				$cache->set('start.shopstate', $shopstate);
			}
		}else{
			$shopstate = $cache->get('start.shopstate');
		}


		if ($shopstate) {
			// redirect customer to page with message of state
			if ($shopstate->getState() == 'shop_audit' && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
				return $this->redirectToRoute('statemessage');
			}

			$session->set('shopstate',  $shopstate->getState());
		}

		if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('logout');
		}

		if ($this->get('security.authorization_checker')->isGranted('ROLE_COURIER') && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
			return $this->redirectToRoute('logout');
		}

		/*                   Get current user
		------------------------------------------------------------------------------*/
		if ($this->getUser()) {
			$user_id = $this->getUser()->getId();
		} else {
			$user_id = 0;
		}

		// Popular products
		$products_repo = $em->getRepository(products::class);

		/*------------------check if products already not saved on cache---------------------------*/

		// if (!$cache->get('start.popular_products')) {
			$popular_products =  $products_repo->findBy(array(
				'entry_status' => 1,
				'rating' => 1
			), array('id' => 'DESC'));

		// 	if ($popular_products) {
		// 		$cache->set('start.popular_products', $popular_products);
		// 	}
		// }else {
		// 	$popular_products = $cache->get('start.popular_products');
		// }
		/*--------------------------------------------------------------------------------------*/

		// reviews
		$reviews_repo = $em->getRepository(reviews::class);
		if (!$cache->get('start.reviews')) {
			$reviews_comments = $reviews_repo->findBy(array('status' => 'confirmed', 'entry_status' => 1), array('id' => 'DESC'));

			if ($reviews_comments) {
				$cache->set('start.reviews', $reviews_comments);
			}
		}else {
			$reviews_comments = $cache->get('start.reviews');
		}


		// category
		$category_repo = $em->getRepository(category::class);
		if (!$cache->get('start.category')) {
			$category = $category_repo->findBy(array('entry_status' => 1), array('name'=>'ASC'));

			if ($category) {
				$cache->set('start.category', $category);
			}
		}else {
			$category = $cache->get('start.category');
		}

		// set on session category 
		$catRell = $category_repo->catRell();

		/*At moment actual not usage
			this service was calculate nr of products attachment
			 by cat and subcat
		*/

		/*------------------------------------------------------------------------------*/
		// $catRell = $pChecker->catProdCount($catRell, $products_repo);
		/*------------------------------------------------------------------------------*/

		$session->set('catRell', $catRell);
		// home_carousel
		$home_carousel_repo = $em->getRepository(home_carousel::class);
		if (!$cache->get('start.home_carousel')) {
			$home_carousel = $home_carousel_repo->findBy(array('entry_status' => 1), array('id' => 'DESC'));

			if ($home_carousel) {
				$cache->set('start.home_carousel', $home_carousel);
			}
		}else{
			$home_carousel = $cache->get('start.home_carousel');
		}


		/*                  Check if user has been add product on basket
		------------------------------------------------------------------------------*/
		// Basket
		$Basket_repo = $em->getRepository(Basket::class);
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') ) {
			$popular_products = $pChecker->ifAdded("onbasket", $popular_products, $Basket_repo, $user_id); 

			// get count of element on basket
			// $basket_count = $pChecker->entries_count($popular_products, $Basket_repo, $user_id);
			$basket_count = $Basket_repo->findBy(['idUser' => $user_id], []);

			// set count of element on session
			$session->set('basket_count', sizeOf($basket_count) );
		}

		/*                 Check if user has benn add this  producton favorites	--------*/
		// favorites
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') ) {
			$favoritess_repo = $em->getRepository(favorites::class);
			$popular_products = $pChecker->ifAdded("favorite", $popular_products, $favoritess_repo, $user_id);
		}

		/*                 Check if product is new on the system
		------------------------------------------------------------------------------*/
		$popular_products = $pChecker->ifNewTwo($popular_products);

		/*                 Contact info
		------------------------------------------------------------------------------*/
		$Contact_repo = $em->getRepository(Contact::class);
		if (!$cache->get('start.Contact')) {
			$Contact = $Contact_repo->findOneBy(
				[] , ['id' => 'DESC'], 1
			);
			if ($Contact) {
				$cache->set('start.Contact', $Contact);
			}
		}else {
			$Contact = $cache->get('start.Contact');
		}

		$session->set('Contact', $Contact);

		/*                 Courses
		------------------------------------------------------------------------------*/
		$Course_repo = $em->getRepository(Course::class);

		// Check relevance of courses
		/*------------------------------------------------------------------------------*/
		$this->coursesRelevance();
		/*------------------------------------------------------------------------------*/

		// Get courser which is in stock
		$Course_in_stock = $Course_repo->findBy(array('entry_status' => 1, 'instock' => 1), array('id' => 'DESC'));
		// check if course is added on basket
		$Course_in_stock = $pChecker->ifCourseAdded("onbasket", $Course_in_stock, $Basket_repo, $user_id);

		// manufacturers
		$Manufacturers_repo = $em->getRepository(Manufacturers::class);

		if (!$cache->get('start.Manufacturers')) {
			$Manufacturers = $Manufacturers_repo->findBy(array('entry_status' => 1), array('id' => 'DESC'));
			if ($Manufacturers) {
				$cache->set('start.Manufacturers', $Manufacturers);
			}
		}else {
			$Manufacturers = $cache->get('start.Manufacturers');
		}


		return $this->render('@Client/start/start.html.twig', array(
			"popular_products" => $popular_products,
			"reviews_comments" => $reviews_comments,
			"home_carousel" => $home_carousel,
			"Manufacturers" => $Manufacturers,
			"course" => $Course_in_stock,
		));

	}

}
