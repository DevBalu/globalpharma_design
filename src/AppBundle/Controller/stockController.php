<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  
// Entity
use AppBundle\Entity\products;
use AppBundle\Entity\Basket;
use AppBundle\Service\pChecker;
use AppBundle\Entity\favorites;
use AppBundle\Entity\shopstate;

class stockController extends Controller
{
	/**
	* @Route("/stock")
	*/
	public function stockAction(pChecker $pChecker)
	{
		$em = $this->getDoctrine()->getManager();

		/*                   Check at shop state
		------------------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);
		$shopstate =  $shopstate_repo->findOneBy(array(
			'status' => 1
		), []);

		if ($shopstate) {
			// redirect customer to page with message of state
			if ($shopstate->getState() == 'shop_audit' && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
				return $this->redirectToRoute('logout');
			}
		}

		/*                   Get current user
		------------------------------------------------------------------------------*/
		if ($this->getUser()) {
			$user_id = $this->getUser()->getId();
		}else {
			$user_id = 0;
		}

		$products_repo = $this->getDoctrine()->getRepository(products::class);
		$products = $products_repo->findBy([
			'entry_status' => 1
		], []);

		/*                  Check if user has been add product on basket
		------------------------------------------------------------------------------*/
		// Basket
		$Basket_repo = $em->getRepository(Basket::class);
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') ) {
			$products = $pChecker->ifAdded("onbasket", $products, $Basket_repo, $user_id);
		}

		/*                 Check if user has benn add this  producton favorites	------------------------------------------------------------------------------*/
		// favorites
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') ) {
			$favoritess_repo = $em->getRepository(favorites::class);
			$products = $pChecker->ifAdded("favorite", $products, $favoritess_repo, $user_id);
		}
		/*                 Check if product is new on the system
		------------------------------------------------------------------------------*/
		$products = $pChecker->ifNewTwo($products);

		return $this->render('@App/stock/stock.html.twig', array(
			"products" => $products
		));
	}

}
