<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;

// Entity
use AppBundle\Entity\Basket;
use AppBundle\Entity\favorites;
use AppBundle\Entity\Course;
use AppBundle\Entity\products;
use AppBundle\Entity\shopstate;
use AppBundle\Entity\Delivery_cost; 

// sevices
use AppBundle\Service\pChecker; 

// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
// Catch db extention
use Doctrine\DBAL\DBALException;

class shop_basketController extends Controller
{
	/*-------------------------------------------------------------------------------------------------------------------
								Custom Methods AG
	---------------------------------------------------------------------------------------------------------------------*/

	public function coursesRelevance()
	{
		$em = $this->getDoctrine()->getManager();
		// Popular products
		$products_repo = $em->getRepository(products::class);
		$Course_repo = $em->getRepository(Course::class);

		/*-----------------------------------------------------------------------------
						COURSE RELEVANCE
		------------------------------------------------------------------------------*/
		$products_list = $Course_repo->coursesRelevance();

		// make course instock = 0 if at least one product of course not in stock
		$courses_not_instock = [];
		foreach ($products_list as $key => $product) {

			$prod_ent = $products_repo->findBy([
				'id' => $product['pid'],
				'inStock' => 1
			],[]);

			$prod_ent_rmved = $products_repo->findBy([
				'id' => $product['pid'],
				'entry_status' => 0
			],[]);

			// if at least one product of course is removed notify customer
			if ($prod_ent_rmved) {
				$cours_ent = $Course_repo->find($product['cid']);

				$warning_message = ['product_removed' => 'продукт(ы) нах-ся в курсе был(и) удалён(ы)'];
				$cours_ent->setWarning($warning_message);
				$em->persist($cours_ent);
				$em->flush();
			}

			if (!$prod_ent || $prod_ent_rmved) {
				$courses_not_instock[] = $product['cid'];
			}
		}

		$courses_not_instock = array_unique($courses_not_instock);
		foreach ($courses_not_instock as $key => $courseid) {
			$cours_ent = $Course_repo->find($courseid);
			$cours_ent->setInstock(0);
			$em->persist($cours_ent);
			$em->flush();
		}

		// check if all product of course is in stock change course status "instock" = 1
		$all_courses =  $Course_repo->findBy(['entry_status' => 1, ],[]);

		foreach ($all_courses as $key => $course) {
			$count = 0;

			foreach ($course->getContent() as $key => $cprod) {

				$prod_ent = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'inStock' => 0,
					'entry_status' => 1
				],[]);

				$prod_ent_rmved = $products_repo->findBy([
					'id' => $cprod['prodid'],
					'entry_status' => 0
				],[]);

				if ($prod_ent || $prod_ent_rmved) {
					$count++;
				}
			}

			if ($count == 0) {
				$change_course = $Course_repo->find($course->getId());
				$change_course->setInstock(1);
				$em->persist($change_course);
				$em->flush();
			}
		}
	}

	/**
	 * @Route("/shop_basket",  name="shop_basket")
	 */
	public function shop_basketAction(pChecker $pChecker)
	{
		$em = $this->getDoctrine()->getManager();

		/*                   Check at shop state
		------------------------------------------------------------------------------*/
		$shopstate_repo = $em->getRepository(shopstate::class);
		$shopstate =  $shopstate_repo->findOneBy(array(
			'status' => 1
		), []);

		if ($shopstate) {
			// redirect customer to page with message of state
			if ($shopstate->getState() == 'shop_audit' && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
				return $this->redirectToRoute('logout');
			}
		}

		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			 return $this->redirectToRoute('homepage');
		}

		// get current authorized user 
		$session = new Session();

		$user = $this->getUser();
		$id_user = $user->getId();

		$Basket_repo = $em->getRepository(Basket::class);

		// get all products & courses of user from basket
		$Course_repo = $em->getRepository(Course::class);
		// Check relevance of courses
		$this->coursesRelevance();
		/*if course is added on bascket but at the moment is not in stock
		make her innactive and eliment her setted count from basket*/
		$Basket_repo->emptyCountCourse($id_user);

		$fullUserBasket = $Basket_repo->fullUserBasket($id_user);

		/*if product is added on bascket but at the moment is not in stock
		make her innactive and eliment her setted count from basket*/
		$Basket_repo->emptyCountProd($id_user);

		$favorites_repo = $this->getDoctrine()->getRepository(favorites::class);
		$user_favorites = $favorites_repo->userfavorites($id_user);
		$user_favorites = $pChecker->ifNewOne($user_favorites);

		$basket_subtotals = $Basket_repo->recalculateSubtotals($id_user);
		$session->set('subtotals', $basket_subtotals);

		// get delivery cost list
		$Delivery_cost_repo = $em->getRepository(Delivery_cost::class);
		$Delivery_cost = $Delivery_cost_repo->findBy([], ['id' => 'ASC']);

		// insert delivery cost list on session for prevent more requests to db
		(!$Delivery_cost)? $session->set('Delivery_cost', []) : $session->set('Delivery_cost', $Delivery_cost); 

		return $this->render('@App/shop_basket/shop.basket.html.twig', array(
			'fullUserBasket' => $fullUserBasket,
			'user_favorites' => $user_favorites,
			'basket_subtotals' => $basket_subtotals
		));

	}

	/**
	 * @Route("/updateBasket", name="updateBasket")
	 */
	public function updateBasketAction(Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('redirectrole');
		}

		$em = $this->getDoctrine()->getManager();

		$entry_id = $request->request->get('entry_id');
		$new_count = $request->request->get('count');
		$target = $request->request->get('target');

		/*                   Get post value
		------------------------------------------------------------------------------*/
		if (empty($entry_id) || empty($new_count)) {
			$arrData = [
				'type' => 'error',
				'output' => 'Параметры не заданы, свяжитесь с администрацией.'
			];
			return new JsonResponse($arrData);
		}

		$user = $this->getUser();
		$id_user = $user->getId();

		/* Basket repo */
		$Basket_repo = $this->getDoctrine()->getRepository(Basket::class);
		$basket = $Basket_repo->find($entry_id);

		if (!$basket) {
			$arrData = [
				'type' => 'error',
				'output' => 'Обновите страницу для достоверности данных.'
			];
			return new JsonResponse($arrData);
		}

		$Course_repo = $em->getRepository(Course::class);
		$products_repo = $em->getRepository(products::class);

		$strongFullUserBasket = $Basket_repo->strongFullUserBasket($id_user);

		/*-----------------------------------------------------------------------------
					AVAILABLE QUANTITY of PRODUCTS ON BASKET
		------------------------------------------------------------------------------*/

		// get available quantity of products

		/**************************/
		$prod_quant_avail = [];
		/**************************/

		$prod_entrys = $products_repo->findBy([
			'entry_status' => 1
		], []);

		foreach ($prod_entrys as $key => $value) {
			$prod_quant_avail[$value->getId()] = $value->getProductQuantity();
		}

		/**************************/
		$products_on_basket = [];
		/**************************/

		// prepare arr with all single PRODUCTS, not in course, added on basket actual
		foreach ($strongFullUserBasket['products'] as $key => $value) {

			if ($basket->getProductid() == $value[0]['id']) {
				$products_on_basket[$value[0]['id']] = $new_count;
			} else {
				$products_on_basket[$value[0]['id']] = (int)$value['count'];
			}
		}

		// prepare arr with all products IN COURSE added on basket actual
		foreach ($strongFullUserBasket['course'] as $key => $course) {

			if ($basket->getCourseid() == $course['courseid']) {
				$course_count = $new_count;
			} else {
				$course_count = (int)$course['count'];
			}

			foreach ($course[0]['content'] as $key => $cprod) {
				$cprod_id  = $cprod['prodid'];
				$cprod_count = $cprod['count'];

				if (array_key_exists($cprod_id, $products_on_basket)) {
					$products_on_basket[$cprod_id] += $cprod_count * $course_count;

				}else {
					$products_on_basket[$cprod_id] = $cprod_count * $course_count;
				}

			}
		}

		/* ---------------------------------------------------------------
			Check if new count selected is accesible for inserting
		----------------------------------------------------------------*/
		foreach ($products_on_basket as $prodId => $value) {
			if (array_key_exists($prodId, $prod_quant_avail)) {
				$prod_avail = $prod_quant_avail[$prodId];
				$new_prod_quant = $products_on_basket[$prodId];

				if ($new_prod_quant > $prod_avail) {
					$arrData = [
						'type' => 'error',
						'output' => 'Общее кол-во продуктов в корзине превышает кол-во продуктов в наличии'
					];
					return new JsonResponse($arrData);
				}

			}else {
				// if selected prod for changing is not in sistem (removed)
				$arrData = [
					'type' => 'error',
					'output' => 'Продукт (ы) не был (и) найдены в системе. Обратитесь к Администрации.'
				];
				return new JsonResponse($arrData);
			}
		}


		try{

			/*                   Update entry on Basket
			------------------------------------------------------------------------------*/
			$basket->setCount($new_count);
			$em->persist($basket);
			$em->flush();

			/*                   Work with session
			------------------------------------------------------------------------------*/
			$session = new Session();

			/* After update entry set session value */
			$basket_subtotals = $Basket_repo->recalculateSubtotals($id_user);
			$session->set('subtotals', $basket_subtotals);


			/*                   return success result
			------------------------------------------------------------------------------*/
			$arrData = [
				'type' => 'success',
				'output' => 'Корзина успешно обновлена.'
			];

			return new JsonResponse($arrData);
		}
		catch(DBALException $e){
			if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
				$arrData = [
					'type' => 'error',
					'output' => $e->getMessage()
				];

			} else {
				$arrData = [
					'type' => 'error',
					'output' => 'Не удалось выполнить операцию , обратитесь к администрации.'
				];
			}
			return new JsonResponse($arrData);
		}
	}


// print '<pre>';
// print_r($products_on_basket[$prodId]);
// print '</pre>';
// die;

	/**
	 * @Route("/toBasket", name="toBasket")
	 */
	public function toBasketAction(Request $request)
	{

		if($request->request->get('pid') || $request->request->get('courseid')) {
			/*                   Work with session
			------------------------------------------------------------------------------*/
			$session = new Session();

			$pid = $request->request->get('pid');
			$courseid = $request->request->get('courseid');

			// get doctrine
			$em = $this->getDoctrine()->getManager();
			// get Basket entity
			$Basket = new Basket();

			// get current id user if he is auth-ed
			if ( $this->getUser() ) {
				$user_id = $this->getUser()->getId();
			} else {
				$user_id = 0;
				// make functionality for add product on basker without id user. Make any session or any. After make it.
			}

			try {
				// INSERT DATA TO DB
				// set id user
				$Basket->setIdUser($user_id);

				// set id of product and her count
				$Basket->setProductid($pid);

				// set id of product and her count
				$Basket->setCourseid($courseid);

				// set count
				if ($request->request->get('count')) {
					$count = $request->request->get('count');
				}else {
					$count = 1;
				}
				$Basket->setCount($count);

				$em->persist($Basket);
				$em->flush();

				// current_basket_count
				$cbc = $session->get('basket_count');

				// set basket count
				$set_bc = $session->set('basket_count', $cbc + 1);

				// get new basket count
				$nbc = $session->get('basket_count');

				//make something curious, get some unbelieveable data
				$arrData = [
					'type' => 'success',
					'output' => 'Товар добавлен в вашу корзину!',
					'nbc' => $nbc
				];
				return new JsonResponse($arrData);

			}
			catch(DBALException $e){
				if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
					$arrData = [
						'type' => 'error',
						'output' => $e->getMessage()
					];

				} else {
					$arrData = [
						'type' => 'error',
						'output' => 'Не удалось выполнить операцию , обратитесь к администрации.'
					];
				}
				return new JsonResponse($arrData);
			}

		} else {
			// if not found product id
			$arrData = [
				'type' => 'error',
				'output' => 'Не были заданы параметры поиска.',
			];
			return new JsonResponse($arrData);
		}
	}

	/**
	 * @Route("/basket_rm_item", name="basket_rm_item")
	 */
	public function shop_basket_rm_itemAction(Request $request)
	{
		// auth checker
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			// if received request have product id
			if($request->request->get('eid')){

				/*                   Work with session
				------------------------------------------------------------------------------*/
				$session = new Session();

				/* get doctrine */
				$em = $this->getDoctrine()->getManager();
				$user_id = $this->getUser()->getId();
				$entry_id= $request->request->get('eid');

				/* products */
				$Basket_repo = $em->getRepository(Basket::class);
				$prod_on_basket = $Basket_repo->findOneBy(
					[
						'id' => $entry_id,
						'idUser' => $user_id,
					] , []
				);

				// if the result is found
				if ($prod_on_basket) {
					$em->remove($prod_on_basket);
					$em->flush();

					$basket_subtotals = $Basket_repo->recalculateSubtotals($user_id);
					$session->set('subtotals', $basket_subtotals);

					// current_basket_count
					$cbc = $session->get('basket_count');

					// set basket count
					$set_bc = $session->set('basket_count', $cbc - 1);

					// get new basket count
					$nbc = $session->get('basket_count');

					/* return success result */
					$arrData = [
						'type' => 'success',
						'output' => 'Товар был успешно удалён.',
						'nbc' => $nbc
					];
					return new JsonResponse($arrData);
				}
				// if the result is not found
				 else {
					$arrData = [
						'type' => 'error',
						'output' => 'Обновите страницу для достоверности данных.'
					];
					return new JsonResponse($arrData);
				}
			}

		}else {
			$arrData = [
				'type' => 'error',
				'output' => 'Не были заданы параметры поиска.'
			];

			return new JsonResponse($arrData);

		}// END auth checker

	}

}
