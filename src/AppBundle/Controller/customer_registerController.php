<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

// Entity
use AppBundle\Entity\Contact;
use AppBundle\Entity\User; 

/*for encode password*/
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

// Catch db extention
use Doctrine\DBAL\DBALException;


// service
use ControlpanelBundle\Service\Mailer;

class customer_registerController extends Controller
{
	/**
	* @Route("/customer_register", name="customer_register")
	*/
	public function customer_registerAction()
	{
		if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
			return $this->redirectToRoute('homepage');
		} else {
			return $this->render('@App/customer_register/customer.register.html.twig', array(

			));
		}
	}


	/**
	* @Route("/regcustomer", name="regcustomer")
	*/
	public function regcustomerAction(Request $request, UserPasswordEncoderInterface $encoder)
	{
		/*      Register new user 
		-----------------------------*/

		// $_POST parameters
		$name_login = $request->request->get('name_login');
		$nickname_login = $request->request->get('nickname_login');
		$email_login = $request->request->get('email_login');
		$password_login = $request->request->get('password_login');

		/*  check if data resived is valid
		-----------------------------------------*/
		if (!empty($name_login) && !empty($password_login) && !empty($nickname_login)) {
				//Get the enity manager
				$em = $this->getDoctrine()->getManager();
				$user = new User();

				try {
					// encode pass
					$encoded = $encoder->encodePassword($user, $password_login);

					/*              Set data in sb
					-------------------------------------------------*/
					$user->setUsername($name_login);
					$user->setNickname($nickname_login);
					$user->setPassword($encoded);

					// mail validation
					if ( filter_var($email_login, FILTER_VALIDATE_EMAIL) ) {
						$user->setEmail($email_login);
					}

					$em->persist($user);
					$em->flush();

					$this->addFlash(
						'success',
						'Поздравляем!!! Вы успешно зарегистрировались.'
					);

					/*        Authentication after registration
					----------------------------------------------------*/
					// get registered user
					$last_user = $user->getId();
					$reg_user = $this->getDoctrine()->getRepository(User::class)->find($last_user);

					//Handle getting or creating the user entity likely with a posted form
					// The third parameter "main" can change according to the name of your firewall in security.yml
					$token = new UsernamePasswordToken($reg_user, null, 'main', $reg_user->getRoles());
					$this->get('security.token_storage')->setToken($token);

					// If the firewall name is not main, then the set value would be instead:
					// $this->get('session')->set('_security_XXXFIREWALLNAMEXXX', serialize($token));
					$this->get('session')->set('_security_main', serialize($token));

					// Fire the login event manually
					$event = new InteractiveLoginEvent($request, $token);
					$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

				}
				/*if appear error with duplicate of entity redirect user to registration page.
				need to male notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'Недоступное имя пользователя. Имя - уже занято.'
					);
					return $this->redirectToRoute('regcustomer');
				}


		} else {
			$this->addFlash(
				'warning',
				'Заполните все обязательные поля'
			);
			return $this->redirectToRoute('customer_register');
		}

		return $this->redirectToRoute('homepage');
	}


	/**
	* @Route("/recovery_password/{rp}", name="recovery_password")
	*/
	public function recovery_passwordAction($rp = null, Request $request, Mailer $mailer, UserPasswordEncoderInterface $encoder)
	{
		$em = $this->getDoctrine()->getManager();

		if ($rp) {
			$name_login = $request->request->get('name_login');
			$nickname = $request->request->get('nickname');
			$email = $request->request->get('email');
			$uniq_pass = uniqid();

			try{
				$User_repo = $em->getRepository(User::class);
				$User = $User_repo->findOneBy(
					[
						'username' => $name_login,
						'nickname' => $nickname,
						'email' => $email
					], [], 1);

				if (!$User) {
					$this->addFlash(
						'error',
						'Не был найден пользователь с указанными параметрами.'
					);
					return $this->redirectToRoute('recovery_password');
				}

				/*------------------------------------------------------------------------
						CHECK AND SEND NEW PASS TO CUSTOMER MAIL
				------------------------------------------------------------------------*/
				$contact_repo = $em->getRepository(Contact::class);

				$contact_prof = $contact_repo->findOneBy([], ['id' => "DESC"], 1)->info;
				/*
					1 - from
					2 - to
					3 - subject
					4 - body
					5 - files
					5 - contact_prof
				*/
				$from = $contact_prof['mail_value'];
				$to = [$email ];

				$subject = 'Globalpharma recovery password';

				$body = ' Ваш новый пароль :' . $uniq_pass;

				$send_mail = $mailer->sendMail($from, $to, $subject, $body, $files = '', $contact_prof);

				if ($send_mail == 'success') {

					// encode pass
					$uniq_pass = $encoder->encodePassword($User, $uniq_pass);

					$User->setPassword($uniq_pass);
					$em->persist($User);
					$em->flush();

					$this->addFlash(
						'success',
						'Новый пароль был отправлен на вашу электронную почту.'
					);

					return $this->redirectToRoute('recovery_password');

				}else {
					$this->addFlash(
						'error',
						'Из-за технических неполадок не удалось изменить пароль. Свяжитесь с администрацией'
					);

					return $this->redirectToRoute('recovery_password');
				}

			}
			catch(DBALException $e) {
				if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
					$this->addFlash(
						'error',
						$e->getMessage()
					);
				}else {
					$this->addFlash(
						'error',
						'По техническим причинам восстановлениt пароля не может быть выполнено'
					);
				}
			}

		} /*if form is submitet */

		return $this->render('@App/customer_register/customer.recovery_password.html.twig', array(
		));
	}


} /*./END of class*/
