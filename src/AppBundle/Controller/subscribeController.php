<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// Entity
use AppBundle\Entity\subscribers;
use AppBundle\Entity\reviews;

// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

// Catch error when insert on DB
use Doctrine\DBAL\DBALException;

class subscribeController extends Controller 
{

	/*-------------------------------------------------------------
				// newsletter
	-------------------------------------------------------------*/

	/**
	* @Route("/subscribe", name="subscribe")
	*/
	public function subscribeAction(Request $request)
	{
		// $_GET parameters
		// $get = $request->query->get('subscribe_input');

		// $_POST parameters
		$subscriber_mail = $request->request->get('subscribe_input');

		if (!empty($subscriber_mail) ) {
			// mail validation
			if ( filter_var($subscriber_mail, FILTER_VALIDATE_EMAIL) ) {
				try {
					// set data in sb
					$em = $this->getDoctrine()->getManager();
					$subscriber = new subscribers();
					$subscriber->setMail($subscriber_mail);

					if ($this->getUser()) {
						$user_id = $this->getUser()->getId();
						$subscriber->setUserId($user_id);
					}else {
						$subscriber->setUserId(0);
					}

					$subscriber->setTarget('newsletter');

					$em->persist($subscriber);
					$em->flush();

					$this->addFlash(
						'success',
						'Вы успешно подписались на новостную рассылку!'
					);
					return $this->redirectToRoute('homepage');
					// END set data in sb
				}
				/*if appear error with duplicate of entity redirect user to registration page.
				need to make notify */
				catch(UniqueConstraintViolationException $e) {
					$this->addFlash(
						'warning',
						'Такой адрес эл.почты уже подписан на новостную рассылку!'
					);
					return $this->redirectToRoute('homepage');
				}
			} else {
				$this->addFlash(
					'error',
					'Недействительный адрес электронной почты.'
				);
				return $this->redirectToRoute('homepage');
			}
		} else {
			$this->addFlash(
				'error',
				'Введи аддресс электронной почьты для подписки на новостную рассылку.'
			);
			return $this->redirectToRoute('homepage');
		}

	}


	/*-------------------------------------------------------------
				// testimonial
	-------------------------------------------------------------*/
	/**
	* @Route("/testimonial", name="testimonial")
	*/
	public function testimonialAction(Request $request)
	{

		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
			$arrData = [
				'type' => 'warrning',
				'output' => 'Необходима авторизация для добавления отзыва.'
			];

			return new JsonResponse($arrData);
		}

		$testimonial = $request->request->get('testimonial');

		if (empty($testimonial)) {
			$arrData = [
				'type' => 'warning',
				'output' => 'Поле с вашим отзывом не было заполнено. Заполните поле и повторите попытку.'
			];

			return new JsonResponse($arrData);
		}

		$em = $this->getDoctrine()->getManager();

		/*                   Get current user
		------------------------------------------------------------------------------*/
		$user_id = $this->getUser()->getId();

		$username = $this->getUser()->getNickname();
		if ( empty($username) ) {
			$username = $this->getUser()->getUsername();
		}

		try{
			$newTestimonial = new reviews();
			/* ------- Set id user ----------*/
			$newTestimonial->setIdUser($user_id);
			/* ------- Set name user ----------*/
			$newTestimonial->setNameUser($this->getUser()->getNickname());
			/* ------- Set comment of user ----------*/
			$newTestimonial->setComment($testimonial);
			/* ------- Set status of entry----------*/
			$newTestimonial->setStatus('confirm');

			$em->persist($newTestimonial);
			$em->flush();

			$arrData = [
				'type' => 'success',
				'output' => 'Ваш отзыв был отправлен на валидацию.'
			];

			return new JsonResponse($arrData);

		}
		catch( DBALException $e ){
			$arrData = [
				'type' => 'error',
				'output' => 'Не удаётся добавить отзыв. Неполадки в системе, свяжитесь с администрацией'
			];

			return new JsonResponse($arrData);
		}

	}



	/*-------------------------------------------------------------
				// productSubscribe
	-------------------------------------------------------------*/

	/**
	* @Route("/productSubscribe", name="productSubscribe")
	*/
	public function productSubscribeAction(Request $request)
	{
		$subscriber_mail = $request->request->get('subscriber_mail');
		$pid = $request->request->get('pid');

		if (!empty($subscriber_mail) || !empty($pid) ) {

			/*----------------------------------------------------------------
						mail validation
			-----------------------------------------------------------------*/
			if ( filter_var($subscriber_mail, FILTER_VALIDATE_EMAIL) ) {
				try {
					// set data in sb
					$em = $this->getDoctrine()->getManager();

					/*----------------------------------------------------------------
								Duplicate checker
					-----------------------------------------------------------------*/
					$subscribers_repo = $em->getRepository(subscribers::class);
					$duplicate = $subscribers_repo->findBy(
						[
							'pid' => $pid,
							'mail' => $subscriber_mail
						],[]
					);

					if (!empty($duplicate)) {
						$arrData = [
							'type' => 'warning',
							'output' => 'Вы уже подписаны на данный продукт'
						];

						return new JsonResponse($arrData);
					}

					/*----------------------------------------------------------------
								 Create new entry
					-----------------------------------------------------------------*/
					$subscriber = new subscribers();
					$subscriber->setMail($subscriber_mail);

					if ($this->getUser()) {
						$user_id = $this->getUser()->getId();
						$subscriber->setUserId($user_id);
					}

					// Set product id
					$subscriber->setPid($pid);

					// Set target 
					$subscriber->setTarget('products');

					$em->persist($subscriber);
					$em->flush();

					$arrData = [
						'type' => 'success',
						'output' => 'Мы оповестим вас когда данный продукт появится в наличии!'
					];

					return new JsonResponse($arrData);
				}
				/*----------------------------------------------------------------
						 if appear error with duplicate of
						  entity redirect user to registration page.
							need to make notify
				-----------------------------------------------------------------*/
				catch(DBALException $e) {
					$arrData = [
						'type' => 'warning',
						'output' => 'Такой адрес эл.почты уже подписан на новостную рассылку!'
					];
					return new JsonResponse($arrData);
				}
			// End if mail is not valid
			} else {
				$arrData = [
					'type' => 'error',
					'output' => 'Недействительный адрес электронной почты.'
				];

				return new JsonResponse($arrData);
			}
		// End if parametres is not defined
		} else {
			$arrData = [
				'type' => 'error',
				'output' => 'Не указаны данные для подписки на товар'
			];

			return new JsonResponse($arrData);
		}

	}
}
