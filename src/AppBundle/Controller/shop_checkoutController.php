<?php
 
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;

// Entities
use AppBundle\Entity\recipient;
use AppBundle\Entity\Basket;
use AppBundle\Entity\Orderr;
use AppBundle\Entity\Delivery_cost; 

// Session
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class shop_checkoutController extends Controller
{
	/*..............................................................................................................................................
											shop_checkout_one
	..............................................................................................................................................*/

	/**
	 * @Route("/shop_checkout_one", name="shop_checkout_one")
	 */
	public function shop_checkout_oneAction(SessionInterface $session)
	{
		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();

		/*                   Get current user
		------------------------------------------------------------------------------*/
		if ($this->getUser()) {
			$user_id = $this->getUser()->getId();
		}else {
			$user_id = 0;
		}

		/*                   Get repository
		------------------------------------------------------------------------------*/
		$recipient_repo = $em->getRepository(recipient::class); 
		$recipient = $recipient_repo->findBy(array(
			"idUser" => $user_id,
			"entry_status" => 1
		), array('id' => 'DESC'));

		// $session = new Session();
		// $session->set('subtotals', $sum);

		// Subjects of Russian Federation
		$subjects_url = $this->container->getParameter('kernel.root_dir') . '/../web/API/russia.subjects.json';

		$subjects_json = file_get_contents($subjects_url);
		$subjects = json_decode($subjects_json);
		$subjects = $subjects->data;

		// Locality_type of Russian Federation
		$locality_type_url = $this->container->getParameter('kernel.root_dir') . '/../web/API/russia.locality-type.json';

		$locality_type_json = file_get_contents($locality_type_url);
		$locality_type = json_decode($locality_type_json);
		$locality_type = $locality_type->data;

		// Locality_type of Russian Federation
		$street_type_url = $this->container->getParameter('kernel.root_dir') . '/../web/API/russia.street-type.json';

		$street_type_json = file_get_contents($street_type_url);
		$street_type = json_decode($street_type_json);
		$street_type = $street_type->data;


		if ($session->get('AddressId')) {
			$AddressId = $session->get('AddressId');
			$AddressDetail = $recipient_repo->find($AddressId);

			if (empty($AddressDetail)) {
				$session->set('AddressDetail', $AddressDetail);
			}
		}

		/* --------------------------------------------------
			Update order summary subtotal on session
		---------------------------------------------------*/
		// Basket repo
		$Basket_repo = $em->getRepository(Basket::class);
		$basket_subtotals = $Basket_repo->recalculateSubtotals($user_id);
		// set count of element on session
		$session->set('subtotals', $basket_subtotals);

		return $this->render('@App/shop_checkout/shop.checkout.one.html.twig', array(
			'recipient' => $recipient,
			'subjects' => $subjects,
			'locality_type' => $locality_type,
			'street_type' => $street_type
		));
	}


	/**
	 * @Route("/SaveRecipientAddress", name="SaveRecipientAddress")
	 */
	public function SaveRecipientAddressAction(Request $request)
	{
		/*                   Work with session
		------------------------------------------------------------------------------*/
		$session = new Session();

		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		try {
			$em = $this->getDoctrine()->getManager();

			/*                   Create new recipient address
			------------------------------------------------------------------------------*/
			$recipient = new recipient();

			/*                   Get current user
			------------------------------------------------------------------------------*/
			if ($this->getUser()) {
				$user_id = $this->getUser()->getId();
				$recipient->setIdUser($user_id);
			}else {
				$this->addFlash(
					'success',
					'Проблемы с авторизацией, свяжитесь с администрацией сайта'
				);
				return $this->redirectToRoute('logout');
			}

			/*                   Form fields 
			------------------------------------------------------------------------------*/
			/* Personal data */
			//surname 
			$surname = $request->request->get('surname');
			if ( !empty($surname) ) {
				$recipient->setSurname($surname);
			}else{
				$this->addFlash(
					'error',
					'Поле фамилия является обязательным'
				);
				return $this->redirectToRoute('shop_checkout_one');
			}

			// name
			$name = $request->request->get('name');
			if ( !empty($name) ) {
				$recipient->setName($name);
			}else{
				$this->addFlash(
					'error',
					'Поле имя является обязательным'
				);
				return $this->redirectToRoute('shop_checkout_one');
			}

			// patronymic
			$patronymic = $request->request->get('patronymic');
			$recipient->setPotronymic($patronymic);

			// phone
			$phone = $request->request->get('phone');
			$recipient->setPhone($phone);

			/* Standard postal address */
			//index_postal
			$Postcode = $request->request->get('index_postal');
			if ( !empty($name) ) {
				$recipient->setPostcode($Postcode);
			}else{
				$this->addFlash(
					'error',
					'Поле "Почтовый индекс" является обязательным'
				);
				return $this->redirectToRoute('shop_checkout_one');
			}

			// region
			$region = $request->request->get('region');
			$recipient->setRegion($region);

			// district
			$district = $request->request->get('district');
			$recipient->setDistrict($district);

			// locality_type
			$locality_type = $request->request->get('locality_type');
			$recipient->setLocalityType($locality_type);

			// locality_name
			$locality_name = $request->request->get('locality_name');
			$recipient->setLocalityName($locality_name);

			/* Expanded address ,
			Poste restante "До востребования"*/

			// poste_restante
			$poste_restante = $request->request->get('poste_restante');
			// $recipient->setPosteRestante($poste_restante);
			$recipient->setPosteRestante($poste_restante);

			// if checked poste_restante 
			// else fields bellow will be empty
			if ($poste_restante == "По адресу") {

				// street_type
				$street_type = $request->request->get('street_type');
				$recipient->setStreetType($street_type);

				// street_name
				$street_name = $request->request->get('street_name');
				$recipient->setStreetName($street_name);

				// home
				$home = $request->request->get('home');
				$recipient->setHome($home);

				// fraction
				$fraction = $request->request->get('fraction'); // Дробь
				$recipient->setFraction($fraction);

				// housing
				$housing = $request->request->get('housing'); // Корпус - Блок
				$recipient->setHousing($housing);

				// porch
				$porch = $request->request->get('porch'); // Подьезд
				$recipient->setPorch($porch);

				// structure
				$structure = $request->request->get('structure'); // Строение
				$recipient->setStructure($structure);

				// room
				$room = $request->request->get('room'); // Квартира
				$recipient->setRoom($room);
			}

			/* Expanded address*/ 
			$comment = $request->request->get('comment'); // Квартира
			$recipient->setComment($comment);

			$em->persist($recipient);
			$em->flush();

			/* Insert on session address id */
			$AddressId = $recipient->getId();
			$session->set('AddressId', $AddressId);

			$recipient_repo = $em->getRepository(recipient::class);
			$rec_det = $recipient_repo->find($AddressId);

			if ($rec_det) {
				$session->set('AddressDetail', $rec_det);
			}else {
				$this->addFlash(
					'default',
					'Ошибка при указание адреса доставки.'
				);
			}

			return $this->redirectToRoute('shop_checkout_two');

		} catch (Exception $e) {
			$this->addFlash(
				'error',
				$e->getMessage()
			);
			return $this->redirectToRoute('shop_checkout_one');
		}

	}

	/**
	 * @Route("/setAddressId", name="setAddressId")
	 */
	public function setAddressIdAction(Request $request)
	{
		/*                   Work with session
		------------------------------------------------------------------------------*/
		$session = new Session();

		$em = $this->getDoctrine()->getManager();

		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		$recipientId = $request->request->get('recipient');

		if ($recipientId) {
			/* Insert on session address id */
			$session->set('Address', 'set');
			$session->set('AddressId', $recipientId);

			/*                   Get repository
			------------------------------------------------------------------------------*/
			$recipient_repo = $em->getRepository(recipient::class);

			$recipient = $recipient_repo->find($recipientId);

			$session->set('AddressDetail', $recipient); 

			$this->addFlash(
				'default',
				'Адрес успешно указан.'
			);

			return $this->redirectToRoute('shop_checkout_two');
		}else{
			$this->addFlash(
				'error',
				'Ошибка при указание Адреса.'
			);

			return $this->redirectToRoute('shop_checkout_one');
		}

	}

	/**
	 * @Route("/rm_Recipient_Address", name="rm_Recipient_Address")
	 */
	public function rm_Recipient_AddressAction(Request $request)
	{

		// auth checker
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

			// if received request have product id
			if($request->request->get('rcpId')){
				/* get doctrine */
				$em = $this->getDoctrine()->getManager();
				$user_id = $this->getUser()->getId();
				$entry_id= $request->request->get('rcpId');

				/* products */
				$recipient_repo = $em->getRepository(recipient::class);
				$recipient = $recipient_repo->findOneBy(
					[
						'id' => $entry_id,
						'idUser' => $user_id,
					] , []
				);

				// if the result is found
				if ($recipient) {
					// $em->remove($recipient);
					$recipient->setEntryStatus(0);
					$em->flush();
					/* return success result */
					$arrData = [
						'type' => 'success',
						'output' => 'Адресс был успешно удалён.',
					];
					return new JsonResponse($arrData);
				}
				// if the result is not found
				 else {
					$arrData = [
						'type' => 'error',
						'output' => 'An entry with this id was not found.'
					];
					return new JsonResponse($arrData);
				}

			} // END if received request have product id
			else {
					$arrData = [
						'type' => 'error',
						'output' => 'Entries what you insert in not found.'
					];
					return new JsonResponse($arrData);
			}

		} // END auth checker
		else {
			return $this->redirectToRoute('shop_checkout_one');
		}
	}

	/*..............................................................................................................................................
									shop_checkout_two
	..............................................................................................................................................*/

	/**
	 * @Route("/shop_checkout_two", name="shop_checkout_two")
	 */
	public function shop_checkout_twoAction(Request $request)
	{
		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		if ($request->request->get('delivery')) {

			/*                   Work with session
			------------------------------------------------------------------------------*/
			$session = new Session();

			$delivery_method = $request->request->get('delivery');

			$this->addFlash(
				'default',
				'Успешно указали метод доставки.'
			);

			$session->set('DeliveryMethod', $delivery_method);

			return $this->redirectToRoute('shop_checkout_four');
		}

		return $this->render('@App/shop_checkout/shop.checkout.two.html.twig', array(
			// ...
		));
	}

	/*..............................................................................................................................................
									shop_checkout_three
	..............................................................................................................................................*/

	/**
	 * @Route("/shop_checkout_three/{order_uniqid}", name="shop_checkout_three")
	 */
	public function shop_checkout_threeAction(SessionInterface $session, Request $request, $order_uniqid = null)
	{
		/*                   Security checker  ----------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}

		if ($order_uniqid == null) {
			$this->addFlash(
				'error',
				'Не заданы параметры заказа для выбора способа оплаты.'
			);

			return $this->redirectToRoute('customer_orders');
		}

		/*                   if order id is not defined -------------------------*/
		if (!$order_uniqid) {
			return $this->redirectToRoute('customer_orders');
		}

		/*                   Work with session ----------------------------------*/
		// $session = new Session();

		/* ------------------id_user------------- */
		$user = $this->getUser();
		$id_user = $user->getId();

		/*                   Find order with potrivit id ------------------------*/
		$Order_repo = $this->getDoctrine()->getRepository(Orderr::class);

		$Order = $Order_repo->findBy([
			"uniqid" => $order_uniqid,
			'idUser' => $id_user
		], []);

		return $this->render('@App/shop_checkout/shop.checkout.three.html.twig', array(
			'order_uniqid' => $order_uniqid
		));
	}

	/*..............................................................................................................................................
									shop_checkout_four
	..............................................................................................................................................*/

	/**
	 * @Route("/shop_checkout_four", name="shop_checkout_four")
	 */
	public function shop_checkout_fourAction(SessionInterface $session)
	{
		/*                   Security checker
		------------------------------------------------------------------------------*/
		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			return $this->redirectToRoute('homepage');
		}
		$em = $this->getDoctrine()->getManager();

		/* ------------------id_user------------- */
		$user = $this->getUser();
		$id_user = $user->getId();

		$Basket_repo = $this->getDoctrine()->getRepository(Basket::class);
		$strongFullUserBasket = $Basket_repo->strongFullUserBasket($id_user);

		$session = new Session();
		$session->set('tmp_into_order', $strongFullUserBasket);


		// get delivery cost list
		$Delivery_cost_repo = $em->getRepository(Delivery_cost::class);
		$Delivery_cost = $Delivery_cost_repo->findAll();

		// insert delivery cost list on session for prevent more requests to db
		(!$Delivery_cost)? $session->set('Delivery_cost', []) : $session->set('Delivery_cost', $Delivery_cost); 

		/*---------------------------------------------------------
					Basket subtotals
		---------------------------------------------------------*/

		// Basket repo
		$basket_subtotals = $Basket_repo->recalculateSubtotals($id_user);
		// set count of element on session
		$session->set('subtotals', $basket_subtotals);

		return $this->render('@App/shop_checkout/shop.checkout.four.html.twig', array(
			"fullUserBasket" => $strongFullUserBasket
		));
	}


}
