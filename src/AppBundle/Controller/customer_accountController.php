<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

// Entity
use AppBundle\Entity\favorites;
use AppBundle\Entity\products;
use AppBundle\Entity\User;
use AppBundle\Entity\Basket;
use AppBundle\Entity\Orderr;
use AppBundle\Entity\recipient;
use Phpeshop\ShoppaymentBundle\Entity\Wallet;


use \Datetime;

/*for encode password*/
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

// sevices
use AppBundle\Service\pChecker;

// Catch error when insert on DB
use Doctrine\DBAL\DBALException;

class customer_accountController extends Controller
{
	//Custom mzf
	public function accessDenied ($role){
		// access denied for anonymous
		if (!$this->get('security.authorization_checker')->isGranted($role)) {
			throw $this->createAccessDeniedException('GET OUT!');
			return $this->redirectToRoute('homepage');
		}
	}

	/*--------------------------------------------------------------------------------
					CUSTOMER ACCOUNT
	---------------------------------------------------------------------------------*/

	/**
	 * @Route("/customer_account", name="customer_account")
	 */
	public function customer_accountAction()
	{
		$this->accessDenied('ROLE_USER');

		$em = $this->getDoctrine()->getManager();

		/* ------------------id_user------------- */
		$user_id = $this->getUser()->getId();
		if (!$user_id) {
			$this->addFlash(
				'error',
				'Не найден пользователь с указанным ИД'
			);

			return $this->redirectToRoute('homepage');
		}

		/* --------------Order products --------- */
		$user_repo = $this->getDoctrine()->getRepository(User::class);
		$user = $user_repo->find($user_id);

		return $this->render('@App/customer_account/customer.account.html.twig', array(
			'user' => $user
		));
	}

	/**
	 * @Route("/updatecustomer", name="updatecustomer")
	 */
	public function updatecustomerAction(Request $request,  UserPasswordEncoderInterface $encoder)
	{
		$this->accessDenied('ROLE_USER');

		$em = $this->getDoctrine()->getManager();

		/* ------------------id_user------------- */
		$user_id = $this->getUser()->getId();
		if (!$user_id) {
			$this->addFlash(
				'error',
				'Не найден пользователь с указанным ИД'
			);

			return $this->redirectToRoute('homepage');
		}

		/* --------------User entry --------- */
		$user_repo = $this->getDoctrine()->getRepository(User::class);
		$user = $user_repo->find($user_id);

		/*------------------------------------------------------------
				prepare Login / Nickname
		--------------------------------------------------------------*/
		$username = $request->request->get('login');
		$nickname = $request->request->get('nickname');

		/*------------------------------------------------------------
				prepare NEW CUSTOMER PASSWORD
		--------------------------------------------------------------*/
		$new_password1 = $request->request->get('new_password1');
		$new_password2 = $request->request->get('new_password2');

		if (!empty($new_password1)) {
			if (empty($new_password1) || empty($new_password2)) {
				$this->addFlash(
					'error',
					'Для изменения пароля необходимо заполнить оба поля'
				);
				return $this->redirectToRoute('customer_account');
			}

			if ( $new_password1 !== $new_password2 ) {
				$this->addFlash(
					'error',
					'Введённые пароли не совпадают'
				);
				return $this->redirectToRoute('customer_account');
			}

			// encode pass
			$encoded_pass = $encoder->encodePassword($user, $new_password1);
		}

		/*------------------------------------------------------------
				prepare  CUSTOMER MAIL
		--------------------------------------------------------------*/
		$customer_mail = $request->request->get('customer_mail');

		if (!empty($customer_mail)) {
			if(!filter_var($customer_mail, FILTER_VALIDATE_EMAIL)){
				$this->addFlash(
					'error',
					'Проверьте правильность заполнения поля "Эл.почта"'
				);
				return $this->redirectToRoute('customer_account');
			}
		}

		try {
			// set username
			if (!empty($username) && sizeOf($username) < 50) {
				$user->setUsername($username);
			}

			// set nickname
			if (!empty($nickname) && sizeOf($nickname) < 50) {
				$user->setNickname($nickname);
			}

			// set pass
			if (!empty($new_password1)) {
				$user->setPassword($encoded_pass);
			}

			// set Email
			if (!empty($customer_mail)) {
				$user->setEmail($customer_mail);
			}

			$em->persist($user);
			$em->flush();

			$this->addFlash(
				'success',
				'Данные были успешно обновлены'
			);
			return $this->redirectToRoute('customer_account');

		} catch (DBALException $e) {
			$this->addFlash(
				'error',
				'Что то пошло не так. Повторите попытку. В случае неудачи свяжитесь с администрацией'
			);

			return $this->redirectToRoute('customer_account');
		}

		return $this->render('@App/customer_account/customer.account.html.twig', array(
		));
	}

	/*---------------------------------------------------------------------------------------------------------
							LIST OF CUSTOMER ORDERS
	----------------------------------------------------------------------------------------------------------*/

	/**
	 * @Route("/customer_orders", name="customer_orders")
	 */
	public function customer_ordersAction()
	{
		/* ------------------ Security checker ------------- */
		$this->accessDenied('ROLE_USER');

		$em = $this->getDoctrine()->getManager();

		/*------------------------------------------------------------
						PREPARE DATA FOR USING 
		--------------------------------------------------------------*/

		/* ------------------id_user------------- */
		$user = $this->getUser();
		$id_user = $user->getId();


		/* --------------Order products --------- */
		$Oreder_repo = $this->getDoctrine()->getRepository(Orderr::class);

		/* --------------Get number of wallet --------- */
		$wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);
		$wallet = $wallet_repo->findOneBy(array(
			'nameWallet' => 'qiwi',
			'mainWallet' => 1
		), array('id' => 'DESC'), 1);


		$Detailwallet = $wallet->getDetailwallet();

		$nrWallet = $Detailwallet['walletNr'];

		/* -------------- Generated link for payment ------------------- */
		// $url = 'https://qiwi.com/payment/form/99?extra%5B%27account%27%5D=' . $nrWallet . '&amountInteger=' . $final_cost . '&extra%5B%27comment%27%5D=' . $comment . '&currency=643';

		$orders = $Oreder_repo->findBy([
			'idUser' =>$id_user,
			'entry_status' => 1
		], ["id" => "DESC"]);


		/* -------------- ------------------------------------- 
			Recalculate track availables day for displaying
		 ---------------------------------------------------- */
		$track_days = [];
		if ($orders) {
			$cd = new Datetime();

			foreach ($orders as $key => $order) {
				$track_date = $order->getTrackDate();
				$orderId = $order->getId();

				if ($track_date) {
					$track_days_avail = $track_date->diff($cd)->format('%r%a');
					$track_days[$key] = ['track_days_avail' => $track_days_avail, 'orderId' => $orderId];
				}else {
					$track_days[$key] = ['track_days_avail' => "0", 'orderId' => $orderId];
				}

			}
		}

		// print "<pre>";
		// print_r($track_days);
		// print "</pre>";

		return $this->render('@App/customer_account/customer.orders.html.twig', array(
			'customer_orders' => $orders,
			'nrWallet' => $nrWallet,
			'track_days' => $track_days,
		));
	}

	/*-----------------------------------------------------------------
				Order detail
	-----------------------------------------------------------------*/

	/**
	 * @Route("/customer_order/{oredrUniqId}", name="customer_order")
	 */
	public function customer_orderAction($oredrUniqId)
	{ 
		/* ------------------ Security checker ------------- */
		$this->accessDenied('ROLE_USER');

		$em = $this->getDoctrine()->getManager();

		/* ------------------id_user------------- */
		$user = $this->getUser();
		$id_user = $user->getId();

		/* --------------Order products --------- */
		$Oreder_repo = $this->getDoctrine()->getRepository(Orderr::class);


		/* --------------Get number of wallet --------- */
		$wallet_repo = $this->getDoctrine()->getRepository(Wallet::class);
		$wallet = $wallet_repo->findOneBy(array(
			'nameWallet' => 'qiwi',
			'mainWallet' => 1
		), array('id' => 'DESC'), 1);


		$Detailwallet = $wallet->getDetailwallet();

		$nrWallet = $Detailwallet['walletNr'];

		$order = $Oreder_repo->findOneBy([
			'idUser' =>$id_user,
			'uniqid' => $oredrUniqId
		], []);

		if (!$order) {
			$this->addFlash(
				'error',
				'Заказ c с таким ид. не был найден. Свяжитесь с администрацией сайта.'
			);

			return $this->redirectToRoute('homepage');
		}

		// $order_detail = $Oreder_repo->orderDetails($orders);

		return $this->render('@App/customer_account/customer.order.html.twig', array(
			'order' => $order,
			'nrWallet' => $nrWallet
		));
	}


	/**
	 * @Route("/customer_wishlist")
	 */
	public function customer_wishlistAction(pChecker $pChecker)
	{
		if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
			// get current authorized user 
			$user = $this->getUser();
			$user_id = $user->getId();

			$em = $this->getDoctrine()->getManager();

			// favorites products
			$products_repo = $em->getRepository(products::class);
			$favorites_products =  $products_repo->findBy(array(), array('rating' => 'DESC'));

			$favoritess_repo = $em->getRepository(favorites::class);
			$favorites_products = $pChecker->ifAdded("favorite", $favorites_products, $favoritess_repo, $user_id);

			/*                  Check if user has been add product on basket
			------------------------------------------------------------------------------*/
			// Basket
			$Basket_repo = $em->getRepository(Basket::class);
			$favorites_products = $pChecker->ifAdded("onbasket", $favorites_products, $Basket_repo, $user_id);

			/*                 Check if product is new on the system
			------------------------------------------------------------------------------*/
			$favorites_products = $pChecker->ifNewTwo($favorites_products);

			return $this->render('@App/customer_account/customer.wishlist.html.twig', array(
				'user_favorites' => $favorites_products,
			));
		}
	}

	/**
	 * @Route("/customer_message")
	 */
	public function customer_messageAction()
	{
		$this->accessDenied('ROLE_USER');

		return $this->render('@App/customer_account/customer.message.html.twig', array(
			// ...
		));
	}

}
