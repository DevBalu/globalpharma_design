<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
// Entity
use AppBundle\Entity\favorites;
// JsoneResponse
use Symfony\Component\HttpFoundation\JsonResponse;

class FavoriteController extends Controller
{
	/**
	* @Route("/addFavorite", name="addFavorite")
	*/
	public function addFavoriteAction(Request $request)
	{

		if($request->request->get('pid')){
			$pid = $request->request->get('pid');

			// get doctrine
			$em = $this->getDoctrine()->getManager();
			// get favorites entity
			$favorites = new favorites();

			// get current id user if he is auth-ed
			if ( $this->getUser() ) {
				$user_id = $this->getUser()->getId();
			} else {
				$user_id = 0;
				// make functionality for add product on basker without id user. Make any session or any. After make it.
			}

			// INSERT DATA TO DB
			// set id user
			$favorites->setIdUser($user_id);

			// set id of product and her count
			$favorites->setProductid($pid);

			/*		custom check of duplicate entries
			---------------------------------------------------------*/
			$favorites_repo = $em->getRepository(favorites::class);

			$dublicate_favorites = $favorites_repo->findBy(
				[
					'idUser' => $user_id,
					'productid' => $pid
				] , []
			);

			if (!$dublicate_favorites) {

				$em->persist($favorites);
				$em->flush();

				//make something curious, get some unbelieveable data
				$arrData = [
					'type' => 'success',
					'output' => 'Продукт успешно добавлен в список избранных!'
				];

			} else {
				$arrData = [
					'type' => 'warning',
					'output' => 'Этот продукт уже добавлен в список избранных.'
				];
			}

		} else {
			// if not found product id
			$arrData = [
				'type' => 'error',
				'output' => 'Не были заданы параметры поиска.'
			];
		}

		return new JsonResponse($arrData);
	}


	/**
	* @Route("/rmFavorite", name="rmFavorite")
	*/
	public function rmFavoriteAction(Request $request)
	{

		if($request->request->get('pid')){
			$pid = $request->request->get('pid');

			// get doctrine
			$em = $this->getDoctrine()->getManager();
			// get favorites entity

			// get current id user if he is auth-ed
			if ( $this->getUser() ) {
				$user_id = $this->getUser()->getId();
			} else {
				$user_id = 0;
				// make functionality for add product on basker without id user. Make any session or any. After make it.
			}

			$favorites_repo = $em->getRepository(favorites::class);
			$on_favorites = $favorites_repo->findOneBy(
				[
					'productid' => $pid,
					'idUser' => $user_id
				],
				[]
			);

			// if the result is found
			if ($on_favorites) {
				$em->remove($on_favorites);
				$em->flush();

				/* return success result */
				$arrData = [
					'type' => 'success',
					'output' => 'Товар был успешно удалён из списка избранных.'
				];

				return new JsonResponse($arrData);
			}
			// if the result is not found
			 else {
				$arrData = [
					'type' => 'error',
					'output' => 'no results found.'
				];
				return new JsonResponse($arrData);
			}

		} else {
			// if not found product id
			$arrData = [
				'type' => 'error',
				'output' => 'Не были заданы параметры поиска.'
			];
		}

		return new JsonResponse($arrData);
	}

}
