<?php

namespace AppBundle\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Cache\Simple\FilesystemCache;


// catch douplicate entity error
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
// Catch db extention
use Doctrine\DBAL\DBALException;

use AppBundle\Entity\products;

class agContainerController extends Controller
{
	/**
	* @Route("/setCache", name="setCache") 
	*/
	public function setCacheAction()
	{

		$cache = new FilesystemCache();

		// save a new item in the cache
		$arr = [
			'vasea' => 'zina',
			'jora' => 'elizabet'
		];

		$cache->set('start.products_count', $arr);

		return new JsonResponse();
	}


	/**
	* @Route("/getCache", name="getCache") 
	*/
	public function getCacheAction()
	{
		$cache = new FilesystemCache();

		if ($cache->get('start.products_count')) {
			// retrieve the value stored by the item
			$productsCount = $cache->get('start.products_count');
			print "<pre>";
			print_r($productsCount);
			print "</pre>";

		} else {
			print "<pre>";
			print_r('UPS');
			print "</pre>";
		}


		return new JsonResponse();
	}

}
