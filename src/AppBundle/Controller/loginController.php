<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use AppBundle\Entity\User; 

class loginController extends Controller
{
	/**
	* @Route("/login", name="login")
	*/
	public function loginAction(Request $request, AuthenticationUtils $authUtils)
	{
		// $_username = $request->request->get('_username');
		// $_password = $request->request->get('_password');

		// if (empty($_username) || empty($_password)) {
		// 	$this->addFlash(
		// 		'error',
		// 		'Для авторизация необходимо заполнить оба поля'
		// 	);
		// 	return $this->redirectToRoute("homepage");
		// }

		/*                   Work with session
		------------------------------------------------------------------------------*/
		// $session = new Session();

		// $shopstate = $session->get('shopstate');
		// if ($shopstate == 'shop_audit') {
		// 	return $this->redirectToRoute("logout");
		// }


		// get the login error if there is one
		$error = $authUtils->getLastAuthenticationError();

		// last username entered by the user
		$lastUsername = $authUtils->getLastUsername();

		return $this->redirectToRoute("homepage");
	}

}


