<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

// sevices
use AppBundle\Service\pChecker;

// Entities
use AppBundle\Entity\products;
use AppBundle\Entity\Basket;
use AppBundle\Entity\favorites;

class shop_detailController extends Controller
{
	/**
	* @Route("/pDetail/{pid}", name="pDetail")
	*/
	public function pDetailAction($pid, pChecker $pChecker)
	{
		if ($pid) {
			// products repo
			$products_repo = $this->getDoctrine()->getRepository(products::class);
			$product = $products_repo->findBy(array("id" => $pid, "entry_status" => 1), array());
			$product = $pChecker->ifNewTwo($product);

			if (!$product) {
				return $this->redirectToRoute('homepage');
			}

			if ($this->getUser()) {
				$user_id = $this->getUser()->getId();
				// Check if product is already added on basket
				$Basket_repo = $this->getDoctrine()->getRepository(Basket::class);
				$on_basket = $Basket_repo->findBy(
					[
						'productid' => $pid,
						'idUser' => $user_id
					],
					[]
				);

				// if there is a result
				if ($on_basket) {
					$p_added = true;
				}else {
					$p_added = false;
				}

			}else {
				$p_added = false;
				$user_id = 0;
			}

			$favorites_repo = $this->getDoctrine()->getRepository(favorites::class);
			$user_favorites = $favorites_repo->userfavorites($user_id);
			$user_favorites = $pChecker->ifNewOne($user_favorites);

			return $this->render('@App/shop_detail/pDetail.html.twig', array(
				'product' => $product[0],
				'p_added' => $p_added,
				'user_favorites' => $user_favorites
			));


		}else{
			return $this->redirectToRoute('homepage');
		}
	}

}/*END class*/
