<?php

namespace AppBundle\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\blog;
// Catch db extention
use Doctrine\DBAL\DBALException;

class blogController extends Controller
{
	/**
	 * @Route("/blog_post/{target}", name="blog_post") 
	 */
	public function blog_postAction($target = null, Request $request)
	{

		if (!$target) {
			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$blog_repo = $this->getDoctrine()->getRepository(blog::class);

		switch ($target) {
			case 'guarantee':
				$blog_post = $blog_repo->findOneBy(
					['target' => $target] , ['id' => 'DESC'], 1
				);

				if (!$blog_post) {
					return $this->redirectToRoute('first_post_insert', ['target' => $target]);
				}

				break;
			case 'pr':
				$blog_post = $blog_repo->findOneBy(
					['target' => $target] , ['id' => 'DESC'], 1
				);
				break;

			default:
				$this->addFlash(
					'error',
					"Не указан параметр для поиска определённого поста"
				);
				return $this->redirectToRoute('homepage');

				break;
		}

		/* if is first accessing of action and on db not entry*/
		if (!$blog_post) {
			return $this->redirectToRoute('first_post_insert', ['target' => $target]);
		}


		return $this->render('@App/blog/blog_post.html.twig', array(
			'target' => $target,
			'blog_post' => $blog_post
		));
	}

	/**
	 * @Route("/edit_blog_post/{target}/{entryId}", name="edit_blog_post") 
	 */
	public function edit_blog_postAction($target, $entryId, Request $request)
	{
		if (!$this->get('security.authorization_checker')->isGranted("ROLE_ADMIN")) {
			$this->addFlash(
				'error',
				"Нет доступа для едитирования контента"
			);
			return $this->redirectToRoute('homepage');
		}

		if (!$target || !$entryId) {
			$this->addFlash(
				'error',
				"Не указан параметр для поиска определённого поста"
			);

			return $this->redirectToRoute('homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$blog_repo = $this->getDoctrine()->getRepository(blog::class);

		$blog_post = $blog_repo->findOneBy(['target' => $target, 'id' => $entryId], []);

		$blog_post_content =  $request->request->get('blog_post_content');

		if (!$blog_post) {
			$this->addFlash(
				'error',
				"Не найдена запись для едитирования"
			);

			return $this->redirectToRoute('homepage');
		}

		try{
			$blog_post->setContent($blog_post_content);

			$em->persist($blog_post);
			$em->flush();

			return $this->redirectToRoute('blog_post', ['target' => $target]);
		}
		catch(DBALException $e) {
				if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
					$this->addFlash(
						'error',
						$e->getMessage()
					);
				}else {
					$this->addFlash(
						'error',
						'Системные неполадки, страница не может быть афиширована'
					);
				}
					return $this->redirectToRoute('homepage');
		}

	}

	/**
	 * @Route("/first_post_insert/{target}", name="first_post_insert") 
	 */
	public function first_post_insertAction($target)
	{
			$em = $this->getDoctrine()->getManager();

			try{
				$blog = new blog;

				$blog->setTarget($target);
				$blog->setContent("/");

				$em->persist($blog);
				$em->flush();

				$this->addFlash(
					'success',
					"Успешно"
				);

				return $this->redirectToRoute('blog_post', ['target' => $target]);
			}
			catch(DBALException $e) {
					if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
						$this->addFlash(
							'error',
							$e->getMessage()
						);
					}else {
						$this->addFlash(
							'error',
							'Системные неполадки, страница не может быть аффилирована'
						);
					}
						return $this->redirectToRoute('homepage');
			}
	}

}
